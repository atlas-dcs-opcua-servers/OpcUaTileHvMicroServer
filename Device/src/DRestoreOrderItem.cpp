
/* This is device body stub */


#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DRestoreOrderItem.h>
#include <ASRestoreOrderItem.h>
#include <HVcan.h>
#include <DDrawer.h>
#include <DChannelNo.h>
#include <ASChannelNo.h>
#include <iostream>




namespace Device
{



/* sample ctr */
DRestoreOrderItem::DRestoreOrderItem (const Configuration::RestoreOrderItem & config

                                      , DChannelNo * parent

                                     ):

    m_parent(parent),

    m_index(config.index()),
    m_addressSpaceLink(0)

/* fill up constructor initialization list here */
{
    /* fill up constructor body here */
}

/* sample dtr */
DRestoreOrderItem::~DRestoreOrderItem ()
{
    /* remove children */

}

/* delegators for cachevariables and externalvariables */

/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */
UaStatus DRestoreOrderItem::readItemValue (
    OpcUa_Double &value,
    UaDateTime &sourceTime
)
{
    	UaStatus ret;

	getParent()->getParent()-> setRequestRESTOREORDER(this);

		msg.c_dlc = 0;
		m_ind = m_index;



   if (m_ind == HV_RESTORE_ORDER) // m_ind is hexadecimal
	    {
	      msg.c_data[0] = 0xA;
	      msg.c_data[1] = getParent()->m_channelNumber;
	      msg.c_dlc = 2;
	      printf("HV_RESTORE_ORDER\n");
	    }

   	    msg.c_id = (0x100 + (getParent()->getParent()->m_drawerID));
	    printf("msg.c_id = %x \n", msg.c_id);
	    printf("msg.c_dlc = %x \n", msg.c_dlc);
	    for (int y = 0; y< msg.c_dlc; y++)
	    {
	      	 printf("msg.c_data[y] = %x \n", msg.c_data[y]);
	    }

	    getParent()->getParent()->messageSend(msg);
	    printf("MESSAGE SENT  \n");
	    getParent()->getParent()->addDelay();
		printf("MESSAGE RECEIVED  \n");
		msgReply = getParent()->getParent()->msgReply;


	    statusValue = msgReply.c_data[0];
	    getAddressSpaceLink()->setStatusValue(statusValue, OpcUa_Good, UaDateTime::now());

	    getParent()->getParent()->freeRequestRESTOREORDER();

    sourceTime = UaDateTime::now();
    return ret;
}


/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */
UaStatus DRestoreOrderItem::writeItemValue (
    OpcUa_Double &value
)
{
    return OpcUa_BadNotImplemented;
}


/* address space linker */

void DRestoreOrderItem::linkAddressSpace( AddressSpace::ASRestoreOrderItem *addressSpaceLink)
{
    if (m_addressSpaceLink != 0)
    {
        /* signalize nice error from here. */
        abort();
    }
    else
        m_addressSpaceLink = addressSpaceLink;
}

/* add/remove */


/* to safely quit */
void DRestoreOrderItem::unlinkAllChildren ()
{
    PRINT_LOCATION();
    m_addressSpaceLink = 0;
    /* Fill up: call unlinkAllChildren on all children */
}



void DRestoreOrderItem::passRESTOREORDER(const CanMessage rmsg)
{
	msgReply = getParent()->getParent()-> passMSG(rmsg);
}



/* find methods for children */



}


