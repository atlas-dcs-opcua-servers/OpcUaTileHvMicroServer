
/* This is device body stub */


#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DReadInfoItem.h>
#include <ASReadInfoItem.h>
#include <HVcan.h>
#include <DDrawer.h>

#include <iostream>




namespace Device
{



/* sample ctr */
DReadInfoItem::DReadInfoItem (const Configuration::ReadInfoItem & config

                              , DDrawer * parent

                             ):

    m_parent(parent),

    m_index(config.index()),
	m_infoType(config.infoType()),
	m_addressSpaceLink(0)

/* fill up constructor initialization list here */
{
    /* fill up constructor body here */
}

/* sample dtr */
DReadInfoItem::~DReadInfoItem ()
{
    /* remove children */

}

/* delegators for cachevariables and externalvariables */


UaStatus DReadInfoItem::readDrawerInfo()
{
	UaStatus ret;
	UaDateTime sourceTime;
			msg.c_dlc = 0;
			m_ind = m_index;


			getParent()-> setRequestINFO(this);

	    	if ((0xFF & (m_ind>>8)) == HV_INFO_READ  ) // m_ind is hexadecimal
		    {
		      msg.c_data[0] = 0xC;
		      msg.c_data[1] = m_infoType;
		      msg.c_dlc = 2;
		      printf("HV_INFO_READ \n");
		    }

	    	msg.c_id = (0x100 + (getParent()->m_drawerID));

		    printf("msg.c_id = %x \n", msg.c_id);
		    printf("msg.c_dlc = %x \n", msg.c_dlc);
		    for (int y = 0; y< msg.c_dlc; y++)
		    {
		      	 printf("msg.c_data[y] = %x \n", msg.c_data[y]);
		    }



		    getParent()->messageSend(msg);
		    printf("MESSAGE SENT  \n");
	   	    getParent()->addDelay();
	   		printf("MESSAGE RECEIVED  \n");
	   		msgReply = getParent()->msgReply;

			if(m_infoType == 0x0A)
			{
				infoValue =  (((unsigned char)msgReply.c_data[2]*10)+ ((unsigned char)msgReply.c_data[3]*10)+ ((unsigned char)msgReply.c_data[4])+ ((unsigned char)msgReply.c_data[5]))/10.0;
			}
			else
			{
				infoValue =  ((unsigned char)msgReply.c_data[2]<<24)+ ((unsigned char)msgReply.c_data[3]<<16)+ ((unsigned char)msgReply.c_data[4]<<8)+ ((unsigned char)msgReply.c_data[5]);
			}



			getAddressSpaceLink()->setInfoValue(infoValue, OpcUa_Good, UaDateTime::now());

		    getParent()->freeRequestINFO();


	    sourceTime = UaDateTime::now();
	    return ret;
}


/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */
UaStatus DReadInfoItem::readItemValue (
    OpcUa_Double &value,
    UaDateTime &sourceTime
)
{

	UaStatus ret;

		msg.c_dlc = 0;
		m_ind = m_index;

		getParent()-> setRequestINFO(this);

    	if ((0xFF & (m_ind>>8)) == HV_INFO_READ  ) // m_ind is hexadecimal
	    {
	      msg.c_data[0] = 0xC;
	      msg.c_data[1] = m_infoType;
	      msg.c_dlc = 2;
	      printf("HV_INFO_READ \n");
	    }

    	msg.c_id = (0x100 + (getParent()->m_drawerID));

	    printf("msg.c_id = %x \n", msg.c_id);
	    printf("msg.c_dlc = %x \n", msg.c_dlc);
	    for (int y = 0; y< msg.c_dlc; y++)
	    {
	      	 printf("msg.c_data[y] = %x \n", msg.c_data[y]);
	    }



	    getParent()->messageSend(msg);
	    printf("MESSAGE SENT  \n");
   	    getParent()->addDelay();
   		printf("MESSAGE RECEIVED  \n");
   		msgReply = getParent()->msgReply;


		infoValue =  ((unsigned char)msgReply.c_data[2]<<24)+ ((unsigned char)msgReply.c_data[3]<<16)+ ((unsigned char)msgReply.c_data[4]<<8)+ ((unsigned char)msgReply.c_data[5]);
	    getAddressSpaceLink()->setInfoValue(infoValue, OpcUa_Good, UaDateTime::now());

	    getParent()->freeRequestINFO();
    sourceTime = UaDateTime::now();
    return ret;
}






/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */
UaStatus DReadInfoItem::writeItemValue (
    OpcUa_Double &value
)
{
    return OpcUa_BadNotImplemented;
}


/* address space linker */

void DReadInfoItem::linkAddressSpace( AddressSpace::ASReadInfoItem *addressSpaceLink)
{
    if (m_addressSpaceLink != 0)
    {
        /* signalize nice error from here. */
        abort();
    }
    else
        m_addressSpaceLink = addressSpaceLink;
}

/* add/remove */


/* to safely quit */
void DReadInfoItem::unlinkAllChildren ()
{
    PRINT_LOCATION();
    m_addressSpaceLink = 0;
    /* Fill up: call unlinkAllChildren on all children */


}

/* find methods for children */


void DReadInfoItem::passINFO(const CanMessage rmsg)
{
	msgReply = getParent()-> passMSG(rmsg);
}


}


