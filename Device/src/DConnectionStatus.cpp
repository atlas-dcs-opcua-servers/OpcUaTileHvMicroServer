
/* This is device body stub */


#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DConnectionStatus.h>
#include <ASConnectionStatus.h>

	#include <HVcan.h>
	#include <DDrawer.h>



namespace Device
{



/* sample ctr */
DConnectionStatus::DConnectionStatus (const Configuration::ConnectionStatus & config

                                      , DDrawer * parent

                                     ):

		m_parent(parent),

			    m_index(config.index()),
			    m_addressSpaceLink(0)

/* fill up constructor initialization list here */
{
    /* fill up constructor body here */
}

/* sample dtr */
DConnectionStatus::~DConnectionStatus ()
{
    /* remove children */

}

/* delegators for cachevariables and externalvariables */


/* address space linker */

void DConnectionStatus::linkAddressSpace( AddressSpace::ASConnectionStatus *addressSpaceLink)
{
    if (m_addressSpaceLink != 0)
    {
        /* signalize nice error from here. */
        abort();
    }
    else
        m_addressSpaceLink = addressSpaceLink;
}

/* add/remove */


/* to safely quit */
void DConnectionStatus::unlinkAllChildren ()
{
    PRINT_LOCATION();
    m_addressSpaceLink = 0;
    /* Fill up: call unlinkAllChildren on all children */


}


void DConnectionStatus::passCONNECTIONSTATUS(const CanMessage rmsg)
{
	msgReply = getParent()-> passMSG(rmsg);
}




UaStatus DConnectionStatus::readGlobalStatus()
{

	/*UaStatus ret;
	OpcUa_Int16 resetreg, warnstat, alarstat, statusbyte, nb_IRQ, statebytevalue;
	UaDateTime sourceTime;



	msg.c_dlc = 0;
	msgReply.c_dlc = 0;
	m_ind = m_index;



	getParent()-> setRequestCONNECTIONSTATUS(this);


	if (m_ind == HV_READ_GLOBAL_STATUS) // m_ind is hexadecimal
	{
		msg.c_data[0] = 0x05;
		msg.c_data[1] = 0x00;
		msg.c_dlc = 2;
		printf("HV_READ_GLOBAL_STATUS\n");
	}


	msg.c_id = (0x100 + (getParent()->m_drawerID));

	printf("msg.c_id = %x \n", msg.c_id);
	printf("msg.c_dlc = %x \n", msg.c_dlc);
	for (int y = 0; y< msg.c_dlc; y++)
	{
		printf("msg.c_data[y] = %x \n", msg.c_data[y]);
	}


	getParent()->messageSend(msg);
	printf("MESSAGE SENT  \n");
	getParent()->addDelay();
	printf("MESSAGE RECEIVED  \n");
	msgReply = getParent()->msgReply;


	if (m_ind == HV_READ_GLOBAL_STATUS) // m_ind is hexadecimal
	{
		statebytevalue = msgReply.c_data[0];
		resetreg = (unsigned char)msgReply.c_data[1] ;
		warnstat = ((unsigned char)msgReply.c_data[2]<< 8) + (unsigned char)msgReply.c_data[3];
		alarstat = ((unsigned char)msgReply.c_data[4]<< 8) + (unsigned char)msgReply.c_data[5];
		nb_IRQ = ((unsigned char)msgReply.c_data[6]<< 8) + (unsigned char)msgReply.c_data[7];
		//printf("HV_READ_GLOBAL_STATUS\n");
	}



	   getAddressSpaceLink()->setNbIRQValue(nb_IRQ, OpcUa_Good, UaDateTime::now());
	   getAddressSpaceLink()->setResetRegValue(resetreg, OpcUa_Good, UaDateTime::now());
	   getAddressSpaceLink()->setWarningStatValue(warnstat, OpcUa_Good, UaDateTime::now());
	   getAddressSpaceLink()->setAlarmStatValue(alarstat, OpcUa_Good, UaDateTime::now());
	   getAddressSpaceLink()->setStateValue(statebytevalue, OpcUa_Good, UaDateTime::now());





	getParent()-> freeRequestCONNECTIONSTATUS()	;
	sourceTime = UaDateTime::now();
	return ret;*/
}




}


