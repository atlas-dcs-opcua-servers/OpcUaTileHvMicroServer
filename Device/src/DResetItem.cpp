
/* This is device body stub */


#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DResetItem.h>
#include <ASResetItem.h>
#include <HVcan.h>
#include <DDrawer.h>


#include <iostream>




namespace Device
{



/* sample ctr */
DResetItem::DResetItem (const Configuration::ResetItem & config

                        , DDrawer * parent

                       ):

    m_parent(parent),

    m_index(config.index()),
    m_addressSpaceLink(0),
	m_ResetreplyStatus(0)

/* fill up constructor initialization list here */
{
    /* fill up constructor body here */
}

/* sample dtr */
DResetItem::~DResetItem ()
{
    /* remove children */

}

/* delegators for cachevariables and externalvariables */

/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */




UaStatus DResetItem::readItemValue (
    OpcUa_Double &value,
    UaDateTime &sourceTime
)
{
	return OpcUa_BadNotImplemented;
}


/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */
UaStatus DResetItem::writeItemValue (
    OpcUa_Double &value
)
{


	UaStatus ret;

	msg.c_dlc = 0;
	m_ind = m_index;

	getParent()-> setRequestRESET(this);


	    if (m_ind == HV_RESET) // m_ind is hexadecimal
		{
		      msg.c_data[0] = 0x01;
		      msg.c_data[1] = 0x00;
		      msg.c_dlc = 2;
		      printf("HV_RESET message sent\n");
		}

		    msg.c_id = (0x100 + (getParent()->m_drawerID));
		    printf("msg.c_id = %d \n", msg.c_id);
		    printf("msg.c_dlc = %d \n", msg.c_dlc);
		    for (int y = 0; y< msg.c_dlc; y++)
		    {
		      	 printf("msg.c_data[y] = %d \n", msg.c_data[y]);
		    }

		    getParent()->messageSend(msg);
		    printf("MESSAGE SENT  \n");
		    getParent()->addDelay();
		    printf("MESSAGE RECEIVED  \n");
		    msgReply = getParent()->msgReply;

		    statusValue = msgReply.c_data[0];
		    //getAddressSpaceLink()->setStatusValue(statusValue, OpcUa_Good, UaDateTime::now());
		    //m_ResetreplyStatus = 0;


		    getParent()->freeRequestRESET();

		    return ret;

}


/* address space linker */

void DResetItem::linkAddressSpace( AddressSpace::ASResetItem *addressSpaceLink)
{
    if (m_addressSpaceLink != 0)
    {
        /* signalize nice error from here. */
        abort();
    }
    else
        m_addressSpaceLink = addressSpaceLink;
}

/* add/remove */


/* to safely quit */
void DResetItem::unlinkAllChildren ()
{
    PRINT_LOCATION();
    m_addressSpaceLink = 0;
    /* Fill up: call unlinkAllChildren on all children */


}

/* find methods for children */

void DResetItem::passRESET(const CanMessage rmsg)
{
	msgReply = getParent()->passMSG(rmsg);
}

}


