
/* This is device body stub */


#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DWriteMaxstep.h>
#include <ASWriteMaxstep.h>





namespace Device
{



/* sample ctr */
DWriteMaxstep::DWriteMaxstep (const Configuration::WriteMaxstep & config

                              , DChannelNo * parent

                             ):

    m_parent(parent),

    m_addressSpaceLink(0)

/* fill up constructor initialization list here */
{
    /* fill up constructor body here */
}

/* sample dtr */
DWriteMaxstep::~DWriteMaxstep ()
{
    /* remove children */

}

/* delegators for cachevariables and externalvariables */

/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */
UaStatus DWriteMaxstep::readItemValue (
		OpcUa_Float &value,
    UaDateTime &sourceTime
)
{
    sourceTime = UaDateTime::now();
    return OpcUa_BadNotImplemented;
}

/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */
UaStatus DWriteMaxstep::writeItemValue (
		OpcUa_Float &value
)
{
    return OpcUa_BadNotImplemented;
}


/* address space linker */

void DWriteMaxstep::linkAddressSpace( AddressSpace::ASWriteMaxstep *addressSpaceLink)
{
    if (m_addressSpaceLink != 0)
    {
        /* signalize nice error from here. */
        abort();
    }
    else
        m_addressSpaceLink = addressSpaceLink;
}

/* add/remove */


/* to safely quit */
void DWriteMaxstep::unlinkAllChildren ()
{
    PRINT_LOCATION();
    m_addressSpaceLink = 0;
    /* Fill up: call unlinkAllChildren on all children */


}

/* find methods for children */



}


