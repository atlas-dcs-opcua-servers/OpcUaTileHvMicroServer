
/* This is device body stub */


#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DAddressReadItem.h>
#include <ASAddressReadItem.h>
#include <HVcan.h>
#include <DDrawer.h>

#include <iostream>




namespace Device
{



/* sample ctr */
DAddressReadItem::DAddressReadItem (const Configuration::AddressReadItem & config

                                    , DDrawer * parent

                                   ):

    m_parent(parent),

    m_index(config.index()),
    m_addressSpaceLink(0)

/* fill up constructor initialization list here */
{
    /* fill up constructor body here */
}

/* sample dtr */
DAddressReadItem::~DAddressReadItem ()
{
    /* remove children */

}

/* delegators for cachevariables and externalvariables */

/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */

UaStatus DAddressReadItem::readAddress ()
{
	UaDateTime sourceTime;
	UaStatus ret;
		OpcUa_Int16 command = -1;
		msg.c_dlc = 0;
		m_bufferReplySize = 0;
		m_ind = m_index;

		 msg.c_id = (0x100 + (getParent()->m_drawerID));

		 getParent()-> setRequestAddressReadItem(this);


	   if (m_ind == AUTOMATIC_READ_ADDRESS) // m_ind is hexadecimal
	   {
			      msg.c_data[0] = 0xC;
			      msg.c_data[1] = 0x07;
			      msg.c_dlc = 2;
			      printf("HV_INFO_READ HVMICRO SN \n");


			      msg.c_id = (0x100 + (getParent()->m_drawerID));
			      	    printf("msg.c_id = %x \n", msg.c_id);
			      	    printf("msg.c_dlc = %x \n", msg.c_dlc);
			      	    for (int y = 0; y< msg.c_dlc; y++)
			      	    {
			      	      	 printf("msg.c_data[y] = %x \n", msg.c_data[y]);
			      	    }

			      	    getParent()->messageSend(msg);
			      	    printf("MESSAGE SENT  \n");
			      	    getParent()->addDelay();
			      		printf("MESSAGE RECEIVED  \n");


					   		msg.c_data[0] = 0x1A;
							msg.c_data[1] = 00;
							msg.c_data[2] = msgReply.c_data[2];
							msg.c_data[3] = msgReply.c_data[3];
							msg.c_data[4] = msgReply.c_data[4];
							msg.c_data[5] = msgReply.c_data[5];
							msg.c_dlc = 6;
							msg.c_id = (0x100 + (getParent()->m_drawerID));


							printf("msg.c_id = %x \n", msg.c_id);
							printf("msg.c_dlc = %x \n", msg.c_dlc);
							for (int y = 0; y< msg.c_dlc; y++)
							{
							  	 printf("msg.c_data[y] = %x \n", msg.c_data[y]);
							}

								    getParent()->messageSend(msg);
								    printf("MESSAGE SENT  \n");
								    getParent()->addDelay();
									printf("MESSAGE RECEIVED  \n\n");


	   	    printf("msgReply.c_id = %x \n", msgReply.c_id);
		    printf("msgReply.c_dlc = %x \n", msgReply.c_dlc);
		    for (int y = 0; y< msgReply.c_dlc; y++)
		    {
		      	 printf("msgReply.c_data[y] = %x \n", msgReply.c_data[y]);
		    }



		    softAddress =  ((unsigned char)msgReply.c_data[2]<<8) + ((unsigned char)msgReply.c_data[3]);
		    hardAddress =  ((unsigned char)msgReply.c_data[4]<<8) + ((unsigned char)msgReply.c_data[5]);
		    printf("softAddress is %x \n", softAddress);
		    printf("hardAddress is %x \n", hardAddress);



		    printf("msgReply CAME\n");

		    getAddressSpaceLink()->setAddressSoftValue(softAddress, OpcUa_Good, UaDateTime::now());
		    getAddressSpaceLink()->setAddressHardValue(hardAddress, OpcUa_Good, UaDateTime::now());
	   }



		    getParent()->freeRequestAddressRead()	;

	    sourceTime = UaDateTime::now();
	    return ret;

}



UaStatus DAddressReadItem::readItemValue (
    OpcUa_Double &value,
    UaDateTime &sourceTime
)
{
	return OpcUa_BadNotImplemented;
}




/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */
UaStatus DAddressReadItem::writeItemValue (
    OpcUa_Double &value
)
{
    return OpcUa_BadNotImplemented;
}


/* address space linker */

void DAddressReadItem::linkAddressSpace( AddressSpace::ASAddressReadItem *addressSpaceLink)
{
    if (m_addressSpaceLink != 0)
    {
        /* signalize nice error from here. */
        abort();
    }
    else
        m_addressSpaceLink = addressSpaceLink;
}

/* add/remove */


/* to safely quit */
void DAddressReadItem::unlinkAllChildren ()
{
    PRINT_LOCATION();
    m_addressSpaceLink = 0;
    /* Fill up: call unlinkAllChildren on all children */


}

/* find methods for children */

void DAddressReadItem::passADDRESSREAD(const CanMessage rmsg)
{
	msgReply = getParent()-> passMSG(rmsg);
}

}


