/*
 * DCanBus.cpp
 *
 *  Created on: Mar, 2015
 *      Author: pagoncal
 */


/* This is device body stub */


#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DCanBus.h>
#include <ASCanBus.h>


#include <DDrawer.h>
#include <DCrate.h>





#include <iostream>
using namespace std;

#include <CanBusAccess.h>





namespace Device
{




// 1111111111111111111111111111111111111111111111111111111111111111111111111
// 1     GENERATED CODE STARTS HERE AND FINISHES AT SECTION 2              1
// 1     Users don't modify this code!!!!                                  1
// 1     If you modify this code you may start a fire or a flood somewhere,1
// 1     and some human being may possible cease to exist. You don't want  1
// 1     to be charged with that!                                          1
// 1111111111111111111111111111111111111111111111111111111111111111111111111






// 2222222222222222222222222222222222222222222222222222222222222222222222222
// 2     SEMI CUSTOM CODE STARTS HERE AND FINISHES AT SECTION 3            2
// 2     (code for which only stubs were generated automatically)          2
// 2     You should add the implementation but dont alter the headers      2
// 2     (apart from constructor, in which you should complete initializati2
// 2     on list)                                                          2
// 2222222222222222222222222222222222222222222222222222222222222222222222222

/* sample ctr */
DCanBus::DCanBus (
    const Configuration::CanBus& config,
    Parent_DCanBus* parent
):
    Base_DCanBus( config

                  , parent),

        		m_hardwareComponentName(config.type()),
    	    m_canPortID(config.port()),
    	    m_canPortOptions(config.speed()),
    	    m_canBusAccess(0)


/* fill up constructor initialization list here */
{
    /* fill up constructor body here */
}

/* sample dtr */
DCanBus::~DCanBus ()
{
}

/* delegators for cachevariables and externalvariables */



// 3333333333333333333333333333333333333333333333333333333333333333333333333
// 3     FULLY CUSTOM CODE STARTS HERE                                     3
// 3     Below you put bodies for custom methods defined for this class.   3
// 3     You can do whatever you want, but please be decent.               3
// 3333333333333333333333333333333333333333333333333333333333333333333333333


void DCanBus::openCanBus ()
{
	printf("Inside the opening the ports function \n");
	std::cout << __PRETTY_FUNCTION__ << std::endl;

	m_canBusAccess = CanBusAccess::getInstance ()->openCanBus(m_hardwareComponentName+":"+m_canPortID,m_canPortOptions);
	printf("after open the port on \n");
	if (!m_canBusAccess)
		throw std::runtime_error("Unable to open CAN bus");
	m_canBusAccess->canMessageCame.connect ( boost::bind( &DCanBus::handleMessage, this, _1 ) );

}


void DCanBus::handleMessage (const CanMessage & msg)
{

	/*printf("CANBUS HERE  \n");
	for(int i = 0; i < msg.c_dlc; i++)
			{
				printf("data[%d]: %x\n",i, msg.c_data[i]);
			}*/

	//std::cout << __PRETTY_FUNCTION__ << std::endl;
	int nodeId = getNodeId(msg);
	if (nodeId > 0)
	{	//printf("nodeId of drawer number is : %x\n", nodeId );
		/* Should be handled by specific object */
		DDrawer* node = getDrawerByDrawerID(nodeId);
		DCrate* crate = getCrateByCrateID(nodeId);
		
		if (node)
		{
			node->handleMessage(msg);
		}
		else if(crate)
		{
			crate->handleMessage(msg);
		}
		else
		{
		}
		//m_deviceID = nodeId;
		//std::cout << "m_deviceID is "<< m_deviceID << std::endl;
	}
	else
	{
		std::cout << "Not implemented: message came for everybody" << std::endl;
	}


}

void DCanBus::sendMessage ( const CanMessage & msg)
{
	unsigned long cobId = msg.c_id;
	if (cobId > 0xffff)
		cout << __PRETTY_FUNCTION__ << " Big cobid not supported (maybe RTR was intended ? ) " << endl;
	else
		m_canBusAccess->sendMessage( msg.c_id, msg.c_dlc, (unsigned char*)msg.c_data );
}




void DCanBus::sendRTR ( unsigned int cobId)
{
	if (cobId > 0xffff)
		cout << __PRETTY_FUNCTION__ << " Big cobid not supported (maybe RTR was intended ? ) " << endl;
	else
		m_canBusAccess->sendRemoteRequest( cobId );
}



}


