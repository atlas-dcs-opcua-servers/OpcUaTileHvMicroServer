/*
 * CommunicationObject.cpp
 *
 *  Created on: Mar 9, 2015
 *      Author: pnikiel
 */

#include <CommunicationObject.h>
#include <iostream>

namespace Device

{

CommunicationObject::CommunicationObject( 
		int type,
		unsigned char commandIndex,
		unsigned char nodeID,
		const unsigned char * arguments,
		unsigned int argumentsLen,
		HandlerType handler,
		float maxWaitingTime ):
				m_HardwareType (type),
				m_commandIndex (commandIndex),
				m_maxWaitingTime (maxWaitingTime),
				m_handler( handler )
{

	if (argumentsLen > 0)
	{
		if (argumentsLen > MAX_LEN_ARGUMENTS)
			throw std::runtime_error("Given arguments len too big!");
		m_arguments.assign( argumentsLen, 0 );
		for (unsigned int i=0; i<argumentsLen; i++)
		{
			m_arguments[i] = arguments[i];
		}
	}

}

CommunicationObject::CommunicationObject(
		int type,
		unsigned char commandIndex,
		unsigned char nodeID,
		unsigned char arg,
		HandlerType handler,
		float maxWaitingTime ):
		m_HardwareType (type),
		m_commandIndex( commandIndex ),
		m_maxWaitingTime( maxWaitingTime ),
		m_arguments(1, arg),
		m_handler( handler )
{


}



void CommunicationObject::requestToCanMessage ( CanMessage & msg )
{
	msg.c_data[0] = m_commandIndex;
	for (int i=0; i<m_arguments.size(); i++)
		msg.c_data[1+i] = m_arguments[i];
	msg.c_dlc = 1 + m_arguments.size();
	msg.c_id = nodeID;
}

void CommunicationObject::handleReply ( const CanMessage & msg, bool valid )
{
	
	if (m_handler)
		m_handler (msg.c_data, msg.c_dlc, valid);



}





}



