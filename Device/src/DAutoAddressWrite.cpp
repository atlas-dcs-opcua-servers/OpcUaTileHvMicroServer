
/* This is device body stub */


#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DAutoAddressWrite.h>
#include <ASAutoAddressWrite.h>

#include <HVcan.h>



namespace Device
{



/* sample ctr */
DAutoAddressWrite::DAutoAddressWrite (const Configuration::AutoAddressWrite & config

                                      , DDrawer * parent

                                     ):

		m_parent(parent),

		m_index(config.index()),
		m_addressSpaceLink(0)

/* fill up constructor initialization list here */
{
    /* fill up constructor body here */
}

/* sample dtr */
DAutoAddressWrite::~DAutoAddressWrite ()
{
    /* remove children */

}

/* delegators for cachevariables and externalvariables */



UaStatus DAutoAddressWrite::writeAddressSoftValue ( const OpcUa_Int32 & v)
{
	UaStatus ret;
		msg.c_dlc = 0;
		m_ind = m_index;

		getParent()-> setRequestADDRESSWRITE(this);


		if ((0xFF & (m_ind>>8)) == AUTOMATIC_WRITE_NEW_ADDRESS) // m_ind is hexadecimal
		{
		      msg.c_data[0] = 0xC;
		      msg.c_data[1] = 0x07;
		      msg.c_dlc = 2;

		      printf("HV_INFO_READ HVMICRO SN \n");
		      getParent()->messageSend(msg);
	    	  printf("MESSAGE SENT  \n");
	    	  getParent()->addDelay();
	    	  printf("MESSAGE RECEIVED  \n");


			  for (int y = 0; y< msgReply.c_dlc; y++)
			  {
			    	printf("Reply Message: msg.c_data[%d] = %hhx \n", y, msgReply.c_data[y]) ;
			  }
			  printf("Reply Message size is : %hhx \n", msgReply.c_dlc) ;

				    		msg.c_data[0] = 0x1B;
							msg.c_data[1] = 0x00;
							msg.c_data[2] = msgReply.c_data[2];
							msg.c_data[3] = msgReply.c_data[3];
							msg.c_data[4] = msgReply.c_data[4];
							msg.c_data[5] = msgReply.c_data[5];
							msg.c_data[6] = (((int)(v)) & 0x00FF00)>>8;
							msg.c_data[7] = (((int)(v)) & 0x0000FF);
						  msg.c_dlc = 8 ;

						  getParent()->messageSend(msg);
						  printf("MESSAGE SENT  \n");
						  getParent()->addDelay();
						  printf("MESSAGE RECEIVED  \n");


					statebyteWrite = msgReply.c_data[0];
					SoftAddress = (msgReply.c_data[2]<<8) + msgReply.c_data[3];
					HardAddress = (msgReply.c_data[4]<<8) + msgReply.c_data[5];
					NewSoftAddress = (msgReply.c_data[6]<<8) + msgReply.c_data[7];

					printf("HV_AUTOMATIC_ADDRESS_WRITE");


					getAddressSpaceLink()->setNewSoftAddressValue(NewSoftAddress, OpcUa_Good, UaDateTime::now());
					getAddressSpaceLink()->setAddressSoftValue(SoftAddress, OpcUa_Good, UaDateTime::now());
					getAddressSpaceLink()->setAddressHardValue(HardAddress, OpcUa_Good, UaDateTime::now());


				    }


		getParent()->freeRequestADDRESSWRITE()	;


	    return OpcUa_Good;
}


/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */
UaStatus DAutoAddressWrite::readItemValue (
		OpcUa_Float &value,
    UaDateTime &sourceTime
)
{
    sourceTime = UaDateTime::now();
    return OpcUa_BadNotImplemented;
}

/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */
UaStatus DAutoAddressWrite::writeItemValue (
		OpcUa_Float &value
)
{
	return OpcUa_BadNotImplemented;

}


/* address space linker */

void DAutoAddressWrite::linkAddressSpace( AddressSpace::ASAutoAddressWrite *addressSpaceLink)
{
    if (m_addressSpaceLink != 0)
    {
        /* signalize nice error from here. */
        abort();
    }
    else
        m_addressSpaceLink = addressSpaceLink;
}

/* add/remove */


/* to safely quit */
void DAutoAddressWrite::unlinkAllChildren ()
{
    PRINT_LOCATION();
    m_addressSpaceLink = 0;
    /* Fill up: call unlinkAllChildren on all children */


}

/* find methods for children */

void DAutoAddressWrite::passADDRESSWRITE(const CanMessage rmsg)
{
	msgReply = getParent()-> passMSG(rmsg);
}

}


