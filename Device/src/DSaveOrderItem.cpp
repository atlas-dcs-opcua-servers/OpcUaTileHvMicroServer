
/* This is device body stub */


#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DSaveOrderItem.h>
#include <ASSaveOrderItem.h>

#include <HVcan.h>
#include <DDrawer.h>
#include <DChannelNo.h>
#include <ASChannelNo.h>
#include <iostream>



namespace Device
{



/* sample ctr */
DSaveOrderItem::DSaveOrderItem (const Configuration::SaveOrderItem & config

                                , DChannelNo * parent

                               ):

    m_parent(parent),

    m_index(config.index()),
    m_addressSpaceLink(0)

/* fill up constructor initialization list here */
{
    /* fill up constructor body here */
}

/* sample dtr */
DSaveOrderItem::~DSaveOrderItem ()
{
    /* remove children */

}

/* delegators for cachevariables and externalvariables */

/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */
UaStatus DSaveOrderItem::readItemValue (
    OpcUa_Double &value,
    UaDateTime &sourceTime
)
{
    	UaStatus ret;
		getParent()->getParent()-> setRequestSAVEORDER(this);

		msg.c_dlc = 0;
		m_ind = m_index;

		if (m_ind == HV_SAVE_ORDER) // m_ind is hexadecimal
	    {
	      msg.c_data[0] = 0x09;
	      msg.c_data[1] = getParent()->m_channelNumber;
	      msg.c_dlc = 2;
	      printf("HV_SAVE_ORDER\n");
	    }

	    msg.c_id = (0x100 + (getParent()->getParent()->m_drawerID));
	   	printf("msg.c_id = %x \n", msg.c_id);
	   	printf("msg.c_dlc = %x \n", msg.c_dlc);
	   	for (int y = 0; y< msg.c_dlc; y++)
	   	{
	   	    printf("msg.c_data[y] = %x \n", msg.c_data[y]);
	   	}

	   	getParent()->getParent()->messageSend(msg);
	   	printf("MESSAGE SENT  \n");
	   	getParent()->getParent()->addDelay();
	   	printf("MESSAGE RECEIVED  \n");
	   	msgReply = getParent()->getParent()->msgReply;

	    statusValue = msgReply.c_data[0];
	    getAddressSpaceLink()->setStatusValue(statusValue, OpcUa_Good, UaDateTime::now());

	    getParent()->getParent()->freeRequestSAVEORDER();

    sourceTime = UaDateTime::now();
    return ret;
}



/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */
UaStatus DSaveOrderItem::writeItemValue (
    OpcUa_Double &value
)
{
    return OpcUa_BadNotImplemented;
}


/* address space linker */

void DSaveOrderItem::linkAddressSpace( AddressSpace::ASSaveOrderItem *addressSpaceLink)
{
    if (m_addressSpaceLink != 0)
    {
        /* signalize nice error from here. */
        abort();
    }
    else
        m_addressSpaceLink = addressSpaceLink;
}

/* add/remove */


/* to safely quit */
void DSaveOrderItem::unlinkAllChildren ()
{
    PRINT_LOCATION();
    m_addressSpaceLink = 0;
    /* Fill up: call unlinkAllChildren on all children */


}

/* find methods for children */

void DSaveOrderItem::passSAVEORDER(const CanMessage rmsg)
{
	msgReply = getParent()->getParent()-> passMSG(rmsg);
}


}


