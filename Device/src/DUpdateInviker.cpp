#include <iostream>
#include <boost/foreach.hpp>

#include <DRoot.h>
#include <DUpdateInviker.h>
#include <DDrawer.h>
#include <DCanBus.h>

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>




namespace Device
{

DUpdateInvoker::DUpdateInvoker ():
			 m_exitFlag(0)
{
}

void DUpdateInvoker::run()
{
	while (!m_exitFlag)
	{
		UaThread::sleep(15);
		std::cout << "reading channels" << std::endl;
		BOOST_FOREACH(DCanBus* h,  DRoot::getInstance()->canbuss ())
		{
			std::cout << "inside the boost" << std::endl;
			h->readcanBus();
		}
	}
	std::cout << "thread exiting because of handler" << std::endl;
}

}
