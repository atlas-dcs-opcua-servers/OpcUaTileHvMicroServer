
/*
 * DChannel.cpp
 *
 *  Created on: Mar, 2015
 *      Author: pagoncal
 */

#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DChannel.h>
#include <ASChannel.h>
#include <ASDrawer.h>

#include <DDrawer.h>


namespace Device
{




// 1111111111111111111111111111111111111111111111111111111111111111111111111
// 1     GENERATED CODE STARTS HERE AND FINISHES AT SECTION 2              1
// 1     Users don't modify this code!!!!                                  1
// 1     If you modify this code you may start a fire or a flood somewhere,1
// 1     and some human being may possible cease to exist. You don't want  1
// 1     to be charged with that!                                          1
// 1111111111111111111111111111111111111111111111111111111111111111111111111






// 2222222222222222222222222222222222222222222222222222222222222222222222222
// 2     SEMI CUSTOM CODE STARTS HERE AND FINISHES AT SECTION 3            2
// 2     (code for which only stubs were generated automatically)          2
// 2     You should add the implementation but dont alter the headers      2
// 2     (apart from constructor, in which you should complete initializati2
// 2     on list)                                                          2
// 2222222222222222222222222222222222222222222222222222222222222222222222222

/* sample ctr */
DChannel::DChannel (
    const Configuration::Channel& config,
    Parent_DChannel* parent
):
    Base_DChannel( config, parent)

/* fill up constructor initialization list here */
{
    /* fill up constructor body here */
    
	getParent()->getCommunicationController()->addPeriodicObject( PeriodicCommunicationObject(
	CommunicationObject(HV_Parameter, HV_CHAN_READ, channelNumber(), channelNumber(), boost::bind(&DChannel::handleReadChannelReply, this, _1, _2, _3), /*timeout*/ 1.0 ),
					/* period */ 10.0 ));

}

/* sample dtr */
DChannel::~DChannel ()
{
}

/* delegators for cachevariables and externalvariables */

/* Note: never directly call this function. */



UaStatus DChannel::writeWriteOrder ( const OpcUa_Float & v)
{
	OpcUa_Int16 value;
	OpcUa_Float reference;

 	if (v < 0 || v > HVPSU_MAX)
 		{return OpcUa_BadOutOfRange;}

	reference = returnReference(channelNumber());
	value = v*reference;


	unsigned char channel = channelNumber();
	unsigned char list[] = {channel,
							(unsigned char)((value & 0xFF00)>> 8 ),
							(unsigned char)((value & 0x00FF))};

	CommunicationObject request (HV_Parameter, HV_WRITE_ORDER, channelNumber(), list, sizeof(list), boost::bind(&DChannel::handleWriteChannelReply, this, _1, _2, _3), 1.0 );
	getParent()->getCommunicationController()->requestObject( request, /* highest prio */true );


	return OpcUa_Good;

}

/* Note: never directly call this function. */

UaStatus DChannel::writeWriteMaxstep ( const OpcUa_Float & v)
{
 	OpcUa_Int16 value;
 	OpcUa_Float reference;

 	if (v < 0 || v > HVPSU_MAX)
 		{return OpcUa_BadOutOfRange;}


		reference = returnReference(channelNumber());
		value = v*reference;

		unsigned char channel = channelNumber();
		unsigned char list[] = {channel,
				(unsigned char)((value & 0xFF00)>> 8 ),
				(unsigned char)((value & 0x00FF))};

		CommunicationObject request (HV_Parameter, HV_WRITE_MAXSTEP, channelNumber(), list, sizeof(list), boost::bind(&DChannel::handleWriteItems, this, _1, _2, _3), 1.0 );
		getParent()->getCommunicationController()->requestObject( request, /* highest prio */true );

		return OpcUa_Good;


}

/* Note: never directly call this function. */

UaStatus DChannel::writeSaveOrder ( const OpcUa_Boolean & v)
{
 	if (v != true)
 		{return OpcUa_BadOutOfRange;}

 	CommunicationObject request (HV_Parameter, HV_SAVE_ORDER, channelNumber(), channelNumber(), boost::bind(&DChannel::handleWriteItems, this, _1, _2, _3), 1.0 );
	getParent()->getCommunicationController()->requestObject( request, /* highest prio */true );

	return OpcUa_Good;

}

/* Note: never directly call this function. */

UaStatus DChannel::writeRestoreOrder ( const OpcUa_Boolean & v)
{
 	if (v != true)
 		{return OpcUa_BadOutOfRange;}

	CommunicationObject request (HV_Parameter, HV_RESTORE_ORDER, channelNumber(), channelNumber(), boost::bind(&DChannel::handleWriteItems, this, _1, _2, _3), 1.0 );
	getParent()->getCommunicationController()->requestObject( request, /* highest prio */true );

	return OpcUa_Good;

}

/* Note: never directly call this function. */

UaStatus DChannel::writeCalibrationTrigger ( const OpcUa_Boolean & v)
{
 	if (v != true)
 		{return OpcUa_BadOutOfRange;}

 	unsigned char channel = channelNumber();
	unsigned char list[] = {channel, 0, 0};
	//unsigned char DAC = 0;
	//unsigned char ADC = 1;
	list[1] = 0;
	list[2] = 0;

	CommunicationObject requestDACx0 (HV_Parameter, HV_READ_CALIBRATIO_PARAMETERS, channelNumber(), list, sizeof(list), boost::bind(&DChannel::handleReadCalibrationsDACX0Reply, this, _1, _2, _3), 1.0 );
	getParent()->getCommunicationController()->requestObject( requestDACx0, /* highest prio */true );

	list[2] = 1;
	CommunicationObject requestDACx1 (HV_Parameter, HV_READ_CALIBRATIO_PARAMETERS, channelNumber(), list, sizeof(list),boost::bind(&DChannel::handleReadCalibrationsDACX1Reply, this, _1, _2, _3), 1.0 );
	getParent()->getCommunicationController()->requestObject( requestDACx1, /* highest prio */true );

	list[2] = 2;
	CommunicationObject requestDACx2 (HV_Parameter, HV_READ_CALIBRATIO_PARAMETERS, channelNumber(), list, sizeof(list),boost::bind(&DChannel::handleReadCalibrationsDACX2Reply, this, _1, _2, _3), 1.0 );
	getParent()->getCommunicationController()->requestObject( requestDACx2, /* highest prio */true );


	list[1] = 1;
	list[2] = 0;
	CommunicationObject requestADCx0 (HV_Parameter, HV_READ_CALIBRATIO_PARAMETERS, channelNumber(), list, sizeof(list),boost::bind(&DChannel::handleReadCalibrationsADCX0Reply, this, _1, _2, _3), 1.0 );
	getParent()->getCommunicationController()->requestObject( requestADCx0, /* highest prio */true );

	list[2] = 1;
	CommunicationObject requestADCx1 (HV_Parameter, HV_READ_CALIBRATIO_PARAMETERS, channelNumber(), list, sizeof(list),boost::bind(&DChannel::handleReadCalibrationsADCX1Reply, this, _1, _2, _3), 1.0 );
	getParent()->getCommunicationController()->requestObject( requestADCx1, /* highest prio */true );

	list[2] = 2;
	CommunicationObject requestADCx2 (HV_Parameter, HV_READ_CALIBRATIO_PARAMETERS, channelNumber(), list, sizeof(list),boost::bind(&DChannel::handleReadCalibrationsADCX2Reply, this, _1, _2, _3), 1.0 );
	getParent()->getCommunicationController()->requestObject( requestADCx2, /* highest prio */true );

	return OpcUa_Good;


}



// 3333333333333333333333333333333333333333333333333333333333333333333333333
// 3     FULLY CUSTOM CODE STARTS HERE                                     3
// 3     Below you put bodies for custom methods defined for this class.   3
// 3     You can do whatever you want, but please be decent.               3
// 3333333333333333333333333333333333333333333333333333333333333333333333333





OpcUa_Float DChannel::returnReference(OpcUa_Int32 channel)
{
	OpcUa_Float referenceValue;

	if((channelNumber() >= 0x3C) & (channelNumber() <= 0x42))
	{
		referenceValue = 1000.0;
	}
	else if((channelNumber() >= 0x43) & (channelNumber() <= 0x44))
	{
		referenceValue = 1;
	}
	else
	{
		referenceValue = 10.0;
	}

	return referenceValue;
}


UaStatus DChannel::handleReadChannelReply ( const unsigned char reply[], unsigned int msgLen, bool valid)

{

	OpcUa_Float appliedValue;
	OpcUa_Float orderValue;
	OpcUa_Float maxstepValue;
	OpcUa_Float stateValue;
	OpcUa_Float	statusValue;
	OpcUa_Float reference;

	signed char buff1 = reply[6];
	signed char buff2 = reply[7];


	// TODO: add a constant for zero!
	if(reply[0] != 0 || msgLen != 8 || valid == false)
	{
		getAddressSpaceLink()->setNullAppliedValue(OpcUa_Bad );
		getAddressSpaceLink()->setNullMaxstepValue(OpcUa_Bad );
		getAddressSpaceLink()->setNullOrderValue(OpcUa_Bad );
		getAddressSpaceLink()->setNullStateValue(OpcUa_Bad );
		getAddressSpaceLink()->setNullStatusValue(OpcUa_Bad );
		getParent()->getAddressSpaceLink()->setConnectionJammed(1, OpcUa_Good);
		return OpcUa_Bad;
	}


	else
	{
		stateValue = reply[0];
		statusValue = reply[1];

		if (statusValue>2)
		  statusValue=statusValue*(-1);
		reference = returnReference(channelNumber());



		appliedValue = (short)(((reply[6]) << 8) + (reply[7]))/reference;
		orderValue = ((reply[2] << 8) + (reply[3]))/reference;
		maxstepValue= ((reply[4] << 8) + (reply[5]))/reference;



		/*for (int y = 0; y< msgLen; y++)
		{
		   	 printf("reply[y] = %x \n", reply[y]);
		}*/

		/*std::cout << "appliedValue is" << appliedValue << std::endl;
		std::cout << "orderValue is " <<  orderValue << std::endl;
		std::cout << "maxstepValue is " <<  maxstepValue << std::endl;
		std::cout << "stateValue is " <<  stateValue << std::endl;
		std::cout << "statusValue is " <<  statusValue << std::endl;*/


		getAddressSpaceLink()->setAppliedValue(appliedValue, OpcUa_Good );
		getAddressSpaceLink()->setMaxstepValue(maxstepValue, OpcUa_Good );
		getAddressSpaceLink()->setOrderValue(orderValue, OpcUa_Good );
		getAddressSpaceLink()->setStateValue(stateValue, OpcUa_Good );
		getAddressSpaceLink()->setStatusValue(statusValue, OpcUa_Good );

		getParent()->getAddressSpaceLink()->setConnectionJammed(reply[0], OpcUa_Good);

		return OpcUa_Good;

	}
}




UaStatus DChannel::handleRestoreOrder ( const unsigned char reply[], unsigned int msgLen, bool valid)

{
	OpcUa_Int16 statebytevalue = reply[0];

	if(statebytevalue != 0 || msgLen != 2 || valid == false)
	{
			// TODO: proper null value
			//getAddressSpaceLink()->setInfoValue(  OpcUa_Bad );
		getParent()->getAddressSpaceLink()->setConnectionJammed(1, OpcUa_Good);
			return OpcUa_Bad;
	}
	else
	{
		getParent()->getAddressSpaceLink()->setConnectionJammed(reply[0], OpcUa_Good);
		return OpcUa_Good;
	}
}


UaStatus DChannel::handleSaveOrder ( const unsigned char reply[], unsigned int msgLen, bool valid)

{
	OpcUa_Int16 statebytevalue = reply[0];

	if(statebytevalue != 0 || msgLen != 2 || valid == false)
	{
			// TODO: proper null value
		getParent()->getAddressSpaceLink()->setConnectionJammed(1, OpcUa_Good);
		return OpcUa_Bad;
	}
	else
	{
		getParent()->getAddressSpaceLink()->setConnectionJammed(reply[0], OpcUa_Good);
		return OpcUa_Good;
	}

}



UaStatus DChannel::handleWriteChannelReply ( const unsigned char reply[], unsigned int msgLen, bool valid)

{

	// TODO: add a constant for zero!
	if(reply[0] != 0 || msgLen != 2 || valid == false)
	{
		getParent()->getAddressSpaceLink()->setConnectionJammed(1, OpcUa_Good);
		return OpcUa_Bad;
	}

	else
	{

		getParent()->getAddressSpaceLink()->setConnectionJammed(reply[0], OpcUa_Good);
		return OpcUa_Good;
	}

}




UaStatus DChannel::handleWriteItems ( const unsigned char reply[], unsigned int msgLen, bool valid)
{
	OpcUa_Int16 statebytevalue = reply[0];

		if(statebytevalue != 0 || msgLen != 2 || valid == false)
		{
				// TODO: proper null value
				//getAddressSpaceLink()->setInfoValue(  OpcUa_Bad);
			getParent()->getAddressSpaceLink()->setConnectionJammed(1, OpcUa_Good);
			return OpcUa_Bad;
		}
		else
		{
			getParent()->getAddressSpaceLink()->setConnectionJammed(reply[0], OpcUa_Good);
			return OpcUa_Good;
		}
}













UaStatus DChannel::handleReadCalibrationsDACX0Reply ( const unsigned char reply[], unsigned int msgLen, bool valid)

{
	OpcUa_Float stateValue;
	OpcUa_Int16 buff1;
	OpcUa_Int16 buff2;
	OpcUa_Float value;


	// TODO: add a constant for zero!
	if(reply[0] != 0 || msgLen != 6 || valid == false)
	{
		getAddressSpaceLink()->setNullCalibrationDACX0(  OpcUa_Bad );
		getParent()->getAddressSpaceLink()->setConnectionJammed(1, OpcUa_Good);
		return OpcUa_Bad;
	}

	else
	{
		stateValue = reply[0];
		buff1 = (reply[2] << 8)+(reply[3]);
		buff2 = (reply[4] << 8)+(reply[5]);
		value = ((buff1 << 16)+(buff2))/1.0;
		getAddressSpaceLink()->setCalibrationDACX0(value, OpcUa_Good );
		getParent()->getAddressSpaceLink()->setConnectionJammed(reply[0], OpcUa_Good);
		return OpcUa_Good;
	}

}

UaStatus DChannel::handleReadCalibrationsDACX1Reply ( const unsigned char reply[], unsigned int msgLen, bool valid)

{
	OpcUa_Float stateValue;
	OpcUa_Int16 buff1;
	OpcUa_Int16 buff2;
	OpcUa_Int32 value;
	// TODO: add a constant for zero!

	if(reply[0] != 0 || msgLen != 6 || valid == false)
	{
		getAddressSpaceLink()->setNullCalibrationDACX1(  OpcUa_Bad );
		getParent()->getAddressSpaceLink()->setConnectionJammed(1, OpcUa_Good);
		return OpcUa_Bad;
	}

	else
	{
		stateValue = reply[0];
		buff1 = (reply[2] << 8)+(reply[3]);
		buff2 = (reply[4] << 8)+(reply[5]);
		value = (buff1 << 16)+(buff2);
		getAddressSpaceLink()->setCalibrationDACX1(value, OpcUa_Good );
		getParent()->getAddressSpaceLink()->setConnectionJammed(reply[0], OpcUa_Good);
		return OpcUa_Good;
	}

}


UaStatus DChannel::handleReadCalibrationsDACX2Reply ( const unsigned char reply[], unsigned int msgLen, bool valid)

{
	OpcUa_Float stateValue;
	OpcUa_Int16 buff1;
	OpcUa_Int16 buff2;
	OpcUa_Int32 value;
	// TODO: add a constant for zero!
	if(reply[0] != 0 || msgLen != 6 || valid == false)
	{
		getAddressSpaceLink()->setNullCalibrationDACX2(  OpcUa_Bad );
		getParent()->getAddressSpaceLink()->setConnectionJammed(1, OpcUa_Good);
		return OpcUa_Bad;
	}

	else
	{
		stateValue = reply[0];
		buff1 = (reply[2] << 8)+(reply[3]);
		buff2 = (reply[4] << 8)+(reply[5]);
		value = (buff1 << 16)+(buff2);
		getAddressSpaceLink()->setCalibrationDACX2(value, OpcUa_Good );
		getParent()->getAddressSpaceLink()->setConnectionJammed(reply[0], OpcUa_Good);
		return OpcUa_Good;
	}

}





UaStatus DChannel::handleReadCalibrationsADCX0Reply ( const unsigned char reply[], unsigned int msgLen, bool valid)

{
	OpcUa_Float stateValue;
	OpcUa_Int16 buff1;
	OpcUa_Int16 buff2;
	OpcUa_Int32 value;
	// TODO: add a constant for zero!
	if(reply[0] != 0 || msgLen != 6 || valid == false)
	{
		getAddressSpaceLink()->setNullCalibrationADCX0(  OpcUa_Bad );
		getParent()->getAddressSpaceLink()->setConnectionJammed(1, OpcUa_Good);
		return OpcUa_Bad;
	}

	else
	{
		stateValue = reply[0];
		buff1 = (reply[2] << 8)+(reply[3]);
		buff2 = (reply[4] << 8)+(reply[5]);
		value = (buff1 << 16)+(buff2);
		getAddressSpaceLink()->setCalibrationADCX0(value, OpcUa_Good );
		getParent()->getAddressSpaceLink()->setConnectionJammed(reply[0], OpcUa_Good);
		return OpcUa_Good;
	}

}

UaStatus DChannel::handleReadCalibrationsADCX1Reply ( const unsigned char reply[], unsigned int msgLen, bool valid)

{
	OpcUa_Float stateValue;
	OpcUa_Int16 buff1;
	OpcUa_Int16 buff2;
	OpcUa_Int32 value;
	// TODO: add a constant for zero!
	if(reply[0] != 0 || msgLen != 6 || valid == false)
	{
		getAddressSpaceLink()->setNullCalibrationADCX1(  OpcUa_Bad );
		getParent()->getAddressSpaceLink()->setConnectionJammed(1, OpcUa_Good);
		return OpcUa_Bad;
	}

	else
	{
		stateValue = reply[0];
		buff1 = (reply[2] << 8)+(reply[3]);
		buff2 = (reply[4] << 8)+(reply[5]);
		value = (buff1 << 16)+(buff2);
		getAddressSpaceLink()->setCalibrationADCX1(value, OpcUa_Good );
		getParent()->getAddressSpaceLink()->setConnectionJammed(reply[0], OpcUa_Good);
		return OpcUa_Good;
	}

}


UaStatus DChannel::handleReadCalibrationsADCX2Reply ( const unsigned char reply[], unsigned int msgLen, bool valid)

{
	OpcUa_Float stateValue;
	OpcUa_Int16 buff1;
	OpcUa_Int16 buff2;
	OpcUa_Int32 value;
	// TODO: add a constant for zero!
	if(reply[0] != 0 || msgLen != 6 || valid == false)
	{
		getAddressSpaceLink()->setNullCalibrationADCX2(  OpcUa_Bad );
		getParent()->getAddressSpaceLink()->setConnectionJammed(1, OpcUa_Good);
		return OpcUa_Bad;
	}

	else
	{
		stateValue = reply[0];
		buff1 = (reply[2] << 8)+(reply[3]);
		buff2 = (reply[4] << 8)+(reply[5]);
		value = (buff1 << 16)+(buff2);
		getAddressSpaceLink()->setCalibrationADCX2(value, OpcUa_Good );
		getParent()->getAddressSpaceLink()->setConnectionJammed(reply[0], OpcUa_Good);
		return OpcUa_Good;
	}

}






}


