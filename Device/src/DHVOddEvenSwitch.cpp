
/* This is device body stub */


#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DHVOddEvenSwitch.h>
#include <ASHVOddEvenSwitch.h>

#include <HVcan.h>
#include <DDrawer.h>
#include <iostream>



namespace Device
{



/* sample ctr */
DHVOddEvenSwitch::DHVOddEvenSwitch (const Configuration::HVOddEvenSwitch & config

                                    , DDrawer * parent

                                   ):

		m_parent(parent),
		m_switchValue(config.switchValue()),
				    m_index(config.index()),
				    m_addressSpaceLink(0)

/* fill up constructor initialization list here */
{
    /* fill up constructor body here */
}

/* sample dtr */
DHVOddEvenSwitch::~DHVOddEvenSwitch ()
{
    /* remove children */

}

/* delegators for cachevariables and externalvariables */

/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */





UaStatus DHVOddEvenSwitch::writeValValue ( const OpcUa_Int32 & v)
{

	// TODO
//	CommunicationObject object (HV_RESET, "\0", 1, 2);
//	getParent()->getCommunicationController()->addObject ( object );


	UaStatus ret;
			msg.c_dlc = 0;
			m_ind = m_index;

			getParent()->setRequestODDEVEN(this);


			 if ((0xFF & (m_ind>>8))== HV_ODDEVEN_SWITCH ) // m_ind is hexadecimal
				    {
				 msg.c_data[0] = 0x13;
				 msg.c_data[1] = m_switchValue;
				 msg.c_data[2] = ((int)(v) & 0x00FF00)>>8;
				 msg.c_data[3] = ((int)(v) & 0x0000FF);
				 msg.c_dlc = 4;
				      printf("HV_ODD/EVEN_SWITCH \n");
					Switch = msg.c_data[1];
					val = (msg.c_data[2]<<8)+(msg.c_data[3]);




					msg.c_id = (0x100 + (getParent()->m_drawerID));

						    printf("msg.c_id = %x \n", msg.c_id);
						    printf("msg.c_dlc = %x \n", msg.c_dlc);
						    for (int y = 0; y< msg.c_dlc; y++)
						    {
						      	 printf("msg.c_data[y] = %x \n", msg.c_data[y]);
						    }


					   		getParent()->messageSend(msg);
					   	    printf("MESSAGE SENT  \n");
					   	    getParent()->addDelay();
					   		printf("MESSAGE RECEIVED  \n");
					   		msgReply = getParent()->msgReply;


						    statusValue = msgReply.c_data[0];
						    //getAddressSpaceLink()->set....(statusValue, OpcUa_Good, UaDateTime::now());

				    }



			 getParent()->freeRequestODDEVEN();

			printf("Sent and all done \n");
			return ret;

}





UaStatus DHVOddEvenSwitch::readItemValue (
    OpcUa_Float &value,
    UaDateTime &sourceTime
)
{
    sourceTime = UaDateTime::now();
    return OpcUa_BadNotImplemented;
}

/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */
UaStatus DHVOddEvenSwitch::writeItemValue (
    OpcUa_Float &value
)
{
	return OpcUa_BadNotImplemented;
	}


/* address space linker */

void DHVOddEvenSwitch::linkAddressSpace( AddressSpace::ASHVOddEvenSwitch *addressSpaceLink)
{
    if (m_addressSpaceLink != 0)
    {
        /* signalize nice error from here. */
        abort();
    }
    else
        m_addressSpaceLink = addressSpaceLink;
}

/* add/remove */


/* to safely quit */
void DHVOddEvenSwitch::unlinkAllChildren ()
{
    PRINT_LOCATION();
    m_addressSpaceLink = 0;
    /* Fill up: call unlinkAllChildren on all children */


}

/* find methods for children */



void DHVOddEvenSwitch::passHVODDEVENSWITCH(const CanMessage rmsg)
{
	msgReply = getParent()-> passMSG(rmsg);
}



}


