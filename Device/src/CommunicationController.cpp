/*
 * CommunicationController.cpp
 *
 *  Created on: Mar 9, 2015
 *      Author: pnikiel
 */

#include <DDrawer.h>
#include <DCrate.h>
#include <HVcan.h>
#include <CommunicationController.h>
#include <CommunicationObject.h>

#include <iostream>

#include <boost/foreach.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>

#include <list>

namespace Device
{

/*
template <class T>
CommunicationController<T>::CommunicationController ( DDrawer * drawer ):
		m_state (IDLE),
		m_drawer (drawer)
{



}


template <class T>
CommunicationController<T>::CommunicationController ( DCrate * crate ):
		m_state (IDLE),
		m_crate (crate)
{



}*/

template <class T> 
CommunicationController<T>::CommunicationController ( T* device ):
		m_state (IDLE),
		m_device (device)
{



}


template <class T> void CommunicationController<T>::tick ()
{
	boost::interprocess::scoped_lock<boost::mutex> lock (m_accessLock);
	//std::cout << __PRETTY_FUNCTION__ << std::endl;

	/* Part 1. Request periodically-configured objects */
	BOOST_FOREACH( PeriodicCommunicationObject &o, m_periodicObjects )
	{
		if (o.periodPassed())
		{
			o.onFire();
			// copy this object to requested objects
			// TODO remove this old code below
			//m_requestedObjects.push_back( boost::shared_ptr<CommunicationObject> ( new CommunicationObject( o.getCommunicationObject() )));
      //
      // TODO : debug!
			requestObjectInternal( o.getCommunicationObject(), /* high-prio*/ false );
		}
	}

	/* Part 2. If the controller is IDLE and there are things to do, trigger them. */
	switch (m_state)
	{

	case IDLE:
	{
		if (m_requestedObjects.size() > 0)
		{
			startNewTransaction() ;
		}

	}
	break;
	case WAITING_REPLY:
	{
		/* Maybe the ongoing operation takes too much time and we have a timeout? */
		timeval tnow;
		gettimeofday (&tnow, 0);
		float timeSinceRequest = subtractTimeval( m_lastRequestTime, tnow );
		//std::cout << "since request: " << timeSinceRequest << std::endl;

		if (timeSinceRequest > m_currentObject->getMaxWaitingTime() )
		{
			// timeout condition happened


			CanMessage msg;

			// TODO we wanted to have separate byte buffers, not a msg !
			m_currentObject->handleReply( msg, false );

			m_state = IDLE;
			// take next request to be sent away
			startNewTransaction() ;


		}
	}

		break;
	default:
		throw std::runtime_error ("Logic error");

	}

}
template <class T> void CommunicationController<T>::handleMessage ( const CanMessage & msg )
{
	boost::interprocess::scoped_lock<boost::mutex> lock (m_accessLock);
	// TODO check whether COB is is appropriate (0x200 ..)


	if (m_state != WAITING_REPLY)
	{
		// TODO error-handling?
		return;
	}

	//std::cout << "Answer came for the transaction " << std::endl;
	m_currentObject->handleReply( msg, true );
	m_state = IDLE;
	startNewTransaction();

	// TODO pick the next

}

template <class T> bool CommunicationController<T>::startNewTransaction ()
{
	if (m_requestedObjects.size() < 1)
		return false;
	/* Take out one object from the todo list */
	m_currentObject = m_requestedObjects.front() ;
	m_requestedObjects.pop_front();
	/* And send out its request */
	CanMessage request;
	m_currentObject->requestToCanMessage( request );
	// TODO make a function that can compute given id basing on a constant and node-id
	//request.c_id = 0x100 + m_drawer->id();
	//getDrawer()->sendMessage( request );

/*
if(m_currentObject->m_HardwareType == CANPSU_Parameter)
{
	request.c_id = 0x100 + m_crate->id();
	getCrate()->sendMessage( request );
}
else if(m_currentObject->m_HardwareType == HV_Parameter)
{
	request.c_id = 0x100 + m_drawer->id();
	getDrawer()->sendMessage( request );
}
else
{}

*/

	request.c_id = 0x100 + m_device->id();
	getDevice()->sendMessage( request );

	gettimeofday( &m_lastRequestTime, 0 );
	m_state = WAITING_REPLY;
	return true;


}


template <class T> 
bool CommunicationController<T>::requestObject(  const CommunicationObject & object, bool highPrio )
{
	boost::interprocess::scoped_lock<boost::mutex> lock (m_accessLock);
	return requestObjectInternal ( object, highPrio );
}


template <class T> bool CommunicationController<T>::requestObjectInternal (  const CommunicationObject & object, bool highPrio )
{

	BOOST_FOREACH( boost::shared_ptr<CommunicationObject> & inListObject, m_requestedObjects )
	{
		if (*inListObject == object)
		{
			cout << "Object exists -- not adding" << endl;
			// TODO: Jam Condition from  here
			return false;
		}
	}

	if (highPrio)
		m_requestedObjects.push_front(boost::shared_ptr<CommunicationObject> ( new CommunicationObject (object ) ));
	else
		m_requestedObjects.push_back (boost::shared_ptr<CommunicationObject> ( new CommunicationObject (object ) ));

	return true;

}

}



