/*
 * DDrawer.cpp
 *
 *  Created on: Mar, 2015
 *      Author: pagoncal
 */

#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DDrawer.h>
#include <ASDrawer.h>


#include <DChannel.h>


#include <HVcan.h>
#include <CommunicationObject.h>


namespace Device
{




// 1111111111111111111111111111111111111111111111111111111111111111111111111
// 1     GENERATED CODE STARTS HERE AND FINISHES AT SECTION 2              1
// 1     Users don't modify this code!!!!                                  1
// 1     If you modify this code you may start a fire or a flood somewhere,1
// 1     and some human being may possible cease to exist. You don't want  1
// 1     to be charged with that!                                          1
// 1111111111111111111111111111111111111111111111111111111111111111111111111






// 2222222222222222222222222222222222222222222222222222222222222222222222222
// 2     SEMI CUSTOM CODE STARTS HERE AND FINISHES AT SECTION 3            2
// 2     (code for which only stubs were generated automatically)          2
// 2     You should add the implementation but dont alter the headers      2
// 2     (apart from constructor, in which you should complete initializati2
// 2     on list)                                                          2
// 2222222222222222222222222222222222222222222222222222222222222222222222222

/* sample ctr */
DDrawer::DDrawer (const Configuration::Drawer & config

                  , DCanBus * parent

                 ):
    Base_DDrawer( config

                  , parent),


      	m_communicationController( this )


/* fill up constructor initialization list here */
{
    /* fill up constructor body here */
    	m_communicationController.addPeriodicObject( PeriodicCommunicationObject(
		CommunicationObject(HV_Parameter, HV_READ_GLOBAL_STATUS, drawerID(), 0, boost::bind(&DDrawer::handleReadGlobalStatus, this, _1, _2, _3), /*timeout*/ 1.0 ),
				/* period */ 10.0 )); //creates a periodic object executed on initialization

}

/* sample dtr */
DDrawer::~DDrawer ()
{
}

/* delegators for cachevariables and externalvariables */

/* Note: never directly call this function. */

UaStatus DDrawer::writeVerificationOnOffValue ( const OpcUa_Boolean & v)
{
	//if (v != false || v != true)
	 //		{return OpcUa_BadOutOfRange;}

	CommunicationObject request (HV_Parameter, HV_CONTROL_MODE, drawerID(), v, boost::bind(&DDrawer::handleWriteItems, this, _1, _2, _3), 1.0 );
		m_communicationController.requestObject( request, /* highest prio */ true );


		return OpcUa_Good;

}

/* Note: never directly call this function. */

UaStatus DDrawer::writeResetDrawer ( const OpcUa_Boolean & v)
{
 	if (v != true)
 		{return OpcUa_BadOutOfRange;}

	CommunicationObject request (HV_Parameter, HV_RESET, drawerID(), 0, boost::bind(&DDrawer::handleWriteItems, this, _1, _2, _3), 1.0 );
		m_communicationController.requestObject( request, /* highest prio */ true );


		return OpcUa_Good;

}

/* Note: never directly call this function. */

UaStatus DDrawer::writeOddEvenSwitch0 ( const OpcUa_Boolean & v)
{


	unsigned char switchValue = 0;
	unsigned char list[] = {switchValue, 0, v};

	CommunicationObject request (HV_Parameter, HV_ODDEVEN_SWITCH, drawerID(), list, sizeof(list), boost::bind(&DDrawer::handleWriteItems, this, _1, _2, _3), 1.0 );
	m_communicationController.requestObject( request, /* highest prio */ true );


	return OpcUa_Good;

}

/* Note: never directly call this function. */

UaStatus DDrawer::writeOddEvenSwitch1 ( const OpcUa_Boolean & v)
{


	unsigned char switchValue = 1;
	unsigned char list[] = {switchValue, 0, v};

	CommunicationObject request (HV_Parameter, HV_ODDEVEN_SWITCH, drawerID(), list, sizeof(list), boost::bind(&DDrawer::handleWriteItems, this, _1, _2, _3), 1.0 );
	m_communicationController.requestObject( request, /* highest prio */ true );
	return OpcUa_Good;
}

/* Note: never directly call this function. */

UaStatus DDrawer::writeOddEvenSwitch2 ( const OpcUa_Boolean & v)
{


	unsigned char switchValue = 2;
	unsigned char list[] = {switchValue, 0, v};

	CommunicationObject request (HV_Parameter, HV_ODDEVEN_SWITCH, drawerID(), list, sizeof(list), boost::bind(&DDrawer::handleWriteItems, this, _1, _2, _3), 1.0 );
	m_communicationController.requestObject( request, /* highest prio */ true );


	return OpcUa_Good;

}

/* Note: never directly call this function. */

UaStatus DDrawer::writeOddEvenSwitch3 ( const OpcUa_Boolean & v)
{


	unsigned char switchValue = 3;
	unsigned char list[] = {switchValue, 0, v};

	CommunicationObject request (HV_Parameter, HV_ODDEVEN_SWITCH, drawerID(), list, sizeof(list), boost::bind(&DDrawer::handleWriteItems, this, _1, _2, _3), 1.0 );
	m_communicationController.requestObject( request, /* highest prio */ true );


	return OpcUa_Good;

}

/* Note: never directly call this function. */

UaStatus DDrawer::writeOddEvenSwitch4 ( const OpcUa_Boolean & v)
{


	unsigned char switchValue = 4;
	unsigned char list[] = {switchValue, 0, v};

	CommunicationObject request ( HV_Parameter,HV_ODDEVEN_SWITCH, drawerID(), list, sizeof(list), boost::bind(&DDrawer::handleWriteItems, this, _1, _2, _3), 1.0 );
	m_communicationController.requestObject( request, /* highest prio */ true );

	return OpcUa_Good;

}

/* Note: never directly call this function. */

UaStatus DDrawer::writeInfoTrigger ( const OpcUa_Boolean & v)
{

 	if (v != true)
 		{return OpcUa_BadOutOfRange;}

	for(int type = 1; type <= 10; type++)

	{
		CommunicationObject requestInfoType1 (HV_Parameter, HV_INFO_READ, drawerID(), type, boost::bind(&DDrawer::handleReadInfo, this, _1, _2, _3), 1.0 );
			m_communicationController.requestObject( requestInfoType1, /* highest prio */ true );
	}
	return OpcUa_Good;
}



/* Note: never directly call this function. */

UaStatus DDrawer::writeAddressReadTrigger ( const OpcUa_Boolean & v)
{

 	if (v != true)
 		{return OpcUa_BadOutOfRange;}

	OpcUa_Int32 serial;
	getAddressSpaceLink()->getHvMicroSerialNumber(serial);

	unsigned char list[] = {0,
					(unsigned char)((serial & 0xFF000000)>> 24 ),
					(unsigned char)((serial & 0x00FF0000)>> 16 ),
					(unsigned char)((serial & 0x0000FF00)>> 8 ),
					(unsigned char)((serial & 0x000000FF))};



	CommunicationObject request (HV_Parameter, HV_ADDRESS_READ, drawerID(), list, sizeof(list), boost::bind(&DDrawer::handleAddressReadReply, this, _1, _2, _3), 1.0 );
	m_communicationController.requestObject( request, /* highest prio */ true );

	return OpcUa_Good;

}

/* Note: never directly call this function. */

UaStatus DDrawer::writeAddressWriteNewSoftAddressValueInput ( const OpcUa_Int32 & v)
{
	OpcUa_Int32 serial;

 	if (v < 0 || v> 16)
 		{return OpcUa_BadOutOfRange;}


	getAddressSpaceLink()->getHvMicroSerialNumber(serial);
	unsigned char list[] = {0,
				(unsigned char)((serial & 0xFF000000)>> 24 ),
				(unsigned char)((serial & 0x00FF0000)>> 16 ),
				(unsigned char)((serial & 0x0000FF00)>> 8 ),
				(unsigned char)((serial & 0x000000FF)),
	 			(unsigned char)((v & 0xFF00)>> 8 ),
	 			(unsigned char)((v & 0x00FF))};

	CommunicationObject request (HV_Parameter, HV_ADDRESS_WRITE, drawerID(), list, sizeof(list), boost::bind(&DDrawer::handleAddressWriteReply, this, _1, _2, _3), 1.0 );
	m_communicationController.requestObject( request, /* highest prio */ true );

    return OpcUa_Good;

}



// 3333333333333333333333333333333333333333333333333333333333333333333333333
// 3     FULLY CUSTOM CODE STARTS HERE                                     3
// 3     Below you put bodies for custom methods defined for this class.   3
// 3     You can do whatever you want, but please be decent.               3
// 3333333333333333333333333333333333333333333333333333333333333333333333333

UaStatus DDrawer::handleReadGlobalStatus ( const unsigned char reply[], unsigned int msgLen, bool valid)
{

	OpcUa_Int16 resetreg, warnstat, alarstat, nb_IRQ, statebytevalue;

	if(reply[0] != 0 || msgLen != 8 || valid == false)
	{
		// TODO: proper null value

		getAddressSpaceLink()->setNullNbIRQValue(OpcUa_Bad);

		getAddressSpaceLink()->setNullNbIRQValue(  OpcUa_Bad);
		getAddressSpaceLink()->setNullResetRegValue(  OpcUa_Bad);
		getAddressSpaceLink()->setNullWarningStatValue(  OpcUa_Bad);
		getAddressSpaceLink()->setNullAlarmStatValue(  OpcUa_Bad);

		getAddressSpaceLink()->setConnectionStatus(1, OpcUa_Good);
		getAddressSpaceLink()->setConnectionJammed(1, OpcUa_Good);

		return OpcUa_Bad;
	}

	else
	{
			statebytevalue = reply[0];
			resetreg = (unsigned char)reply[1] ;
			warnstat = ((unsigned char)reply[2]<< 8) + (unsigned char)reply[3];
			alarstat = ((unsigned char)reply[4]<< 8) + (unsigned char)reply[5];
			nb_IRQ   = ((unsigned char)reply[6]<< 8) + (unsigned char)reply[7];

			getAddressSpaceLink()->setNbIRQValue(nb_IRQ, OpcUa_Good);
			getAddressSpaceLink()->setResetRegValue(resetreg, OpcUa_Good);
			getAddressSpaceLink()->setWarningStatValue(warnstat, OpcUa_Good);
			getAddressSpaceLink()->setAlarmStatValue(alarstat, OpcUa_Good);

			getAddressSpaceLink()->setConnectionStatus(statebytevalue, OpcUa_Good);
			getAddressSpaceLink()->setConnectionJammed(reply[0], OpcUa_Good);
			return OpcUa_Good;
	}
}

UaStatus DDrawer::handleReadInfo ( const unsigned char reply[], unsigned int msgLen, bool valid)
{
	OpcUa_Int16 infoType;
	OpcUa_Float infoSoftVersionMajor;
	OpcUa_Float infoSoftVersionMinor;
	OpcUa_Int32 infoValue;
	OpcUa_Int16 statebytevalue = reply[0];
	OpcUa_Int32 newSerial;

	if(statebytevalue != 0 || msgLen != 6 || valid == false)
	{
		// TODO: proper null value
		getAddressSpaceLink()->setNullInternalHVOptoSerialNumber(  OpcUa_Bad);
		getAddressSpaceLink()->setNullInternalHVOptoManufactoryDate(  OpcUa_Bad);
		getAddressSpaceLink()->setNullInternalHVOptoTestDate(  OpcUa_Bad);
		getAddressSpaceLink()->setNullExternalHVOptoSerialNumber(  OpcUa_Bad);
		getAddressSpaceLink()->setNullExternalHVOptoManufactoryDate(  OpcUa_Bad);
		getAddressSpaceLink()->setNullExternalHVOptoTestDate(  OpcUa_Bad);
		getAddressSpaceLink()->setNullHvMicroSerialNumber(  OpcUa_Bad);
		getAddressSpaceLink()->setNullHvMicroHVOptoManufactoryDate(  OpcUa_Bad);
		getAddressSpaceLink()->setNullHvMicroHVOptoTestDate(  OpcUa_Bad);
		getAddressSpaceLink()->setNullSoftWareVersionMajor(OpcUa_Bad);
		getAddressSpaceLink()->setNullSoftWareVersionMinor(OpcUa_Bad);

		getAddressSpaceLink()->setConnectionJammed(1, OpcUa_Good);

		return OpcUa_Bad;
	}

	else
	{
		infoType = reply[1] ;

			switch(infoType)
			{
				case 1:
					infoValue =  (reply[2]<<24)+ (reply[3]<<16)+ (reply[4]<<8)+ (reply[5]);
					newSerial = convertSerials(infoValue);
					getAddressSpaceLink()->setInternalHVOptoSerialNumber(newSerial, OpcUa_Good);
				break;
				case 2:
					infoValue =  (reply[2]<<24)+ (reply[3]<<16)+ (reply[4]<<8)+ (reply[5]);
					getAddressSpaceLink()->setInternalHVOptoManufactoryDate(infoValue, OpcUa_Good);
				break;
				case 3:
					infoValue =  (reply[2]<<24)+ (reply[3]<<16)+ (reply[4]<<8)+ (reply[5]);
					getAddressSpaceLink()->setInternalHVOptoTestDate(infoValue, OpcUa_Good);
				break;
				case 4:
					infoValue =  (reply[2]<<24)+ (reply[3]<<16)+ (reply[4]<<8)+ (reply[5]);
					newSerial = convertSerials(infoValue);
					getAddressSpaceLink()->setExternalHVOptoSerialNumber(newSerial, OpcUa_Good);
				break;
				case 5:
					infoValue =  (reply[2]<<24)+ (reply[3]<<16)+ (reply[4]<<8)+ (reply[5]);
					getAddressSpaceLink()->setExternalHVOptoManufactoryDate(infoValue, OpcUa_Good);
				break;
				case 6:
					infoValue =  (reply[2]<<24)+ (reply[3]<<16)+ (reply[4]<<8)+ (reply[5]);
					getAddressSpaceLink()->setExternalHVOptoTestDate(infoValue, OpcUa_Good);
				break;
				case 7:
					infoValue =  (reply[2]<<24)+ (reply[3]<<16)+ (reply[4]<<8)+ (reply[5]);
					newSerial = convertSerials(infoValue);
					getAddressSpaceLink()->setHvMicroSerialNumber(newSerial, OpcUa_Good);
				break;
				case 8:
					infoValue =  (reply[2]<<24)+ (reply[3]<<16)+ (reply[4]<<8)+ (reply[5]);
					getAddressSpaceLink()->setHvMicroHVOptoManufactoryDate(infoValue, OpcUa_Good);
				break;
				case 9:
					infoValue =  (reply[2]<<24)+ (reply[3]<<16)+ (reply[4]<<8)+ (reply[5]);
					getAddressSpaceLink()->setHvMicroHVOptoTestDate(infoValue, OpcUa_Good);
				break;
				case 10:
					infoSoftVersionMajor =  ((reply[2])+ (reply[3]));
					infoSoftVersionMinor = (reply[4])+ (reply[5]);
					getAddressSpaceLink()->setSoftWareVersionMajor(infoSoftVersionMajor, OpcUa_Good);
					getAddressSpaceLink()->setSoftWareVersionMinor(infoSoftVersionMinor, OpcUa_Good);
				break;
			}

			getAddressSpaceLink()->setConnectionJammed(reply[0], OpcUa_Good);

		return OpcUa_Good;
	}


}



OpcUa_Int32 DDrawer::convertSerials (OpcUa_Int32 serial)
{
	OpcUa_Int32 newSerial;
	OpcUa_Int32 buff1;
	OpcUa_Int32 buff2;
	OpcUa_Int32 buff3;
	OpcUa_Int32 buff4;
	OpcUa_Int32 newhvInternalOptoSerial1;
	OpcUa_Int32 newhvInternalOptoSerial2;


	   buff1 = serial & 0x000000FF;
	   buff2 = (serial & 0x0000FF00) >> 8;
	   buff3 = (serial & 0x00FF0000) >> 16;
	   buff4 = (serial & 0xFF000000) >> 24;

	   newhvInternalOptoSerial1 = (((buff4<<8) + buff3)*1000);
	   newhvInternalOptoSerial2 = (((buff2<<8) + buff1));

	   newSerial = newhvInternalOptoSerial1 + newhvInternalOptoSerial2;


		return newSerial;
}








UaStatus DDrawer::handleWriteItems ( const unsigned char reply[], unsigned int msgLen, bool valid)
{
	OpcUa_Int16 statebytevalue = reply[0];

		if(statebytevalue != 0 || msgLen != 2 || valid == false)
		{
				// TODO: proper null value
				//getAddressSpaceLink()->setInfoValue(  OpcUa_Bad);
			getAddressSpaceLink()->setConnectionJammed(1, OpcUa_Good);
				return OpcUa_Bad;
		}
		else
		{
			return OpcUa_Good;
		}

		getAddressSpaceLink()->setConnectionJammed(reply[0], OpcUa_Good);
}






UaStatus DDrawer::handleControlMode ( const unsigned char reply[], unsigned int msgLen, bool valid)
{
	OpcUa_Int16 statebytevalue = reply[0];

	if(statebytevalue != 0 || msgLen != 2 || valid == false)
	{
			// TODO: proper null value
			//getAddressSpaceLink()->setInfoValue(  OpcUa_Bad);
		getAddressSpaceLink()->setConnectionJammed(1, OpcUa_Good);
			return OpcUa_Bad;
	}
	else
	{
		getAddressSpaceLink()->setConnectionJammed(reply[0], OpcUa_Good);
		return OpcUa_Good;
	}

}




UaStatus DDrawer::handleOddEvenSwitchReply ( const unsigned char reply[], unsigned int msgLen, bool valid)
{
	if(reply[0] != 0 || msgLen != 2 || valid == false)
	{
		getAddressSpaceLink()->setConnectionJammed(1, OpcUa_Good);
		return OpcUa_Bad;
	}

	else
	{
		getAddressSpaceLink()->setConnectionJammed(reply[0], OpcUa_Good);
		return OpcUa_Good;
	}

}



UaStatus DDrawer::handleResetReply ( const unsigned char reply[], unsigned int msgLen, bool valid)

{

	// TODO: add a constant for zero!
	if(reply[0] != 0 || msgLen != 2 || valid == false)
	{
		getAddressSpaceLink()->setConnectionJammed(1, OpcUa_Good);
		return OpcUa_Bad;
	}

	else
	{
		getAddressSpaceLink()->setConnectionJammed(reply[0], OpcUa_Good);
		return OpcUa_Good;
	}

}







UaStatus DDrawer::handleAddressWriteReply ( const unsigned char reply[], unsigned int msgLen, bool valid)

{
	OpcUa_Int16 SoftAddress ;
	OpcUa_Int32 NewSoftAddress ;
	OpcUa_Int32 HardAddress  ;
	OpcUa_Int16 statebyteWrite ;

	if(reply[0] != 0 || msgLen != 8 || valid == false)
	{
		getAddressSpaceLink()->setNullAddressWriteNewSoftValue(  OpcUa_Bad);
		getAddressSpaceLink()->setNullAddressWriteSoftValue(  OpcUa_Bad);
		getAddressSpaceLink()->setNullAddressWriteHardValue(  OpcUa_Bad);
		getAddressSpaceLink()->setConnectionJammed(1, OpcUa_Good);
		return OpcUa_Bad;
	}

	else
	{
		statebyteWrite = reply[0];
		SoftAddress = (reply[2]<<8) + reply[3];
		HardAddress = (reply[4]<<8) + reply[5];
		NewSoftAddress = (reply[6]<<8) + reply[7];

		getAddressSpaceLink()->setAddressWriteNewSoftValue(NewSoftAddress, OpcUa_Good);
		getAddressSpaceLink()->setAddressWriteSoftValue(SoftAddress, OpcUa_Good);
		getAddressSpaceLink()->setAddressWriteHardValue(HardAddress, OpcUa_Good);
		getAddressSpaceLink()->setConnectionJammed(reply[0], OpcUa_Good);

		return OpcUa_Good;
	}
}



UaStatus DDrawer::handleAddressReadReply ( const unsigned char reply[], unsigned int msgLen, bool valid)
{

	OpcUa_Int32 SoftAddress ;
	OpcUa_Int32 HardAddress  ;
	
	if(reply[0] != 0 || msgLen != 6 || valid == false)
	{
		getAddressSpaceLink()->setNullAddressReadSoftValue(  OpcUa_Bad);
		getAddressSpaceLink()->setNullAddressReadHardValue(  OpcUa_Bad);
		getAddressSpaceLink()->setConnectionJammed(1, OpcUa_Good);
		return OpcUa_Bad;
	}

	else
	{
		
		SoftAddress = (reply[2]<<8) + reply[3];
		HardAddress = (reply[4]<<8) + reply[5];

			getAddressSpaceLink()->setAddressReadSoftValue(SoftAddress, OpcUa_Good);
			getAddressSpaceLink()->setAddressReadHardValue(HardAddress, OpcUa_Good);
			getAddressSpaceLink()->setConnectionJammed(reply[0], OpcUa_Good);

			return OpcUa_Good;
	}


}




void DDrawer::handleMessage (const CanMessage & msg)
{

	m_communicationController.handleMessage( msg );

	return;

}



void DDrawer::sendMessage( const CanMessage & msg)
{
	getParent()->sendMessage(msg);
}

void DDrawer::tick ()
{
	m_communicationController.tick();
}




}


