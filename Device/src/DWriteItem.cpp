
/* This is device body stub */


#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DWriteItem.h>
#include <ASWriteItem.h>

#include <HVcan.h>



namespace Device
{



/* sample ctr */
DWriteItem::DWriteItem (const Configuration::WriteItem & config

                        , DChannelNo * parent

                       ):

		m_parent(parent),

		    m_index(config.index()),
		    m_addressSpaceLink(0)

/* fill up constructor initialization list here */
{
    /* fill up constructor body here */
}

/* sample dtr */
DWriteItem::~DWriteItem ()
{
    /* remove children */

}

/* delegators for cachevariables and externalvariables */

/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */
UaStatus DWriteItem::readItemValue (
		OpcUa_Float &value,
    UaDateTime &sourceTime
)
{
	return OpcUa_BadNotImplemented;
}


/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */
UaStatus DWriteItem::writeItemValue (
    OpcUa_Float &value
)
{
	return OpcUa_BadNotImplemented;
}



/*

UaStatus DWriteItem::writeOrderValue (
    OpcUa_Float &value
)
{
	UaStatus ret;
	OpcUa_Int32 v;
	getParent()->getParent()-> setRequestWRITE(this);

printf("IN WRITE ORDER ITEM \n");
				msg.c_dlc = 0;
				m_ind = m_index;
				printf("IN WRITE \n");

				if((getParent()->m_channelNumber >= 0x3C) & (getParent()->m_channelNumber <= 0x42))
				{
					v = value*1000;
				}
				else if((getParent()->m_channelNumber >= 0x43) & (getParent()->m_channelNumber <= 0x44))
				{
					v = value;
				}
				else
				{
					v = value*10;
				}

//					CommunicationObject object ( HV_WRITE_ORDER,  );
//					communicationController->requestObject (object );

					msg.c_data[0] = 0x08;
					msg.c_data[1] = getParent()->m_channelNumber;
					msg.c_data[2] = (((int)v) & 0xFF00)>> 8 ;  //check the values for the order
					msg.c_data[3] = (((int)v) & 0x00FF);      //check the values for the order
					msg.c_dlc = 4;

					printf("HV_WRITE_ORDER\n");

					//orderValue = ((msg.c_data[2] << 8) + (msg.c_data[3]));
					//getAddressSpaceLink()->setOrderValue(orderValue, OpcUa_Good, UaDateTime::now());

					getParent()->getParent()->messageSend(msg);
					printf("MESSAGE SENT  \n");
					getParent()->getParent()->addDelay();
					printf("MESSAGE RECEIVED  \n");
					msgReply = getParent()->getParent()->msgReply;

				    getParent()->getParent()-> freeRequestWRITE()	;
				    printf("Sent and all done \n");

				    return ret;
}







UaStatus DWriteItem::writeMaxStepValue (
    OpcUa_Float &value
)
{
	UaStatus ret;
	OpcUa_Int32 v;

		getParent()->getParent()-> setRequestWRITE(this);
printf("IN WRITE MaxStep ITEM \n");
				msg.c_dlc = 0;
				m_ind = m_index;
				printf("IN WRITE \n");

				if((getParent()->m_channelNumber >= 0x3C) & (getParent()->m_channelNumber <= 0x42))
				{
					v = value*1000;
				}
				else if((getParent()->m_channelNumber >= 0x43) & (getParent()->m_channelNumber <= 0x44))
				{
					v = value;
				}
				else
				{
					v = value*10;
				}


					msg.c_data[0] = 0x0B;
					msg.c_data[1] = getParent()->m_channelNumber;
					msg.c_data[2] = (((int)v) & 0xFF00)>> 8 ;  //check the values for the order
					msg.c_data[3] = (((int)v) & 0x00FF);      //check the values for the order
					msg.c_dlc = 4;

					printf("HV_WRITE_MAXSTEP");

					//maxstepValue= ((msg.c_data[2] << 8) + (msg.c_data[3]));
					//getAddressSpaceLink()->setMaxStepValue(maxstepValue, OpcUa_Good, UaDateTime::now());

					 getParent()->getParent()->messageSend(msg);
					 printf("MESSAGE SENT  \n");
					 getParent()->getParent()->addDelay();
					 printf("MESSAGE RECEIVED  \n");
					 msgReply = getParent()->getParent()->msgReply;
			
				

				    getParent()->getParent()-> freeRequestWRITE()	;
				    printf("Sent and all done \n");

				    return ret;
}
*/



/* address space linker */

void DWriteItem::linkAddressSpace( AddressSpace::ASWriteItem *addressSpaceLink)
{
    if (m_addressSpaceLink != 0)
    {
        /* signalize nice error from here. */
        abort();
    }
    else
        m_addressSpaceLink = addressSpaceLink;
}

/* add/remove */


/* to safely quit */
void DWriteItem::unlinkAllChildren ()
{
    PRINT_LOCATION();
    m_addressSpaceLink = 0;
    /* Fill up: call unlinkAllChildren on all children */


}

/* find methods for children */



void DWriteItem::passWRITE(const CanMessage rmsg)
{
	msgReply = getParent()->getParent()-> passMSG(rmsg);
}



}


