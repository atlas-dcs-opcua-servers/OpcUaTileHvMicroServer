
/* This is device body stub */


#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DReadItems.h>
#include <ASReadItems.h>
#include <HVcan.h>
#include <DDrawer.h>

#include <iostream>




namespace Device
{



/* sample ctr */
DReadItems::DReadItems (const Configuration::ReadItems & config

                        , DChannelNo * parent

                       ):

    m_parent(parent),

    m_index(config.index()),
    m_addressSpaceLink(0)

/* fill up constructor initialization list here */
{
    /* fill up constructor body here */
}

/* sample dtr */
DReadItems::~DReadItems ()
{
    /* remove children */

}

/* delegators for cachevariables and externalvariables */

/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */
UaStatus DReadItems::readItemValue (
    OpcUa_Double &value,
    UaDateTime &sourceTime
)
{
    
    sourceTime = UaDateTime::now();
    return OpcUa_BadNotImplemented;
}




UaStatus DReadItems::readAppliedValue (
    OpcUa_Float &value,
    UaDateTime &sourceTime
)
{
    printf("Reading Applied Value \n");
    sourceTime = UaDateTime::now();
    return OpcUa_Good;
}







UaStatus DReadItems::readManyItems ()
{
	UaDateTime sourceTime;
	getParent()->getParent()-> setRequestREAD(this);

	UaStatus ret;

	msg.c_dlc = 0;
	m_ind = m_index;

	    if (m_ind == HV_CHAN_READ) // m_ind is hexadecimal
	    {
	      msg.c_data[0] = 0x07;
	      msg.c_data[1] = getParent()->m_channelNumber;
	      msg.c_dlc = 2;
	      printf("HV_CHAN_READ\n");
	    }

	    msg.c_id = (0x100 + (getParent()->getParent()->m_drawerID));

	    printf("msg.c_id = %x \n", msg.c_id);
	    printf("msg.c_dlc = %x \n", msg.c_dlc);
	    for (int y = 0; y< msg.c_dlc; y++)
	    {
	      	 printf("msg.c_data[y] = %d \n", msg.c_data[y]);
	    }

	    getParent()->getParent()->messageSend(msg);
	    printf("MESSAGE SENT  \n");
	    getParent()->getParent()->addDelay();
		printf("MESSAGE RECEIVED  \n");
		msgReply = getParent()->getParent()->msgReply;


	    stateValue = msgReply.c_data[0];
		statusValue = msgReply.c_data[1];

	    if((getParent()->m_channelNumber >= 0x3C) & (getParent()->m_channelNumber <= 0x42))
	    {
	    	appliedValue = ((unsigned char)(msgReply.c_data[6] << 8) + ((unsigned char)msgReply.c_data[7]))/1000.0;
	    	orderValue = (((unsigned char)msgReply.c_data[2] << 8) + ((unsigned char)msgReply.c_data[3]))/1000.0;
	    	maxstepValue= (((unsigned char)msgReply.c_data[4] << 8) + ((unsigned char)msgReply.c_data[5]))/1000.0;
	    }
	    else if((getParent()->m_channelNumber >= 0x43) & (getParent()->m_channelNumber <= 0x44))
	    {
	    	appliedValue = (((unsigned char)msgReply.c_data[6] << 8) + ((unsigned char)msgReply.c_data[7]));
	    	orderValue = (((unsigned char)msgReply.c_data[2] << 8) + ((unsigned char)msgReply.c_data[3]));
	    	maxstepValue= (((unsigned char)msgReply.c_data[4] << 8) + ((unsigned char)msgReply.c_data[5]));
	    }
	    else
	    {
	    	appliedValue = (((unsigned char)msgReply.c_data[6] << 8) + ((unsigned char)msgReply.c_data[7]))/10.0;
	    	orderValue = (((unsigned char)msgReply.c_data[2] << 8) + ((unsigned char)msgReply.c_data[3]))/10.0;
	    	maxstepValue= (((unsigned char)msgReply.c_data[4] << 8) + ((unsigned char)msgReply.c_data[5]))/10.0;
	    }




	   	for (int y = 0; y< msgReply.c_dlc; y++)
	   	{
	   	   	 printf("msgReply.c_data[y] = %x \n", msgReply.c_data[y]);
	   	}

	    printf("appliedValue is %d \n", appliedValue);
	    printf("orderValue is %d \n", orderValue);
	    printf("maxstepValue is %d \n", maxstepValue);
	    printf("stateValue is %d \n", stateValue);
	    printf("statusValue is %d \n", statusValue);

	    getAddressSpaceLink()->setAppliedValue(appliedValue, OpcUa_Good, UaDateTime::now());
	    getAddressSpaceLink()->setMaxstepValue(maxstepValue, OpcUa_Good, UaDateTime::now());
	    getAddressSpaceLink()->setOrderValue(orderValue, OpcUa_Good, UaDateTime::now());
	    getAddressSpaceLink()->setStateValue(stateValue, OpcUa_Good, UaDateTime::now());
	    getAddressSpaceLink()->setStatusValue(statusValue, OpcUa_Good, UaDateTime::now());

	    getParent()->getParent()-> freeRequestREAD()	;

	    printf("Sent and all done \n");

    sourceTime = UaDateTime::now();
    return ret;
}





/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */
UaStatus DReadItems::writeItemValue (
    OpcUa_Double &value
)
{
    return OpcUa_BadNotImplemented;
}


/* address space linker */

void DReadItems::linkAddressSpace( AddressSpace::ASReadItems *addressSpaceLink)
{
    if (m_addressSpaceLink != 0)
    {
        /* signalize nice error from here. */
        abort();
    }
    else
        m_addressSpaceLink = addressSpaceLink;
}

/* add/remove */


/* to safely quit */
void DReadItems::unlinkAllChildren ()
{
    PRINT_LOCATION();
    m_addressSpaceLink = 0;
    /* Fill up: call unlinkAllChildren on all children */


}

/* find methods for children */

void DReadItems::passREAD(const CanMessage rmsg)
{
	msgReply = getParent()->getParent()-> passMSG(rmsg);
	//msgReply = getParent()->getParent()->msgReply;
}




}


