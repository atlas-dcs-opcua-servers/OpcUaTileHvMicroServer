
/*
 * DCanPSU.cpp
 *
 *  Created on: Mar, 2015
 *      Author: pagoncal
 */

#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DCanPSU.h>
#include <ASCanPSU.h>
#include <ASCrate.h>




namespace Device
{




// 1111111111111111111111111111111111111111111111111111111111111111111111111
// 1     GENERATED CODE STARTS HERE AND FINISHES AT SECTION 2              1
// 1     Users don't modify this code!!!!                                  1
// 1     If you modify this code you may start a fire or a flood somewhere,1
// 1     and some human being may possible cease to exist. You don't want  1
// 1     to be charged with that!                                          1
// 1111111111111111111111111111111111111111111111111111111111111111111111111






// 2222222222222222222222222222222222222222222222222222222222222222222222222
// 2     SEMI CUSTOM CODE STARTS HERE AND FINISHES AT SECTION 3            2
// 2     (code for which only stubs were generated automatically)          2
// 2     You should add the implementation but dont alter the headers      2
// 2     (apart from constructor, in which you should complete initializati2
// 2     on list)                                                          2
// 2222222222222222222222222222222222222222222222222222222222222222222222222

/* sample ctr */
DCanPSU::DCanPSU (const Configuration::CanPSU & config

                  , DCrate * parent

                 ):
    Base_DCanPSU( config

                  , parent)




/* fill up constructor initialization list here */
{
    /* fill up constructor body here */

	unsigned char channel = canPSUID();



	getParent()->getCommunicationController()->addPeriodicObject( PeriodicCommunicationObject(
		CommunicationObject(CANPSU_Parameter, 0x06, channel, channel, boost::bind(&DCanPSU::handleChannelStatus, this, _1, _2, _3), /*timeout*/ 1.0 ), /* period */ 10.0 ));



	unsigned char list[] = {channel, 0x00, 0x01};

	getParent()->getCommunicationController()->addPeriodicObject( PeriodicCommunicationObject(
		CommunicationObject(CANPSU_Parameter, HV_CHAN_READ, canPSUID(), list, sizeof(list), boost::bind(&DCanPSU::handleReadChannelVoltageReply, this, _1, _2, _3), /*timeout*/ 1.0 ),
						/* period */ 10.0 ));

	list[2] = 0x02;
	getParent()->getCommunicationController()->addPeriodicObject( PeriodicCommunicationObject(
		CommunicationObject(CANPSU_Parameter, HV_CHAN_READ, canPSUID(), list, sizeof(list), boost::bind(&DCanPSU::handleReadChannelConsumptionReply, this, _1, _2, _3), /*timeout*/ 1.0 ),
							/* period */ 10.0 ));

	list[2] = 0x03;
	getParent()->getCommunicationController()->addPeriodicObject( PeriodicCommunicationObject(
		CommunicationObject(CANPSU_Parameter, HV_CHAN_READ, canPSUID(), list, sizeof(list), boost::bind(&DCanPSU::handleReadChannelReferenceReply, this, _1, _2, _3), /*timeout*/ 1.0 ),
								/* period */ 10.0 ));

	list[2] =  0x04;
	getParent()->getCommunicationController()->addPeriodicObject( PeriodicCommunicationObject(
		CommunicationObject(CANPSU_Parameter, HV_CHAN_READ,  canPSUID(), list, sizeof(list), boost::bind(&DCanPSU::handleReadChannelTemperatureReply, this, _1, _2, _3), /*timeout*/ 1.0 ),
									/* period */ 10.0 ));



}

/* sample dtr */
DCanPSU::~DCanPSU ()
{
}

/* delegators for cachevariables and externalvariables */

/* Note: never directly call this function. */

UaStatus DCanPSU::writeWriteConsumptionOrder ( const OpcUa_UInt32 & v)
{
	OpcUa_Int16 value;
	OpcUa_Float reference;

 	if (v < 0 || v > CANPSU_MAX)
 		{return OpcUa_BadOutOfRange;}

	reference = returnReference(canPSUID());
	value = v*reference;


	unsigned char channel = canPSUID();
	unsigned char list[] = {channel, 0x00, 0x02,
							(unsigned char)((value & 0xFF00)>> 8 ),
							(unsigned char)((value & 0x00FF))};

	CommunicationObject request (CANPSU_Parameter,  0x08, canPSUID(), list, sizeof(list), boost::bind(&DCanPSU::handleWriteGeneric, this, _1, _2, _3), 1.0 );
	getParent()->getCommunicationController()->requestObject( request, /* highest prio */true );


	return OpcUa_Good;

}

/* Note: never directly call this function. */

UaStatus DCanPSU::writeWriteConsumptionMaxstep ( const OpcUa_UInt32 & v)
{
	OpcUa_Int16 value;
		OpcUa_Float reference;

	 	if (v < 0 || v > CANPSU_MAX)
	 		{return OpcUa_BadOutOfRange;}

		reference = returnReference(canPSUID());
		value = v*reference;


		unsigned char channel = canPSUID();
		unsigned char list[] = {channel, 0x00, 0x02,
								(unsigned char)((value & 0xFF00)>> 8 ),
								(unsigned char)((value & 0x00FF))};

		CommunicationObject request (CANPSU_Parameter,  0x0b, canPSUID(), list, sizeof(list), boost::bind(&DCanPSU::handleWriteGeneric, this, _1, _2, _3), 1.0 );
		getParent()->getCommunicationController()->requestObject( request, /* highest prio */true );


		return OpcUa_Good;
}


/* Note: never directly call this function. */

UaStatus DCanPSU::writeWriteReferenceOrder ( const OpcUa_UInt32 & v)
{
	OpcUa_Int16 value;
	OpcUa_Float reference;

 	if (v < 0 || v > CANPSU_MAX)
 		{return OpcUa_BadOutOfRange;}

	reference = returnReference(canPSUID());
	value = v*reference;


	unsigned char channel = canPSUID();
	unsigned char list[] = {channel, 0x00, 0x03,
							(unsigned char)((value & 0xFF00)>> 8 ),
							(unsigned char)((value & 0x00FF))};

	CommunicationObject request (CANPSU_Parameter,  0x08, canPSUID(), list, sizeof(list), boost::bind(&DCanPSU::handleWriteGeneric, this, _1, _2, _3), 1.0 );
	getParent()->getCommunicationController()->requestObject( request, /* highest prio */true );


	return OpcUa_Good;
}

/* Note: never directly call this function. */

UaStatus DCanPSU::writeWriteReferenceMaxstep ( const OpcUa_UInt32 & v)
{
	OpcUa_Int16 value;
			OpcUa_Float reference;

		 	if (v < 0 || v > CANPSU_MAX)
		 		{return OpcUa_BadOutOfRange;}

			reference = returnReference(canPSUID());
			value = v*reference;


			unsigned char channel = canPSUID();
			unsigned char list[] = {channel, 0x00, 0x03,
									(unsigned char)((value & 0xFF00)>> 8 ),
									(unsigned char)((value & 0x00FF))};

			CommunicationObject request (CANPSU_Parameter,  0x0b, canPSUID(), list, sizeof(list), boost::bind(&DCanPSU::handleWriteGeneric, this, _1, _2, _3), 1.0 );
			getParent()->getCommunicationController()->requestObject( request, /* highest prio */true );


			return OpcUa_Good;
}

/* Note: never directly call this function. */

UaStatus DCanPSU::writeWriteTemperatureOrder ( const OpcUa_UInt32 & v)
{
	OpcUa_Int16 value;
	OpcUa_Float reference;

 	if (v < 0 || v > CANPSU_MAX)
 		{return OpcUa_BadOutOfRange;}

	reference = returnReference(canPSUID());
	value = v*reference;


	unsigned char channel = canPSUID();
	unsigned char list[] = {channel, 0x00, 0x04,
							(unsigned char)((value & 0xFF00)>> 8 ),
							(unsigned char)((value & 0x00FF))};

	CommunicationObject request (CANPSU_Parameter,  0x08,  canPSUID(), list, sizeof(list), boost::bind(&DCanPSU::handleWriteGeneric, this, _1, _2, _3), 1.0 );
	getParent()->getCommunicationController()->requestObject( request, /* highest prio */true );


	return OpcUa_Good;
}

/* Note: never directly call this function. */

UaStatus DCanPSU::writeWriteTemperatureMaxstep ( const OpcUa_UInt32 & v)
{
	OpcUa_Int16 value;
			OpcUa_Float reference;

		 	if (v < 0 || v > CANPSU_MAX)
		 		{return OpcUa_BadOutOfRange;}

			reference = returnReference(canPSUID());
			value = v*reference;


			unsigned char channel = canPSUID();
			unsigned char list[] = {channel, 0x00, 0x04,
									(unsigned char)((value & 0xFF00)>> 8 ),
									(unsigned char)((value & 0x00FF))};

			CommunicationObject request (CANPSU_Parameter,  0x0b,  canPSUID(), list, sizeof(list), boost::bind(&DCanPSU::handleWriteGeneric, this, _1, _2, _3), 1.0 );
			getParent()->getCommunicationController()->requestObject( request, /* highest prio */true );


			return OpcUa_Good;
}

/* Note: never directly call this function. */

UaStatus DCanPSU::writeWriteVoltageOrder ( const OpcUa_UInt32 & v)
{
	OpcUa_Int16 value;
	OpcUa_Float reference;

 	if (v < 0 || v > CANPSU_MAX)
 		{return OpcUa_BadOutOfRange;}

	reference = returnReference(canPSUID());
	value = v*reference;


	unsigned char channel = canPSUID();
	unsigned char list[] = {channel, 0x00, 0x01,
							(unsigned char)((value & 0xFF00)>> 8 ),
							(unsigned char)((value & 0x00FF))};

	CommunicationObject request (CANPSU_Parameter,  0x08,  canPSUID(), list, sizeof(list), boost::bind(&DCanPSU::handleWriteGeneric, this, _1, _2, _3), 1.0 );
	getParent()->getCommunicationController()->requestObject( request, /* highest prio */true );


	return OpcUa_Good;
}

/* Note: never directly call this function. */

UaStatus DCanPSU::writeWriteVoltageMaxstep ( const OpcUa_UInt32 & v)
{
	OpcUa_Int16 value;
			OpcUa_Float reference;

		 	if (v < 0 || v > CANPSU_MAX)
		 		{return OpcUa_BadOutOfRange;}

			reference = returnReference(canPSUID());
			value = v*reference;


			unsigned char channel = canPSUID();
			unsigned char list[] = {channel, 0x00, 0x01,
									(unsigned char)((value & 0xFF00)>> 8 ),
									(unsigned char)((value & 0x00FF))};

			CommunicationObject request (CANPSU_Parameter,  0x0b, canPSUID(), list, sizeof(list), boost::bind(&DCanPSU::handleWriteGeneric, this, _1, _2, _3), 1.0 );
			getParent()->getCommunicationController()->requestObject( request, /* highest prio */true );

			return OpcUa_Good;
		}

/* Note: never directly call this function. */

UaStatus DCanPSU::writeSwitchChanOnOff ( const OpcUa_Boolean & v)
{
	unsigned char channel = canPSUID();
	unsigned char list[] = {channel, 0, (unsigned char)((v & 0x00FF))};

		CommunicationObject request (CANPSU_Parameter,  0x03, canPSUID(), list, sizeof(list), boost::bind(&DCanPSU::handleWriteGeneric, this, _1, _2, _3), 1.0 );
		getParent()->getCommunicationController()->requestObject( request, /* highest prio */ true );

		return OpcUa_Good;
}



// 3333333333333333333333333333333333333333333333333333333333333333333333333
// 3     FULLY CUSTOM CODE STARTS HERE                                     3
// 3     Below you put bodies for custom methods defined for this class.   3
// 3     You can do whatever you want, but please be decent.               3
// 3333333333333333333333333333333333333333333333333333333333333333333333333

OpcUa_Float DCanPSU::returnReference(OpcUa_Int32 channel)
{
	OpcUa_Float referenceValue = 1.0;

	/*if((m_channelNumber >= 0x3C) & (m_channelNumber <= 0x42))
	{
		referenceValue = 1000.0;
	}
	else if((m_channelNumber >= 0x43) & (m_channelNumber <= 0x44))
	{
		referenceValue = 1;
	}
	else
	{
		referenceValue = 10.0;
	}
*/
	return referenceValue;
}



UaStatus DCanPSU::handleWriteGeneric( const unsigned char reply[], unsigned int msgLen, bool valid)
{
	OpcUa_Int16 statebytevalue = reply[0];

		if(statebytevalue != 0 || msgLen != 2 || valid == false)
		{
				// TODO: proper null value
			getParent()->getAddressSpaceLink()->setConnectionJammed(1, OpcUa_Good);
			return OpcUa_Bad;
		}
		else
		{
			getParent()->getAddressSpaceLink()->setConnectionJammed(reply[0], OpcUa_Good);
			return OpcUa_Good;
		}
}




UaStatus DCanPSU::handleChannelStatus ( const unsigned char reply[], unsigned int msgLen, bool valid)
{

	OpcUa_Int16 onOff, startup, status, serial, statebytevalue;
	OpcUa_Int16 shortStatusValue, localRemote, chanVeto, nav;


	if(reply[0] != 0 || msgLen != 6 || valid == false)
	{
		// TODO: proper null value

		getAddressSpaceLink()->setNullStatusOnOff(  OpcUa_Bad);
		getAddressSpaceLink()->setNullStatusStartUp(  OpcUa_Bad);
		getAddressSpaceLink()->setNullStatusStatus(  OpcUa_Bad);
		getAddressSpaceLink()->setNullStatusSerialNumber(  OpcUa_Bad);
		getAddressSpaceLink()->setNullLocalRemote(OpcUa_Bad);
		getAddressSpaceLink()->setNullChanVeto(OpcUa_Bad);
		getAddressSpaceLink()->setNullChanNav(OpcUa_Bad);

		getParent()->getAddressSpaceLink()->setConnectionStatus(1, OpcUa_Good);
		getParent()->getAddressSpaceLink()->setConnectionJammed(1, OpcUa_Good);

		return OpcUa_Bad;
	}


	else
	{
			statebytevalue = reply[0];
			onOff = (unsigned char)reply[1] ;
			startup = ((unsigned char)reply[2]) ;
			shortStatusValue = ((unsigned char)reply[3]) ;
			serial   = ((unsigned char)reply[4]<< 8) + (unsigned char)reply[5];

		

		//status = (shortStatusValue & 00001111)  ;
		//status = (shortStatusValue & 15);
		//printf("shortStatusValue is %d \n", shortStatusValue);
		//printf("status is %d \n", status);


		//bool status[32] = b  
		

		if ((shortStatusValue & 8) == 8)
		{
			nav = 1; getAddressSpaceLink()->setChanNav(nav,  OpcUa_Good);
		}

		else
		{

			if ((shortStatusValue & 1) == 1){ localRemote = 0; getAddressSpaceLink()->setLocalRemote(localRemote,  OpcUa_Good);}
			else if ((shortStatusValue & 2) == 2){localRemote = 1; getAddressSpaceLink()->setLocalRemote(localRemote,  OpcUa_Good);}
			
			if ((shortStatusValue & 4) == 4){chanVeto = 1; getAddressSpaceLink()->setChanVeto(chanVeto,  OpcUa_Good);}
			else {chanVeto = 0; getAddressSpaceLink()->setChanVeto(chanVeto,  OpcUa_Good);}
			
			nav = 0; getAddressSpaceLink()->setChanNav(nav,  OpcUa_Good);
		}
		
		
		//if (status > 2)
		 // status = status*(-1);



			getAddressSpaceLink()->setStatusOnOff(onOff,  OpcUa_Good);
			getAddressSpaceLink()->setStatusStartUp(startup,  OpcUa_Good);
			getAddressSpaceLink()->setStatusStatus( shortStatusValue,  OpcUa_Good);
			getAddressSpaceLink()->setStatusSerialNumber(serial,  OpcUa_Good );

			getParent()->getAddressSpaceLink()->setConnectionStatus(statebytevalue, OpcUa_Good);
			getParent()->getAddressSpaceLink()->setConnectionJammed(reply[0], OpcUa_Good);
			return OpcUa_Good;
	}
}





UaStatus DCanPSU::handleReadChannelVoltageReply( const unsigned char reply[], unsigned int msgLen, bool valid)

{
	OpcUa_Float appliedValue;
	OpcUa_Float orderValue;
	OpcUa_Float maxstepValue;
	OpcUa_Float stateValue;
	OpcUa_Int16	statusValue;
	OpcUa_Float reference;
	OpcUa_Int16 shortStatusValue;



	// TODO: add a constant for zero!
	if(reply[0] != 0 || msgLen != 8 || valid == false)
	{

		getAddressSpaceLink()->setNullReadVoltageOrder(OpcUa_Bad );
		getAddressSpaceLink()->setNullReadVoltageMaxstep(OpcUa_Bad );
		getAddressSpaceLink()->setNullReadVoltageApplied(OpcUa_Bad );
		getAddressSpaceLink()->setNullReadVoltageStatus(OpcUa_Bad );


		getParent()->getAddressSpaceLink()->setConnectionStatus(1, OpcUa_Good);
		getParent()->getAddressSpaceLink()->setConnectionJammed(1, OpcUa_Good);
		return OpcUa_Bad;
	}


	else
	{
		stateValue = reply[0];
		shortStatusValue = reply[1];

		statusValue = (shortStatusValue & 11110000) >> 4 ;
		if (statusValue>2)
		  statusValue=statusValue*(-1);


		reference = returnReference(canPSUID());
		appliedValue = ((short)((reply[6] << 8) + (reply[7])))/reference;
		orderValue = ((reply[2] << 8) + (reply[3]))/reference;
		maxstepValue= ((reply[4] << 8) + (reply[5]))/reference;



		//for (int y = 0; y< 8; y++)\n\t\t{\n\t\t   \t printf("reply[y] = %x \\n", reply[y]);\n\t\t}

		getAddressSpaceLink()->setReadVoltageOrder(orderValue, OpcUa_Good );
		getAddressSpaceLink()->setReadVoltageMaxstep(maxstepValue, OpcUa_Good );
		getAddressSpaceLink()->setReadVoltageApplied(appliedValue, OpcUa_Good );
		getAddressSpaceLink()->setReadVoltageStatus(statusValue, OpcUa_Good );

		getParent()->getAddressSpaceLink()->setConnectionJammed(reply[0], OpcUa_Good);
		getParent()->getAddressSpaceLink()->setConnectionStatus(stateValue, OpcUa_Good);


		return OpcUa_Good;

	}
}


UaStatus DCanPSU::handleReadChannelConsumptionReply( const unsigned char reply[], unsigned int msgLen, bool valid)

{
	OpcUa_Float appliedValue;
	OpcUa_Float orderValue;
	OpcUa_Float maxstepValue;
	OpcUa_Float stateValue;
	OpcUa_Float	statusValue;
	OpcUa_Int16 shortStatusValue;
	OpcUa_Float reference;


	// TODO: add a constant for zero!
	if(reply[0] != 0 || msgLen != 8 || valid == false)
	{
		getAddressSpaceLink()->setNullReadConsumptionOrder(OpcUa_Bad );
		getAddressSpaceLink()->setNullReadConsumptionMaxstep(OpcUa_Bad );
		getAddressSpaceLink()->setNullReadConsumptionApplied(OpcUa_Bad );
		getAddressSpaceLink()->setNullReadConsumptionStatus(OpcUa_Bad );



		getParent()->getAddressSpaceLink()->setConnectionStatus(1, OpcUa_Good);
		getParent()->getAddressSpaceLink()->setConnectionJammed(1, OpcUa_Good);
		return OpcUa_Bad;
	}


	else
	{
		stateValue = reply[0];
		shortStatusValue = reply[1];

		statusValue = (shortStatusValue & 11110000) >> 4 ;
		if (statusValue>2)
		  statusValue=statusValue*(-1);


		reference = returnReference(canPSUID());
		appliedValue = ((short)((reply[6] << 8) + (reply[7])))/reference;
		orderValue = ((reply[2] << 8) + (reply[3]))/reference;
		maxstepValue= ((reply[4] << 8) + (reply[5]))/reference;



		//for (int y = 0; y< 8; y++)\n\t\t{\n\t\t   \t printf("reply[y] = %x \\n", reply[y]);\n\t\t}

		getAddressSpaceLink()->setReadConsumptionOrder(orderValue, OpcUa_Good );
		getAddressSpaceLink()->setReadConsumptionMaxstep(maxstepValue, OpcUa_Good );
		getAddressSpaceLink()->setReadConsumptionApplied(appliedValue, OpcUa_Good );
		getAddressSpaceLink()->setReadConsumptionStatus(statusValue, OpcUa_Good );


		getParent()->getAddressSpaceLink()->setConnectionJammed(reply[0], OpcUa_Good);
		getParent()->getAddressSpaceLink()->setConnectionStatus(stateValue, OpcUa_Good);


		return OpcUa_Good;

	}
}




UaStatus DCanPSU::handleReadChannelReferenceReply( const unsigned char reply[], unsigned int msgLen, bool valid)

{
	OpcUa_Float appliedValue;
	OpcUa_Float orderValue;
	OpcUa_Float maxstepValue;
	OpcUa_Float stateValue;
	OpcUa_Float	statusValue;
	OpcUa_Float reference;
OpcUa_Int16 shortStatusValue;

	// TODO: add a constant for zero!
	if(reply[0] != 0 || msgLen != 8 || valid == false)
	{

		getAddressSpaceLink()->setNullReadReferenceOrder(OpcUa_Bad );
		getAddressSpaceLink()->setNullReadReferenceMaxstep(OpcUa_Bad );
		getAddressSpaceLink()->setNullReadReferenceApplied(OpcUa_Bad );
		getAddressSpaceLink()->setNullReadReferenceStatus(OpcUa_Bad );

		getParent()->getAddressSpaceLink()->setConnectionStatus(1, OpcUa_Good);
		getParent()->getAddressSpaceLink()->setConnectionJammed(1, OpcUa_Good);
		return OpcUa_Bad;
	}


	else
	{
		stateValue = reply[0];
		shortStatusValue = reply[1];

		statusValue = (shortStatusValue & 11110000) >> 4 ;
		if (statusValue>2)
		  statusValue=statusValue*(-1);


		reference = returnReference(canPSUID());
		appliedValue = ((short)((reply[6] << 8) + (reply[7])))/reference;
		orderValue = ((reply[2] << 8) + (reply[3]))/reference;
		maxstepValue= ((reply[4] << 8) + (reply[5]))/reference;



		//for (int y = 0; y< 8; y++)\n\t\t{\n\t\t   \t printf("reply[y] = %x \\n", reply[y]);\n\t\t}

		getAddressSpaceLink()->setReadReferenceOrder(orderValue, OpcUa_Good );
		getAddressSpaceLink()->setReadReferenceMaxstep(maxstepValue, OpcUa_Good );
		getAddressSpaceLink()->setReadReferenceApplied(appliedValue, OpcUa_Good );
		getAddressSpaceLink()->setReadReferenceStatus(statusValue, OpcUa_Good );

		getParent()->getAddressSpaceLink()->setConnectionJammed(reply[0], OpcUa_Good);
		getParent()->getAddressSpaceLink()->setConnectionStatus(stateValue, OpcUa_Good);


		return OpcUa_Good;

	}
}




UaStatus DCanPSU::handleReadChannelTemperatureReply( const unsigned char reply[], unsigned int msgLen, bool valid)

{
	OpcUa_Float appliedValue;
	OpcUa_Float orderValue;
	OpcUa_Float maxstepValue;
	OpcUa_Float stateValue;
	OpcUa_Float	statusValue;
	OpcUa_Float reference;
OpcUa_Int16 shortStatusValue;

	// TODO: add a constant for zero!
	if(reply[0] != 0 || msgLen != 8 || valid == false)
	{
		getAddressSpaceLink()->setNullReadTemperatureOrder(OpcUa_Bad );
		getAddressSpaceLink()->setNullReadTemperatureMaxstep(OpcUa_Bad );
		getAddressSpaceLink()->setNullReadTemperatureApplied(OpcUa_Bad );
		getAddressSpaceLink()->setNullReadTemperatureStatus(OpcUa_Bad );

		getParent()->getAddressSpaceLink()->setConnectionStatus(1, OpcUa_Good);
		getParent()->getAddressSpaceLink()->setConnectionJammed(1, OpcUa_Good);
		return OpcUa_Bad;
	}


	else
	{
		stateValue = reply[0];
		shortStatusValue = reply[1];

		statusValue = (shortStatusValue & 11110000) >> 4 ;
		if (statusValue>2)
		  statusValue=statusValue*(-1);
	

		reference = returnReference(canPSUID());
		appliedValue = ((short)((reply[6] << 8) + (reply[7])))/reference;
		orderValue = ((reply[2] << 8) + (reply[3]))/reference;
		maxstepValue= ((reply[4] << 8) + (reply[5]))/reference;



		//for (int y = 0; y< 8; y++)\n\t\t{\n\t\t   \t printf("reply[y] = %x \\n", reply[y]);\n\t\t}

		getAddressSpaceLink()->setReadTemperatureOrder(orderValue, OpcUa_Good );
		getAddressSpaceLink()->setReadTemperatureMaxstep(maxstepValue, OpcUa_Good );
		getAddressSpaceLink()->setReadTemperatureApplied(appliedValue, OpcUa_Good );
		getAddressSpaceLink()->setReadTemperatureStatus(statusValue, OpcUa_Good );

		getParent()->getAddressSpaceLink()->setConnectionJammed(reply[0], OpcUa_Good);
		getParent()->getAddressSpaceLink()->setConnectionStatus(stateValue, OpcUa_Good);


		return OpcUa_Good;

	}
}



UaStatus DCanPSU::handleReadChannelReply ( const unsigned char reply[], unsigned int msgLen, bool valid)

{

	OpcUa_Float appliedValue;
	OpcUa_Float orderValue;
	OpcUa_Float maxstepValue;
	OpcUa_Float stateValue;
	OpcUa_Float	statusValue;
	OpcUa_Float reference;
	OpcUa_Int16 shortStatusValue;

	// TODO: add a constant for zero!
	if(reply[0] != 0 || msgLen != 8 || valid == false)
	{
		getAddressSpaceLink()->setNullReadConsumptionOrder(OpcUa_Bad );
		getAddressSpaceLink()->setNullReadConsumptionMaxstep(OpcUa_Bad );
		getAddressSpaceLink()->setNullReadConsumptionApplied(OpcUa_Bad );
		getAddressSpaceLink()->setNullReadConsumptionStatus(OpcUa_Bad );
		getAddressSpaceLink()->setNullReadReferenceOrder(OpcUa_Bad );
		getAddressSpaceLink()->setNullReadReferenceMaxstep(OpcUa_Bad );
		getAddressSpaceLink()->setNullReadReferenceApplied(OpcUa_Bad );
		getAddressSpaceLink()->setNullReadReferenceStatus(OpcUa_Bad );
		getAddressSpaceLink()->setNullReadTemperatureOrder(OpcUa_Bad );
		getAddressSpaceLink()->setNullReadTemperatureMaxstep(OpcUa_Bad );
		getAddressSpaceLink()->setNullReadTemperatureApplied(OpcUa_Bad );
		getAddressSpaceLink()->setNullReadTemperatureStatus(OpcUa_Bad );
		getAddressSpaceLink()->setNullReadVoltageOrder(OpcUa_Bad );
		getAddressSpaceLink()->setNullReadVoltageMaxstep(OpcUa_Bad );
		getAddressSpaceLink()->setNullReadVoltageApplied(OpcUa_Bad );
		getAddressSpaceLink()->setNullReadVoltageStatus(OpcUa_Bad );


		getParent()->getAddressSpaceLink()->setConnectionStatus(1, OpcUa_Good);
		getParent()->getAddressSpaceLink()->setConnectionJammed(1, OpcUa_Good);
		return OpcUa_Bad;
	}


	else
	{
		stateValue = reply[0];
		shortStatusValue = reply[1];

		statusValue = (shortStatusValue & 11110000) >> 4 ;
		if (statusValue>2)
		  statusValue=statusValue*(-1);


		reference = returnReference(canPSUID());


		appliedValue = (short)(((reply[6]) << 8) + (reply[7]))/reference;
		orderValue = ((reply[2] << 8) + (reply[3]))/reference;
		maxstepValue= ((reply[4] << 8) + (reply[5]))/reference;



		/*for (int y = 0; y< msgLen; y++)
		{
		   	 printf("reply[y] = %x \n", reply[y]);
		}*/

		getAddressSpaceLink()->setReadConsumptionOrder(orderValue, OpcUa_Good );
		getAddressSpaceLink()->setReadConsumptionMaxstep(maxstepValue, OpcUa_Good );
		getAddressSpaceLink()->setReadConsumptionApplied(appliedValue, OpcUa_Good );
		getAddressSpaceLink()->setReadConsumptionStatus(statusValue, OpcUa_Good );
		getAddressSpaceLink()->setReadReferenceOrder(orderValue, OpcUa_Good );
		getAddressSpaceLink()->setReadReferenceMaxstep(maxstepValue, OpcUa_Good );
		getAddressSpaceLink()->setReadReferenceApplied(appliedValue, OpcUa_Good );
		getAddressSpaceLink()->setReadReferenceStatus(statusValue, OpcUa_Good );
		getAddressSpaceLink()->setReadTemperatureOrder(orderValue, OpcUa_Good );
		getAddressSpaceLink()->setReadTemperatureMaxstep(maxstepValue, OpcUa_Good );
		getAddressSpaceLink()->setReadTemperatureApplied(appliedValue, OpcUa_Good );
		getAddressSpaceLink()->setReadTemperatureStatus(statusValue, OpcUa_Good );
		getAddressSpaceLink()->setReadVoltageOrder(orderValue, OpcUa_Good );
		getAddressSpaceLink()->setReadVoltageMaxstep(maxstepValue, OpcUa_Good );
		getAddressSpaceLink()->setReadVoltageApplied(appliedValue, OpcUa_Good );
		getAddressSpaceLink()->setReadVoltageStatus(statusValue, OpcUa_Good );

		getParent()->getAddressSpaceLink()->setConnectionJammed(reply[0], OpcUa_Good);
		getParent()->getAddressSpaceLink()->setConnectionStatus(stateValue, OpcUa_Good);


		return OpcUa_Good;

	}
}











}


