
/* This is device body stub */


#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DChannelNo.h>
#include <ASChannelNo.h>


#include <DReadItems.h>

#include <DSaveOrderItem.h>

#include <DRestoreOrderItem.h>

#include <DWriteItem.h>

#include <iostream>
#include <statuscode.h>
#include <uadatetime.h>

namespace Device
{



/* sample ctr */
DChannelNo::DChannelNo (const Configuration::ChannelNo & config

                        , DDrawer * parent

                       ):

    	    m_parent(parent),
    	    m_channelNumber(config.channelNumber()),
    	    m_addressSpaceLink(0)


/* fill up constructor initialization list here */
{
    /* fill up constructor body here */
}

/* sample dtr */
DChannelNo::~DChannelNo ()
{
    /* remove children */

    BOOST_FOREACH( DReadItems *d, m_ReadItemss )
    {
        delete d;
    }

    BOOST_FOREACH( DSaveOrderItem *d, m_SaveOrderItems )
    {
        delete d;
    }

    BOOST_FOREACH( DRestoreOrderItem *d, m_RestoreOrderItems )
    {
        delete d;
    }

    BOOST_FOREACH( DWriteItem *d, m_WriteItems )
    {
        delete d;
    }

}

/* delegators for cachevariables and externalvariables */

/* Note: never directly call this function. */

/*UaStatus DChannelNo::writeWriteOrderValue ( const OpcUa_Float & v)
{

	msg.c_dlc = 0;
	m_ind = m_index;
	//printf("IN WRITE \n");

	if(m_ind ==  HV_WRITE_ORDER )
	{
		msg.c_data[0] = 0x08;
		msg.c_data[1] = m_channelNumber;
		msg.c_data[2] = (((int)v*10) & 0xFF00)>> 8 ;  //check the values for the order
		msg.c_data[3] = (((int)v*10) & 0x00FF);      //check the values for the order
		msg.c_dlc = 4;

		//printf("HV_WRITE_ORDER\n");

		getParent()->messageSend(msg);
		//getParent()->addDelay();
		//msgReply = getParent()->getParent()->msgReply;
	}

	return OpcUa_Good;
}*/

/* Note: never directly call this function. */

/*UaStatus DChannelNo::writeMaxstepValue ( const OpcUa_Float & v)
{
	msg.c_dlc = 0;
		m_ind = m_index;
		//printf("IN WRITE \n");

		if(m_ind ==   HV_WRITE_MAXSTEP )
		{
			msg.c_data[0] = 0x0B;
			msg.c_data[1] = m_channelNumber;
			msg.c_data[2] = (((int)v*10) & 0xFF00)>> 8 ;  //check the values for the order
			msg.c_data[3] = (((int)v*10) & 0x00FF);      //check the values for the order
			msg.c_dlc = 4;

			getParent()->messageSend(msg);

		}

		return OpcUa_Good;
}*/


/* address space linker */

void DChannelNo::linkAddressSpace( AddressSpace::ASChannelNo *addressSpaceLink)
{
    if (m_addressSpaceLink != 0)
    {
        /* signalize nice error from here. */
        abort();
    }
    else
        m_addressSpaceLink = addressSpaceLink;
}

/* add/remove */

void DChannelNo::add ( DReadItems *device)
{
    /* fill it up  --  only proposal given */
    m_ReadItemss.push_back (device);
}

void DChannelNo::add ( DSaveOrderItem *device)
{
    /* fill it up  --  only proposal given */
    m_SaveOrderItems.push_back (device);
}

void DChannelNo::add ( DRestoreOrderItem *device)
{
    /* fill it up  --  only proposal given */
    m_RestoreOrderItems.push_back (device);
}

void DChannelNo::add ( DWriteItem *device)
{
    /* fill it up  --  only proposal given */
    m_WriteItems.push_back (device);
}


/* to safely quit */
void DChannelNo::unlinkAllChildren ()
{
    PRINT_LOCATION();
    m_addressSpaceLink = 0;
    /* Fill up: call unlinkAllChildren on all children */

    BOOST_FOREACH( DReadItems *d, m_ReadItemss )
    {
        d->unlinkAllChildren();
    }

    BOOST_FOREACH( DSaveOrderItem *d, m_SaveOrderItems )
    {
        d->unlinkAllChildren();
    }

    BOOST_FOREACH( DRestoreOrderItem *d, m_RestoreOrderItems )
    {
        d->unlinkAllChildren();
    }

    BOOST_FOREACH( DWriteItem *d, m_WriteItems )
    {
        d->unlinkAllChildren();
    }


}

/* find methods for children */

/* find methods for children */

void DChannelNo::handleMessage (const CanMessage & msg)
{
	/*cout << __PRETTY_FUNCTION__ << endl;
	std::cout << __PRETTY_FUNCTION__ << std::endl;

		int com = msg.c_data[0];
		if (chanId > 0)
		{
			DChannelNo* node = getNodeByNodeId(chanId);
			node->handleMessage(msg);
		}
		else
		{
			std::cout << "Not implemented: message came for everybody" << std::endl;
		}
*/
}



void DChannelNo::readAllChannels()
{
	std::cout << "reading inside channels" << std::endl;
	BOOST_FOREACH(DReadItems* h, m_ReadItemss)
	{
			h->readManyItems();
	}
}




}


