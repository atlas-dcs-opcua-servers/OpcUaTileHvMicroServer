
/* This is device body stub */


#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DIDReadItem.h>
#include <ASIDReadItem.h>
#include <HVcan.h>
#include <DDrawer.h>

#include <iostream>




namespace Device
{



/* sample ctr */
DIDReadItem::DIDReadItem (const Configuration::IDReadItem & config

                          , DDrawer * parent

                         ):

    m_parent(parent),

    m_index(config.index()),
    m_addressSpaceLink(0)

/* fill up constructor initialization list here */
{
    /* fill up constructor body here */
}

/* sample dtr */
DIDReadItem::~DIDReadItem ()
{
    /* remove children */

}

/* delegators for cachevariables and externalvariables */


UaStatus DIDReadItem::readItemID()
{
	UaStatus ret;
	OpcUa_Int16 canNode = 0;
	UaDateTime sourceTime;

		msg.c_dlc = 0;
		m_ind = m_index;
		getParent()-> setRequestID(this);

	    if (m_ind == 0x25) // m_ind is hexadecimal
	    {
		printf("CanNode \n") ;
	         if ((NewSoftAddress < 1) & (NewSoftAddress > 16))
		 {
	 		printf("CanNode is Null going for CobID\n") ;
			NewSoftAddress = getParent()->m_drawerID;
			canNode = NewSoftAddress;
		 }

		 else
		 {	if ((NewSoftAddress > 0) & (NewSoftAddress <= 16))
		 	{printf("CanNode is NewSoftAddress\n") ;
				canNode = NewSoftAddress;}
			else
			{
					printf("CanNode is cobID\n") ;
					canNode = getParent()->m_drawerID;
			}
			}
        }

	    getAddressSpaceLink()->setIdValue(canNode, OpcUa_Good, UaDateTime::now());

	    getParent()->freeRequestID();

    sourceTime = UaDateTime::now();
    return ret;
}




/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */
UaStatus DIDReadItem::readItemValue (
    OpcUa_Double &value,
    UaDateTime &sourceTime
)
{

	UaStatus ret;
	OpcUa_Int16 canNode = 0;


		msg.c_dlc = 0;
		m_ind = m_index;
		getParent()-> setRequestID(this);

	    if (m_ind == 0x25) // m_ind is hexadecimal
	    {
		printf("CanNode \n") ;
	         if ((NewSoftAddress < 1) & (NewSoftAddress > 16))
		 {
	 		printf("CanNode is Null going for CobID\n") ;
			NewSoftAddress = getParent()->m_drawerID;
			canNode = NewSoftAddress;
		 }

		 else
		 {	if ((NewSoftAddress > 0) & (NewSoftAddress <= 16))
		 	{printf("CanNode is NewSoftAddress\n") ;
				canNode = NewSoftAddress;}
			else
			{
					printf("CanNode is cobID\n") ;
					canNode = getParent()->m_drawerID;
			}
			}
        }

	    getAddressSpaceLink()->setIdValue(canNode, OpcUa_Good, UaDateTime::now());

	    getParent()->freeRequestID();

    sourceTime = UaDateTime::now();
    return ret;
}




/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */
UaStatus DIDReadItem::writeItemValue (
    OpcUa_Double &value
)
{
    return OpcUa_BadNotImplemented;
}


/* address space linker */

void DIDReadItem::linkAddressSpace( AddressSpace::ASIDReadItem *addressSpaceLink)
{
    if (m_addressSpaceLink != 0)
    {
        /* signalize nice error from here. */
        abort();
    }
    else
        m_addressSpaceLink = addressSpaceLink;
}

/* add/remove */


/* to safely quit */
void DIDReadItem::unlinkAllChildren ()
{
    PRINT_LOCATION();
    m_addressSpaceLink = 0;
    /* Fill up: call unlinkAllChildren on all children */


}

/* find methods for children */

void DIDReadItem::passID(const CanMessage rmsg)
{
	printf("CanNode ID\n") ;
}

}


