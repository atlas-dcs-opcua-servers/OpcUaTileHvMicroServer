
/* This is device body stub */


#include <boost/foreach.hpp>
#include <Configuration.hxx>
#include <DAdvancedHeater.h>
#include <ASAdvancedHeater.h>
#include <iostream>



namespace Device
{

/* sample ctr */
DAdvancedHeater::DAdvancedHeater (const Configuration::AdvancedHeater & config

                                  , DRoot * parent

                                 ):

    m_parent(parent),

    m_addressSpaceLink(0), m_currentTemperature(0)

/* fill up constructor initializat<ion list here */
{
    /* fill up constructor body here */
}

/* sample dtr */
DAdvancedHeater::~DAdvancedHeater ()
{
    /* remove children */

}

/* delegators for cachevariables and externalvariables */

/* Note: never directly call this function. */

UaStatus DAdvancedHeater::writeDesiredTemperature ( const OpcUa_Float & v)
{
    //return OpcUa_BadNotImplemented;
	//printf("2)in DAdvancedHeater, value is = %f \n", v);
  std::cout << v << std::endl;
	//getAddressSpaceLink()->setDesiredTemperature (v, OpcUa_Good, UaDateTime::now());
  return OpcUa_Good;
}


/* address space linker */

void DAdvancedHeater::linkAddressSpace( AddressSpace::ASAdvancedHeater *addressSpaceLink)
{
    if (m_addressSpaceLink != 0)
    {
        /* signalize nice error from here. */
        abort();
    }
    else
        m_addressSpaceLink = addressSpaceLink;
}

/* add/remove */


/* to safely quit */
void DAdvancedHeater::unlinkAllChildren ()
{
    PRINT_LOCATION();
    m_addressSpaceLink = 0;
    /* Fill up: call unlinkAllChildren on all children */
}

/* find methods for children */


void DAdvancedHeater::update ()
{
	float desiredTemperature;
	if (! getAddressSpaceLink()->getDesiredTemperature(desiredTemperature).isGood())
		return;
	//m_currentTemperature = 0.9*m_currentTemperature + 0.1*desiredTemperature;
	m_currentTemperature = 0.9*m_currentTemperature + 0.1*desiredTemperature;
	getAddressSpaceLink()->setCurrentTemperature(m_currentTemperature, OpcUa_Good, UaDateTime::now());
}

}



