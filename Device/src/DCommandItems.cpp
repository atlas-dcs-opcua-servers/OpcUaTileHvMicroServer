
	/* This is device body stub */


	#include <boost/foreach.hpp>

	#include <Configuration.hxx>

	#include <DCommandItems.h>
	#include <ASCommandItems.h>
	#include <HVcan.h>
	#include <DDrawer.h>
	#include <DChannelNo.h>
	#include <ASChannelNo.h>
	#include <iostream>


	namespace Device
	{

	/* sample ctr */
	DCommandItems::DCommandItems (const Configuration::CommandItems & config

	                              , DDrawer * parent

	                             ):

	    m_parent(parent),

	    m_index(config.index()),
	    m_addressSpaceLink(0),
		m_card(config.cardValue()),
		m_address(config.addressValue()),
		m_polyn(config.polynomialValue()),
		m_deviceDacAdc(config.deviceDacAdc())


	/* fill up constructor initialization list here */
	{
	    /* fill up constructor body here */
	}

	/* sample dtr */
	DCommandItems::~DCommandItems ()
	{
	    /* remove children */

	}

	/* delegators for cachevariables and externalvariables */




UaStatus DCommandItems::readGlobalStatus()
{

	UaStatus ret;
	OpcUa_Int16 resetreg, warnstat, alarstat, statusbyte, nb_IRQ, statebytevalue;
	UaDateTime sourceTime;



	msg.c_dlc = 0;
	msgReply.c_dlc = 0;
	m_ind = m_index;



	getParent()-> setRequestCOMMANDITEMS(this);


	if (m_ind == HV_READ_GLOBAL_STATUS) // m_ind is hexadecimal
	{
		msg.c_data[0] = 0x05;
		msg.c_data[1] = 0x00;
		msg.c_dlc = 2;
		printf("HV_READ_GLOBAL_STATUS\n");
	}


	msg.c_id = (0x100 + (getParent()->m_drawerID));

	printf("msg.c_id = %x \n", msg.c_id);
	printf("msg.c_dlc = %x \n", msg.c_dlc);
	for (int y = 0; y< msg.c_dlc; y++)
	{
		printf("msg.c_data[y] = %x \n", msg.c_data[y]);
	}


	getParent()->messageSend(msg);
	printf("MESSAGE SENT  \n");
	getParent()->addDelay();
	printf("MESSAGE RECEIVED  \n");
	msgReply = getParent()->msgReply;


	if (m_ind == HV_READ_GLOBAL_STATUS) // m_ind is hexadecimal
	{
		statebytevalue = msgReply.c_data[0];
		resetreg = (unsigned char)msgReply.c_data[1] ;
		warnstat = ((unsigned char)msgReply.c_data[2]<< 8) + (unsigned char)msgReply.c_data[3];
		alarstat = ((unsigned char)msgReply.c_data[4]<< 8) + (unsigned char)msgReply.c_data[5];
		nb_IRQ = ((unsigned char)msgReply.c_data[6]<< 8) + (unsigned char)msgReply.c_data[7];
		//printf("HV_READ_GLOBAL_STATUS\n");
	}



	   getAddressSpaceLink()->setNbIRQValue(nb_IRQ, OpcUa_Good, UaDateTime::now());
	   getAddressSpaceLink()->setResetRegValue(resetreg, OpcUa_Good, UaDateTime::now());
	   getAddressSpaceLink()->setWarningStatValue(warnstat, OpcUa_Good, UaDateTime::now());
	   getAddressSpaceLink()->setAlarmStatValue(alarstat, OpcUa_Good, UaDateTime::now());
	   getAddressSpaceLink()->setStateValue(statebytevalue, OpcUa_Good, UaDateTime::now());





	getParent()-> freeRequestCOMMANDITEMS()	;
	sourceTime = UaDateTime::now();
	return ret;
}







	/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */
	UaStatus DCommandItems::readItemValue (
	    OpcUa_Double &value,
	    UaDateTime &sourceTime
	)
	{

		UaStatus ret;
		OpcUa_Int16 command = -1, verificationOnOff = -1 , type = -1, typeReply  = -1 , card = -1, replyCard = -1, address = -1, DACADC = -1;
		OpcUa_Int16 statebyte, statebyte2, resetreg, warnstat, alarstat, statusbyte;
		OpcUa_Int16 StateByte2, resetReg, warnStat, alarStat, statusByte, StateByte;
		OpcUa_Int16 okNb = -1, badNb = -1, nb_IRQ = -1, Mini = -1 , Maxi = -1, maxstep = -1, applied = -1, goodBad = -1;
		OpcUa_Int16 softAddress = -1, hardAddress = -1;
		OpcUa_Int16 DAC = -1, ADC = -1, data = -1, order = -1, channel = -1, channel2 = -1;
		OpcUa_UInt32 info = 0;
		OpcUa_Int16 canNode = 0, statebytevalue = -1, statusbytevalue = -1, statebytevalue2 = -1;
		OpcUa_Int16 chanStateONOFF = 0 , channelSN = 0, canChanError = 0 ,chanOrder = 0, chanMax = 0, chanApplied = 0 , chanPSUStatusGlobal = 0, 		canpsuStatus = 0, chanLocalRemote = 0, chanRemote = 0, chanLocal = 0, chanVeto = 0, chanNav = 0, chanMeasures = 0;


			msg.c_dlc = 0;
			msgReply.c_dlc = 0;
			m_ind = m_index;


		    if (m_ind == HV_SAFETY_MODE ) // m_ind is hexadecimal
		    {
				  msg.c_data[0] = 0x02;
					msg.c_data[1] = 0x00;
					msg.c_dlc = 2;
				  printf("HV_SAFETY_MODE\n");
		    }



		    if (m_ind == HV_READ_GLOBAL_STATUS) // m_ind is hexadecimal
		    {
		      msg.c_data[0] = 0x05;
		      msg.c_data[1] = 0x00;
		      msg.c_dlc = 2;
		      printf("HV_READ_GLOBAL_STATUS\n");


		    }

		    if (m_ind == HV_VALID_REF_CHECK) // m_ind is hexadecimal
		    {
				  msg.c_data[0] = 0x06;
				  msg.c_data[1] = 0x00;
				  msg.c_dlc = 2;
				  printf("HV_VALID_REF_CHECK\n");
		    }


		    if(m_ind ==  EEPROM_Read )
		    {
		    	msg.c_data[0] = EEPROM_Read;
		    	msg.c_data[1] = m_card;
				msg.c_data[2] = (0xFF00 & m_address)>>8;
				msg.c_data[3] = (0x00FF & m_address);    //check the values for the order
				msg.c_dlc = 4;
				printf("EEPROM_Read\n");
		    }

		    if(m_ind ==  EEPROM_CheckSum )
		    {
				msg.c_data[0] = EEPROM_CheckSum;
				msg.c_data[1] = m_card;
				msg.c_dlc = 2;
				printf("EEPROM_CheckSum\n");
		    }




		    if (m_ind == HV_LOAD_CALIBRATION_PARAMETERS) // m_ind is hexadecimal
		    {
				  msg.c_data[0] = 0x1C;
				  msg.c_data[1] = m_card;
				  msg.c_dlc = 2;
				  printf("HV_LOAD_CALIBRATION_PARAMETERS\n");
		     }



		    if (m_ind == HV_LOAD_DEFAULT_CALIBRATION_PARAMETERS ) // m_ind is hexadecimal
		    {
				  msg.c_data[0] = 0x1D;
				  msg.c_data[1] = m_card;
				  msg.c_dlc = 2;
				  printf("HV_LOAD_DEFAULT_CALIBRATION_PARAMETERS \n");
		    }




	/********************************************************************************
	 *
	 * 		channel required commands
	 *
	 *
	 * */

	/*	    if (((0xFF000000 & m_ind)>>24) == HV_READ_CALIBRATIO_PARAMETERS) // m_ind is hexadecimal
		    {
		      msg.c_data[0] = 0x22;
		      msg.c_data[1] = getParent()->m_channelNumber;
		      msg.c_data[2] = (0x00F00000 & (m_ind))>>20;
		      msg.c_data[3] = (0x000F0000 & (m_ind))>>16;
		      msg.c_dlc = 4;
		      printf("HV_READ_CALIBRATIO_PARAMETERS \n");
			command = msg.c_data[0];
			channel = msg.c_data[1];
			DACADC = msg.c_data[2];

		    }


	if (m_ind == HV_GET_CHANNEL_RAW_VALUES ) // m_ind is hexadecimal
	    {
		 	      msg.c_data[0] = 0x1E;
		 	      msg.c_data[1] = getParent()->m_channelNumber;
		 	      msg.c_dlc = 2;
		 	      printf("HV_GET_CHANNEL_RAW_VALUES \n");
		 		command = msg.c_data[0];
		 		channel = msg.c_data[1];

		 	    }


		    if (m_ind == HV_RELOAD_ORDER_INTO_THE_LOOP_DAC) // m_ind is hexadecimal
		    {
		      msg.c_data[0] = 0x24;
		      msg.c_data[1] = getParent()-> m_channelNumber;
		      msg.c_dlc = 2;
		      printf("HV_RELOAD_ORDER_INTO_THE_LOOP_DAC \n");
			command = msg.c_data[0];
			channel = msg.c_data[1];

		    }*/

	/*
	 * End of
	 *
	 * */





		    	msg.c_id = (0x100 + (getParent()->m_drawerID));

		   	    printf("msg.c_id = %x \n", msg.c_id);
		   	    printf("msg.c_dlc = %x \n", msg.c_dlc);
		   	    for (int y = 0; y< msg.c_dlc; y++)
		   	    {
		   	      	 printf("msg.c_data[y] = %x \n", msg.c_data[y]);
		   	    }


		      	    getParent()->messageSend(msg);
		      	    printf("MESSAGE SENT  \n");
		      	    getParent()->addDelay();
		      		printf("MESSAGE RECEIVED  \n");
		      		msgReply = getParent()->msgReply;


	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////   DECODING THE REPLY
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////







	    if (m_ind == HV_SAFETY_MODE ) // m_ind is hexadecimal
	    {
	    	 statebytevalue = msgReply.c_data[0];
	     	 State_Byte = statebytevalue;
	      //printf("\nHV_SAFETY_MODE\n");
	    }

	    if (m_ind == HV_READ_GLOBAL_STATUS) // m_ind is hexadecimal
	    {
	     	statebytevalue = msgReply.c_data[0];
	     	State_Byte = statebytevalue;
	      	resetreg = msgReply.c_data[1] ;
			warnstat = (msgReply.c_data[2]<< 8) + msgReply.c_data[3];
			alarstat = (msgReply.c_data[4]<< 8) + msgReply.c_data[5];
			nb_IRQ = (msgReply.c_data[6]<< 8) + msgReply.c_data[7];
	       	//printf("HV_READ_GLOBAL_STATUS\n");
	     }


	    if (m_ind == HV_VALID_REF_CHECK) // m_ind is hexadecimal
	    {
	        statebytevalue = msgReply.c_data[0];
			State_Byte = statebytevalue;
			statebytevalue2 = (msgReply.c_data[1]);
			Mini = (msgReply.c_data[2]<< 8 ) + (msgReply.c_data[3]);
			Maxi = (msgReply.c_data[4]<< 8 ) + (msgReply.c_data[5]);
	        //printf("\nMini-> %d \n", Mini);
			//printf("Maxi-> %d \n", Maxi);
			//printf("\nHV_VALID_REF_CHECK\n");
	    }


	    if((0xFF & (m_ind>>8)) ==  EEPROM_Read )
	    {
			statebytevalue = msgReply.c_data[0];
			State_Byte = statebytevalue;
			card = msgReply.c_data[1];
			address = (msgReply.c_data[2] << 8) + msgReply.c_data[3];    //check the values for the order
			data = 	((unsigned char)msgReply.c_data[4] << 24) + ((unsigned char)msgReply.c_data[5] << 16) + ((unsigned char)msgReply.c_data[6] << 8) + (unsigned char)msgReply.c_data[7];
			//printf("EEPROM_Read\n");
	    }


	    if((0xFF & (m_ind>>8)) ==  EEPROM_CheckSum )
	    {
			statebytevalue = msgReply.c_data[0];
			State_Byte = statebytevalue;
			replyCard = msgReply.c_data[1];
			address = (msgReply.c_data[2] << 8) + msgReply.c_data[3];    //check the values for the order
			data = 	((unsigned char)msgReply.c_data[4] << 24) + ((unsigned char)msgReply.c_data[5] << 16) + ((unsigned char)msgReply.c_data[6] << 8) + (unsigned char)msgReply.c_data[7];
			//printf("EEPROM_CheckSum\n");
	    }



	    if ((0xFF & (m_ind>>8)) == HV_LOAD_CALIBRATION_PARAMETERS) // m_ind is hexadecimal
	    {
			  statebytevalue = msgReply.c_data[0];
			  /*StateByte = State(msgReply.c_data[0]);*/State_Byte = statebytevalue;
			  replyCard = msgReply.c_data[1];
			  goodBad = (msgReply.c_data[2]<<8) + msgReply.c_data[3];
			  //printf("HV_LOAD_CALIBRATION_PARAMETERS\n");

	     }



	    if ((0xFF & (m_ind>>8)) == HV_LOAD_DEFAULT_CALIBRATION_PARAMETERS ) // m_ind is hexadecimal
	    {
			  statebytevalue = msgReply.c_data[0];
			  /*StateByte = State(msgReply.c_data[0]);*/State_Byte = statebytevalue;
			  card = msgReply.c_data[1];
			  //printf("HV_LOAD_DEFAULT_CALIBRATION_PARAMETERS \n");
	    }




	    /*channel specific extra commands*/

/*
	    if (((0xFF000000 & m_ind)>>24) == HV_READ_CALIBRATIO_PARAMETERS)    // m_ind is hexadecimal
	    {
			  statebytevalue = msgReply.c_data[0];
			  State_Byte = statebytevalue;
			  channel2 = msgReply.c_data[1];

				int buff1 = (msgReply.c_data[2] << 8)+(msgReply.c_data[3]);
				int buff2 = (msgReply.c_data[4] << 8)+(msgReply.c_data[5]);

				value = (buff1 << 8)+(buff2);
				//value = ((unsigned char)msgReply.c_data2]<<24)+((unsigned char)msgReply.c_data3]<<16)+((unsigned char)msgReply.c_data4]<<8)+((unsigned char)msgReply.c_data5]);
				//printf("HV_READ_CALIBRATIO_PARAMETERS for value %d \n", value);

	    }


	    if ((0xFF & (m_ind>>8)) == HV_GET_CHANNEL_RAW_VALUES ) // m_ind is hexadecimal
	    {
			  statebytevalue = msgReply.c_data[0];
			  State_Byte = statebytevalue;
			  statusbytevalue = msgReply.c_data[1];
			  DAC = (msgReply.c_data[2]<<8) + msgReply.c_data[3];
			  ADC = (msgReply.c_data[4]<<8) + msgReply.c_data[5];

			  //printf("HV_GET_CHANNEL_RAW_VALUES \n");

	    }

	    if (m_ind == HV_RELOAD_ORDER_INTO_THE_LOOP_DAC) // m_ind is hexadecimal
	 	{
	 		statebytevalue = msgReply.c_data[0];
	 		State_Byte = statebytevalue;
	 		//printf("HV_RELOAD_ORDER_INTO_THE_LOOP_DAC \n");
	    }


*/

	/*
		   	    statusValue = msgReply.c_data[0];
		   	    getAddressSpaceLink()->setVerificationOnOffValue(statusValue, OpcUa_Good, UaDateTime::now());

		   	    getParent()->freeRequestCONTROL();
	*/





		       sourceTime = UaDateTime::now();
		       return ret;
}



/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */
UaStatus DCommandItems::writeItemValue (
    OpcUa_Double &value)
{
    return OpcUa_BadNotImplemented;
}




UaStatus DCommandItems::sendMessage(const CanMessage &msg)
{
	UaStatus ret;
	getParent()->getParent()->sendMessage(msg);
	printf("message SENT\n ");
	ret = OpcUa_Good;
	return ret;
}




/* address space linker */

void DCommandItems::linkAddressSpace( AddressSpace::ASCommandItems *addressSpaceLink)
{
    if (m_addressSpaceLink != 0)
    {
        /* signalize nice error from here. */
        abort();
    }
    else
        m_addressSpaceLink = addressSpaceLink;
}

/* add/remove */


/* to safely quit */
void DCommandItems::unlinkAllChildren ()
{
    PRINT_LOCATION();
    m_addressSpaceLink = 0;
    /* Fill up: call unlinkAllChildren on all children */


}

/* find methods for children */

void DCommandItems::passCOMMANDITEMS(const CanMessage rmsg)
{
	msgReply = getParent()-> passMSG(rmsg);
}





}


