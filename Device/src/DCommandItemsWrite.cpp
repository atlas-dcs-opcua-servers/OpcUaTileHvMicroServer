
/* This is device body stub */


#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DCommandItemsWrite.h>
#include <ASCommandItemsWrite.h>


	#include <HVcan.h>
	#include <DDrawer.h>
	#include <DChannelNo.h>
	#include <ASChannelNo.h>
	#include <iostream>


namespace Device
{



/* sample ctr */
DCommandItemsWrite::DCommandItemsWrite (const Configuration::CommandItemsWrite & config

										, DDrawer * parent):

	    	    m_parent(parent),
				m_index(config.index()),
	    	    m_addressSpaceLink(0),
	    		m_card(config.cardValue()),
	    		m_address(config.addressValue()),
	    		m_polyn(config.polynomialValue()),
				m_infoType(config.infoType()),
	    		m_deviceDacAdc(config.deviceDacAdc())

/* fill up constructor initialization list here */
{
    /* fill up constructor body here */
}

/* sample dtr */
DCommandItemsWrite::~DCommandItemsWrite ()
{
    /* remove children */

}

/* delegators for cachevariables and externalvariables */

/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */
UaStatus DCommandItemsWrite::readItemValue (
    OpcUa_Float &value,
    UaDateTime &sourceTime
)
{
    sourceTime = UaDateTime::now();
    return OpcUa_BadNotImplemented;
}

/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */
UaStatus DCommandItemsWrite::writeItemValue (
    OpcUa_Float &value
)
{


	UaStatus ret;
	OpcUa_Int16 card = -1;
	OpcUa_Int16 data = -1;
	OpcUa_UInt32 info = 0;


				msg.c_dlc = 0;
				msgReply.c_dlc = 0;
				m_ind = m_index;



	if (m_ind == HV_SAFETY_MODE ) // m_ind is hexadecimal
	{
		msg.c_data[0] = 0x02;
		msg.c_data[1] = 0x00;
  		msg.c_data[2] =  ((int)value & 0xFF00) >> 8 ;
		msg.c_data[3] =  ((int)value & 0x00FF) ;
		msg.c_dlc = 4;
		//printf("HV_SAFETY_MODE2");

		value = (msg.c_data[2]<<8) + msg.c_data[3];

		}


	if(m_ind ==  HV_INFO_WRITE )
	{
		msg.c_data[0] = 0x0D;
		msg.c_data[1] = m_infoType;
		msg.c_data[2] = ((int)value & 0xFF000000)>> 24 ;  //check the values for the order
		msg.c_data[3] = ((int)value & 0x00FF0000)>> 16;      //check the values for the order
		msg.c_data[4] = ((int)value & 0x0000FF00)>> 8 ;  //check the values for the order
		msg.c_data[5] = ((int)value & 0x000000FF);
		msg.c_dlc = 6;
		//printf("HV_INFO_WRITE");



		info = ((unsigned char)msg.c_data[2]<<24)+((unsigned char)msg.c_data[3]<<16)+((unsigned char)msg.c_data[4]<<8)+((unsigned char)msg.c_data[5]);


	}


	if(m_ind ==  EEPROM_Write )
	{
		msg.c_data[0] = EEPROM_Write ;
		msg.c_data[1] = m_card ;
		msg.c_data[2] = (0xFF00 & m_address)>> 8;    //check the values for the order
		msg.c_data[3] = (0x00FF & m_address) ;       //check the values for the order
		msg.c_data[4] = ((int)value & 0xFF000000)>> 24 ;  //check the values for the order
		msg.c_data[5] = ((int)value & 0x00FF0000)>> 16;
		msg.c_data[6] = ((int)value & 0x0000FF00)>> 8;
		msg.c_data[7] = ((int)value & 0x000000FF);
		msg.c_dlc = 8;


		//printf("EEPROM_Write");

		data = ((unsigned char)msg.c_data[4]<<24)+((unsigned char)msg.c_data[5]<<16)+((unsigned char)msg.c_data[6]<<8)+((unsigned char)msg.c_data[7]);


	}


   /* if (m_ind == HV_WRITE_CALIBRATION_PARAMETERS) // m_ind is hexadecimal
    {
      msg.c_data[0] = 0x23;
      msg.c_data[1] = getChannelIndex();
      msg.c_data[2] = m_deviceDacAdc;
      msg.c_data[3] = m_polyn ;
      msg.c_data[4] = ((int)value & 0xFF000000)>>24;
      msg.c_data[5] = ((int)value & 0x00FF0000)>>16;
      msg.c_data[6] = ((int)value & 0x0000FF00)>>8;
      msg.c_data[7] = ((int)value & 0x000000FF);
      msg.c_dlc = 8;

      //printf("HV_WRITE_CALIBRATION_PARAMETERS \n");



	val = ((unsigned char)msg.c_data[4]<<24)+((unsigned char)msg.c_data[5]<<16)+((unsigned char)msg.c_data[6]<<8)+((unsigned char)msg.c_data[7]);

		 }

		 */








    msg.c_id = (0x100 + (getParent()->m_drawerID));

    		   	    printf("msg.c_id = %x \n", msg.c_id);
    		   	    printf("msg.c_dlc = %x \n", msg.c_dlc);
    		   	    for (int y = 0; y< msg.c_dlc; y++)
    		   	    {
    		   	      	 printf("msg.c_data[y] = %x \n", msg.c_data[y]);
    		   	    }


    		      	    getParent()->messageSend(msg);
    		      	    printf("MESSAGE SENT  \n");
    		      	    getParent()->addDelay();
    		      		printf("MESSAGE RECEIVED  \n");
    		      		msgReply = getParent()->msgReply;









    		      		if (m_ind == HV_SAFETY_MODE ) // m_ind is hexadecimal
    		      		{

    		      		     statebyteWrite = msgReply.c_data[0];
    		      		     State_Byte = statebyteWrite;
    		      			//printf("HV_SAFETY_MODE2");
    		      		}



    		      		if(m_ind ==  HV_INFO_WRITE )
    		      		{

    		      			statebyteWrite = msgReply.c_data[0];
    		      			State_Byte = statebyteWrite;
    		      			//printf("HV_WRITE_INFO");

    		      		}

    		      		if(m_ind ==  EEPROM_Write )
    		      		{
    		      			statebyteWrite = msgReply.c_data[0];
    		      			State_Byte = statebyteWrite;
    		      			card = msgReply.c_data[1];
    		      			//printf("EEPROM_Write");
    		      		}




    		      	    if (m_ind == HV_WRITE_CALIBRATION_PARAMETERS) // m_ind is hexadecimal
    		      	    {
    		      		   	statebyteWrite = msgReply.c_data[0];State_Byte = statebyteWrite;
    		      	    	//printf("HV_WRITE_CALIBRATION_PARAMETERS \n");
    		      	    }



    return OpcUa_Good;
}


/* address space linker */


UaStatus DCommandItemsWrite::sendMessage(const CanMessage &msg)
{
	UaStatus ret;
	getParent()->getParent()->sendMessage(msg);
	printf("message SENT\n ");
	ret = OpcUa_Good;
	return ret;
}



/* address space linker */

void DCommandItemsWrite::linkAddressSpace( AddressSpace::ASCommandItemsWrite *addressSpaceLink)
{
    if (m_addressSpaceLink != 0)
    {
        /* signalize nice error from here. */
        abort();
    }
    else
        m_addressSpaceLink = addressSpaceLink;
}

/* add/remove */


/* to safely quit */
void DCommandItemsWrite::unlinkAllChildren ()
{
    PRINT_LOCATION();
    m_addressSpaceLink = 0;
    /* Fill up: call unlinkAllChildren on all children */


}

/* find methods for children */

void DCommandItemsWrite::passCOMMANDITEMSWRITE(const CanMessage rmsg)
{
	msgReply = getParent()-> passMSG(rmsg);
}





}


