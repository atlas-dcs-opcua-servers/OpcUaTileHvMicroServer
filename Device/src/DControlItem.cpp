
/* This is device body stub */


#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DControlItem.h>
#include <ASControlItem.h>
#include <HVcan.h>
#include <DDrawer.h>

#include <iostream>




namespace Device
{



/* sample ctr */
DControlItem::DControlItem (const Configuration::ControlItem & config

                            , DDrawer * parent

                           ):

    m_parent(parent),

    m_index(config.index()),
    m_addressSpaceLink(0)

/* fill up constructor initialization list here */
{
    /* fill up constructor body here */
}

/* sample dtr */
DControlItem::~DControlItem ()
{
    /* remove children */

}

/* delegators for cachevariables and externalvariables */

/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */

UaStatus DControlItem::writeVerificationOnOffValue ( const OpcUa_Int32 & v)
{
	UaStatus ret;
		msg.c_dlc = 0;
		m_ind = m_index;
		UaDateTime sourceTime;

		getParent()-> setRequestCONTROL(this);


	    if ((0xFF & (m_ind>>8))== HV_CONTROL_MODE) // m_ind is hexadecimal
	    {
		      msg.c_data[0] = 0x03;
		      msg.c_data[1] = (0xFF & (m_ind));
		      msg.c_dlc = 2;
		      printf("HV_CONTROL_MODE\n");
	    }


		    msg.c_id = (0x100 + (getParent()->m_drawerID));

		    printf("msg.c_id = %x \n", msg.c_id);
		    printf("msg.c_dlc = %x \n", msg.c_dlc);
		    for (int y = 0; y< msg.c_dlc; y++)
		    {
		      	 printf("msg.c_data[y] = %x \n", msg.c_data[y]);
		    }


	   		getParent()->messageSend(msg);
	   	    printf("MESSAGE SENT  \n");
	   	    getParent()->addDelay();
	   		printf("MESSAGE RECEIVED  \n");
	   		msgReply = getParent()->msgReply;


		    //statusValue = msgReply.c_data[0];
		    //getAddressSpaceLink()->setVerificationOnOffValue(statusValue, OpcUa_Good, UaDateTime::now());

		    getParent()->freeRequestCONTROL();


	    sourceTime = UaDateTime::now();
	    return ret;
}







UaStatus DControlItem::readItemValue (
    OpcUa_Double &value,UaDateTime &sourceTime)
{
	return OpcUa_BadNotImplemented;

}







/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */
UaStatus DControlItem::writeItemValue (
    OpcUa_Double &value
)
{
    return OpcUa_BadNotImplemented;
}


/* address space linker */

void DControlItem::linkAddressSpace( AddressSpace::ASControlItem *addressSpaceLink)
{
    if (m_addressSpaceLink != 0)
    {
        /* signalize nice error from here. */
        abort();
    }
    else
        m_addressSpaceLink = addressSpaceLink;
}

/* add/remove */


/* to safely quit */
void DControlItem::unlinkAllChildren ()
{
    PRINT_LOCATION();
    m_addressSpaceLink = 0;
    /* Fill up: call unlinkAllChildren on all children */
}

/* find methods for children */

void DControlItem::passCONTROL(const CanMessage rmsg)
{
	msgReply = getParent()-> passMSG(rmsg);
}

}


