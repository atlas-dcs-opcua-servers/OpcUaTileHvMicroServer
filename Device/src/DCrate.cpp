/*
 * DCrate.cpp
 *
 *  Created on: Mar, 2015
 *      Author: pagoncal
 */

#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DCrate.h>
#include <ASCrate.h>


#include <DCanPSU.h>
#include <HVcan.h>



namespace Device
{




// 1111111111111111111111111111111111111111111111111111111111111111111111111
// 1     GENERATED CODE STARTS HERE AND FINISHES AT SECTION 2              1
// 1     Users don't modify this code!!!!                                  1
// 1     If you modify this code you may start a fire or a flood somewhere,1
// 1     and some human being may possible cease to exist. You don't want  1
// 1     to be charged with that!                                          1
// 1111111111111111111111111111111111111111111111111111111111111111111111111






// 2222222222222222222222222222222222222222222222222222222222222222222222222
// 2     SEMI CUSTOM CODE STARTS HERE AND FINISHES AT SECTION 3            2
// 2     (code for which only stubs were generated automatically)          2
// 2     You should add the implementation but dont alter the headers      2
// 2     (apart from constructor, in which you should complete initializati2
// 2     on list)                                                          2
// 2222222222222222222222222222222222222222222222222222222222222222222222222

/* sample ctr */
DCrate::DCrate (const Configuration::Crate & config, DCanBus * parent):
    Base_DCrate( config
    		, parent),

	m_communicationController( this )

/* fill up constructor initialization list here */
{
    /* fill up constructor body here */

	m_communicationController.addPeriodicObject( PeriodicCommunicationObject(
		CommunicationObject(CANPSU_Parameter, HV_READ_GLOBAL_STATUS, crateID(), 0, boost::bind(&DCrate::handleReadGlobalStatus, this, _1, _2, _3), /*timeout*/ 1.0 ),
				/* period */ 10.0 ));


}

/* sample dtr */
DCrate::~DCrate ()
{
}

/* delegators for cachevariables and externalvariables */

/* Note: never directly call this function. */

UaStatus DCrate::writeResetHVMicro ( const OpcUa_Boolean & v)
{
	if (v != true)
	 		{return OpcUa_BadOutOfRange;}

		CommunicationObject request (CANPSU_Parameter,  0x01, crateID(), 0, boost::bind(&DCrate::handleWriteItems, this, _1, _2, _3), 1.0 );
			m_communicationController.requestObject( request, /* highest prio */ true );


			return OpcUa_Good;
}

/* Note: never directly call this function. */

UaStatus DCrate::writeSoftwareVeto ( const OpcUa_Boolean & v)
{

//clear/set software veto

	unsigned char list[] = {0,
						(unsigned char)((v & 0xFF00)>> 8 ),
						(unsigned char)((v & 0x00FF))};



	CommunicationObject request (CANPSU_Parameter,  0x02, crateID(), list, sizeof(list), boost::bind(&DCrate::handleWriteItems, this, _1, _2, _3), 1.0 );
		m_communicationController.requestObject( request, /* highest prio */ true );

	return OpcUa_Good;
}

/* Note: never directly call this function. */

UaStatus DCrate::writeSetClearTrips ( const OpcUa_Boolean & v)
{
	//writeSetClearTrips

	unsigned char list[] = {0, 0,
				(unsigned char)((v))};



		CommunicationObject request (CANPSU_Parameter,  0x0A, crateID(), list, sizeof(list), boost::bind(&DCrate::handleWriteItems, this, _1, _2, _3), 1.0 );
			m_communicationController.requestObject( request, /* highest prio */ true );

		return OpcUa_Good;
}

UaStatus DCrate::writeInfoTrigger ( const OpcUa_Boolean & v)
{

 	if (v != true)
 		{return OpcUa_BadOutOfRange;}

	for(int type = 7; type <= 10; type++)
	{
		CommunicationObject requestInfo (CANPSU_Parameter,  HV_INFO_READ, crateID(), type, boost::bind(&DCrate::handleReadInfo, this, _1, _2, _3), 1.0 );
			m_communicationController.requestObject( requestInfo, /* highest prio */ true );
	}
	return OpcUa_Good;
}



// 3333333333333333333333333333333333333333333333333333333333333333333333333
// 3     FULLY CUSTOM CODE STARTS HERE                                     3
// 3     Below you put bodies for custom methods defined for this class.   3
// 3     You can do whatever you want, but please be decent.               3
// 3333333333333333333333333333333333333333333333333333333333333333333333333


void DCrate::handleMessage (const CanMessage & msg)
{

	m_communicationController.handleMessage( msg );

	return;

}



void DCrate::sendMessage( const CanMessage & msg)
{
	getParent()->sendMessage(msg);
}

void DCrate::tick ()
{
	m_communicationController.tick();
}




UaStatus DCrate::handleWriteItems ( const unsigned char reply[], unsigned int msgLen, bool valid)
{
	OpcUa_Int16 statebytevalue = reply[0];

		if(statebytevalue != 0 || msgLen != 2 || valid == false)
		{
				// TODO: proper null value
				//getAddressSpaceLink()->setInfoValue(  OpcUa_Bad);
			getAddressSpaceLink()->setConnectionJammed(1, OpcUa_Good);
			getAddressSpaceLink()->setConnectionStatus(1, OpcUa_Good);

				return OpcUa_Bad;
		}
		else
		{
			return OpcUa_Good;
		}

		getAddressSpaceLink()->setConnectionJammed(reply[0], OpcUa_Good);
		getAddressSpaceLink()->setConnectionStatus(reply[0], OpcUa_Good);

}


UaStatus DCrate::handleReadGlobalStatus ( const unsigned char reply[], unsigned int msgLen, bool valid)
{

	OpcUa_Int16 resetreg, warnstat, alarstat, nb_IRQ, statebytevalue, config, configSN;

	if(reply[0] != 0 || msgLen != 8 || valid == false)
	{
		// TODO: proper null value

		getAddressSpaceLink()->setNullStatusNBIRQ(  OpcUa_Bad);
		getAddressSpaceLink()->setNullResetRegistry(  OpcUa_Bad);
		getAddressSpaceLink()->setNullStatusWarning(  OpcUa_Bad);
		getAddressSpaceLink()->setNullStatusAlarm(  OpcUa_Bad);
		getAddressSpaceLink()->setNullStatusConfig(  OpcUa_Bad);
		getAddressSpaceLink()->setNullStatusConfigSN(  OpcUa_Bad);

		getAddressSpaceLink()->setConnectionStatus(1, OpcUa_Good);
		getAddressSpaceLink()->setConnectionJammed(1, OpcUa_Good);

		return OpcUa_Bad;
	}


	else
	{
			statebytevalue = reply[0];
			resetreg = (unsigned char)reply[1] ;
			warnstat = ((unsigned char)reply[2]) ;
			alarstat = ((unsigned char)reply[3]) ;
			config = ((unsigned char)reply[4]) ;
			configSN = ((unsigned char)reply[5]) ;
			nb_IRQ   = ((unsigned char)reply[6]<< 8) + (unsigned char)reply[7];

			getAddressSpaceLink()->setStatusNBIRQ(  nb_IRQ, OpcUa_Good);
			getAddressSpaceLink()->setResetRegistry(  resetreg, OpcUa_Good);
			getAddressSpaceLink()->setStatusWarning(  warnstat, OpcUa_Good);
			getAddressSpaceLink()->setStatusAlarm(  alarstat, OpcUa_Good);
			getAddressSpaceLink()->setStatusConfig( config, OpcUa_Good);
			getAddressSpaceLink()->setStatusConfigSN(configSN,  OpcUa_Good);

			getAddressSpaceLink()->setConnectionStatus(statebytevalue, OpcUa_Good);
			getAddressSpaceLink()->setConnectionJammed(reply[0], OpcUa_Good);
			return OpcUa_Good;
	}
}

UaStatus DCrate::handleReadInfo ( const unsigned char reply[], unsigned int msgLen, bool valid)
{
	OpcUa_Int16 infoType;
	OpcUa_Float infoSoftVersionMajor;
	OpcUa_Float infoSoftVersionMinor;
	OpcUa_Int32 infoValue;
	OpcUa_Int16 statebytevalue = reply[0];
	OpcUa_Int32 newSerial;

	if(statebytevalue != 0 || msgLen != 6 || valid == false)
	{
		// TODO: proper null value
		getAddressSpaceLink()->setNullHvMicroSerialNumber(  OpcUa_Bad);
		getAddressSpaceLink()->setNullHvMicroHVOptoManufactoryDate(  OpcUa_Bad);
		getAddressSpaceLink()->setNullHvMicroHVOptoTestDate(  OpcUa_Bad);
		getAddressSpaceLink()->setNullSoftWareVersionMajor(OpcUa_Bad);
		getAddressSpaceLink()->setNullSoftWareVersionMinor(OpcUa_Bad);

		getAddressSpaceLink()->setConnectionStatus(1, OpcUa_Good);
		getAddressSpaceLink()->setConnectionJammed(1, OpcUa_Good);

		return OpcUa_Bad;
	}

	else
	{
		infoType = reply[1] ;

			switch(infoType)
			{
				case 7:
					infoValue =  (reply[2]<<24)+ (reply[3]<<16)+ (reply[4]<<8)+ (reply[5]);
					printf("infoValue is %d \n", infoValue);
					printf("infoValue is %x \n", infoValue);
					newSerial = convertSerials(infoValue);
					printf("newSerial is %d \n", newSerial);
					getAddressSpaceLink()->setHvMicroSerialNumber(newSerial, OpcUa_Good);

				break;
				case 8:
					infoValue =  (reply[2]<<24)+ (reply[3]<<16)+ (reply[4]<<8)+ (reply[5]);
					getAddressSpaceLink()->setHvMicroHVOptoManufactoryDate(infoValue, OpcUa_Good);
				break;
				case 9:
					infoValue =  (reply[2]<<24)+ (reply[3]<<16)+ (reply[4]<<8)+ (reply[5]);
					getAddressSpaceLink()->setHvMicroHVOptoTestDate(infoValue, OpcUa_Good);
				break;
				case 10:
					infoSoftVersionMajor =  ((reply[2])+ (reply[3]));
					infoSoftVersionMinor = (reply[4])+ (reply[5]);
					getAddressSpaceLink()->setSoftWareVersionMajor(infoSoftVersionMajor, OpcUa_Good);
					getAddressSpaceLink()->setSoftWareVersionMinor(infoSoftVersionMinor, OpcUa_Good);
				break;
			}

			getAddressSpaceLink()->setConnectionJammed(reply[0], OpcUa_Good);
			getAddressSpaceLink()->setConnectionStatus(statebytevalue, OpcUa_Good);

		return OpcUa_Good;
	}


}


OpcUa_Int32 DCrate::convertSerials (OpcUa_Int32 serial)
	{
		OpcUa_Int32 newSerial;
		OpcUa_Int32 buff1;
		OpcUa_Int32 buff2;
		OpcUa_Int32 buff3;
		OpcUa_Int32 buff4;
		OpcUa_Int32 newhvInternalOptoSerial1;
		OpcUa_Int32 newhvInternalOptoSerial2;

		   buff1 = serial & 0x000000FF;
		   buff2 = (serial & 0x0000FF00) >> 8;
		   buff3 = (serial & 0x00FF0000) >> 16;
		   buff4 = (serial & 0xFF000000) >> 24;

		   newhvInternalOptoSerial1 = (((buff4<<8) + buff3)*1000);
		   newhvInternalOptoSerial2 = (((buff2<<8) + buff1));
		   newSerial = newhvInternalOptoSerial1 + newhvInternalOptoSerial2;


			return newSerial;
	}

}



