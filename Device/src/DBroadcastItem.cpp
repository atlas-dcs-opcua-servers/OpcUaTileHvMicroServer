
/* This is device body stub */


#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DBroadcastItem.h>
#include <ASBroadcastItem.h>
#include <HVcan.h>
#include <DDrawer.h>

#include <iostream>




namespace Device
{



/* sample ctr */
DBroadcastItem::DBroadcastItem (const Configuration::BroadcastItem & config

                                , DDrawer * parent

                               ):

    m_parent(parent),

    m_index(config.index()),
    m_addressSpaceLink(0)

/* fill up constructor initialization list here */
{
    /* fill up constructor body here */
}

/* sample dtr */
DBroadcastItem::~DBroadcastItem ()
{
    /* remove children */

}

/* delegators for cachevariables and externalvariables */

/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */



UaStatus DBroadcastItem::BroadcastItem ()
{
	UaDateTime sourceTime;
	UaStatus ret;

			msg.c_dlc = 0;
			m_ind = m_index;

			if (m_ind == HV_BROADCAST_STATUS) // m_ind is hexadecimal
		    {
		      msg.c_data[0] = 0x04;
		      msg.c_data[1] = 0x00;
		      msg.c_dlc = 2;
		      printf("HV_BROADCAST_STATUS\n");
		    }


		    msg.c_id = (0x100 + (getParent()->m_drawerID));
		    printf("msg.c_id = %x \n", msg.c_id);
		    printf("msg.c_dlc = %x \n", msg.c_dlc);
		    for (int y = 0; y< msg.c_dlc; y++)
		    {
		      	 printf("msg.c_data[y] = %x \n", msg.c_data[y]);
		    }

		    getParent()->messageSend(msg);
		    printf("MESSAGE SENT  \n");
		    getParent()->addDelay();
			printf("MESSAGE RECEIVED  \n");

			msgReply = getParent()->msgReply;
		    //statusValue = msgReply.c_data[0];
		   	//getAddressSpaceLink()->setStatusValue(statusValue, OpcUa_Good, UaDateTime::now());

	    sourceTime = UaDateTime::now();
	    return ret;
}



UaStatus DBroadcastItem::readItemValue (
    OpcUa_Double &value,
    UaDateTime &sourceTime
)
{
	sourceTime = UaDateTime::now();
    return OpcUa_BadNotImplemented;
}



/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */
UaStatus DBroadcastItem::writeItemValue (
    OpcUa_Double &value
)
{
    return OpcUa_BadNotImplemented;
}


/* address space linker */

void DBroadcastItem::linkAddressSpace( AddressSpace::ASBroadcastItem *addressSpaceLink)
{
    if (m_addressSpaceLink != 0)
    {
        /* signalize nice error from here. */
        abort();
    }
    else
        m_addressSpaceLink = addressSpaceLink;
}

/* add/remove */


/* to safely quit */
void DBroadcastItem::unlinkAllChildren ()
{
    PRINT_LOCATION();
    m_addressSpaceLink = 0;
    /* Fill up: call unlinkAllChildren on all children */


}

/* find methods for children */
void DBroadcastItem::passBROADCAST(const CanMessage rmsg)
{
	msgReply = getParent()-> passMSG(rmsg);
}


}


