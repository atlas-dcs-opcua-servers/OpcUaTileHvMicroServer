
/* This is device header stub */


#ifndef __DHVOddEvenSwitch__H__
#define __DHVOddEvenSwitch__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>

#include <CanBusAccess.h>
#include <CANopen_frames.h>
#include <CCanAccess.h>

#include <boost/foreach.hpp>



/* forward decl for AddressSpace */
namespace AddressSpace {
class
    ASHVOddEvenSwitch
    ;
}
/* forward decl for Parent */

#include <DDrawer.h>


namespace Device
{

/* forward declarations (comes from design.class.hasObjects) */

class DCanBus;
class DDrawer;
class
    DHVOddEvenSwitch
{

public:
    /* sample constructor */
    explicit DHVOddEvenSwitch ( const Configuration::HVOddEvenSwitch & config

                                , DDrawer * parent

                              ) ;
    /* sample dtr */
    ~DHVOddEvenSwitch ();

    DDrawer * getParent () const {
        return m_parent;
    }


    /* add/remove for handling device structure */

    UaStatus writeValValue ( const OpcUa_Int32 & v);


    /* to safely quit */
    void unlinkAllChildren ();

    /* delegators for
    cachevariables and sourcevariables */

    /* ASYNCHRONOUS !! */
    UaStatus readItemValue (
        OpcUa_Float &value,
        UaDateTime &sourceTime
    );

    /* ASYNCHRONOUS !! */
    UaStatus writeItemValue (
        OpcUa_Float &value
    );


    void linkAddressSpace (AddressSpace::
                           ASHVOddEvenSwitch
                           * as);
    AddressSpace::ASHVOddEvenSwitch * getAddressSpaceLink () const {
        return m_addressSpaceLink;
    }


    /* find methods for children */


    /* getters for values which are keys */


    /* mutex operations */


    /* variable-wise locks */


    void passHVODDEVENSWITCH(const CanMessage rmsg);
       CanMessage msg;
       	CanMessage msgReply;

    	   unsigned int m_index;
    	    unsigned int m_subindex;
    	    OpcUa_UInt32 m_ind;
    	      OpcUa_UInt32 m_subind;
    	      OpcUa_Int16 m_switchValue;
    	     OpcUa_Int16 Switch ;
    	    OpcUa_Int16	val;

private:


    DDrawer * m_parent;

    OpcUa_Int16 statebyteWrite ;
        OpcUa_Int16 State_Byte  ;
        OpcUa_Int16 repMessageIn ;
        OpcUa_Int16 statusValue;


    AddressSpace::
    ASHVOddEvenSwitch
    *m_addressSpaceLink;

    /* if any of our cachevariables has isKey=true then we shall keep its copy here for find functions  (it is const, either way) */


    /* object-wise lock */


    /* variable-wise locks */


};




}

#endif // include guard
