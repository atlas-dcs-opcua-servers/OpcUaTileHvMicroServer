
/* This is device header stub */


#ifndef __DConnectionStatus__H__
#define __DConnectionStatus__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>



/* forward decl for AddressSpace */
namespace AddressSpace {
class
    ASConnectionStatus
    ;
}
/* forward decl for Parent */

#include <DDrawer.h>


namespace Device
{

/* forward declarations (comes from design.class.hasObjects) */


class
    DConnectionStatus
{

public:
    /* sample constructor */
    explicit DConnectionStatus ( const Configuration::ConnectionStatus & config

                                 , DDrawer * parent

                               ) ;
    /* sample dtr */
    ~DConnectionStatus ();

    DDrawer * getParent () const {
        return m_parent;
    }


    /* add/remove for handling device structure */


    /* to safely quit */
    void unlinkAllChildren ();

    /* delegators for
    cachevariables and sourcevariables */


    void linkAddressSpace (AddressSpace::
                           ASConnectionStatus
                           * as);
    AddressSpace::ASConnectionStatus * getAddressSpaceLink () const {
        return m_addressSpaceLink;
    }


    void passCONNECTIONSTATUS(const CanMessage rmsg);
        UaStatus readGlobalStatus();


    /* find methods for children */


    /* getters for values which are keys */


    /* mutex operations */


    /* variable-wise locks */


    CanMessage msgReply;
    	CanMessage msg;

    	unsigned int m_subindex;
    	unsigned int m_index;


    	DDrawer * m_parent;


    	    OpcUa_Int16 statebyteWrite ;
    	    OpcUa_Int16 State_Byte  ;
    	    OpcUa_Int16 SoftAddress ;
    	    OpcUa_Int16 repMessageIn ;
    	    OpcUa_UInt32 m_ind;
    	    OpcUa_UInt32 m_subind;






private:



    AddressSpace::
    ASConnectionStatus
    *m_addressSpaceLink;

    /* if any of our cachevariables has isKey=true then we shall keep its copy here for find functions  (it is const, either way) */


    /* object-wise lock */


    /* variable-wise locks */


};




}

#endif // include guard
