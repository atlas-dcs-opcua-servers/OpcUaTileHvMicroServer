/*
 * DUpdateInviker.h
 *
 *  Created on: Sep 1, 2014
 *      Author: pagoncal
 */




#ifndef DUPDATEINVOKER_H_
#define DUPDATEINVOKER_H_
#include <uathread.h>

#include <vector>
#include <boost/thread/mutex.hpp>
#include <Configuration.hxx>


#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>
#include <boost/foreach.hpp>

#include <DDrawer.h>
#include <DCanBus.h>
#include <DReadItems.h>

namespace Device
{

class DReadItems;
class DDrawer;
class DCanBus;

class DUpdateInvoker : public UaThread
{
public:
	DUpdateInvoker ();

	void run ();
	void exit () { m_exitFlag=1; }


	std::vector < DDrawer * > m_Drawers;
	std::vector < DCanBus * > m_CanBus;



    const std::vector<DCanBus * > & canbus() const {
          return m_CanBus;
      }






private:
	volatile bool m_exitFlag;

};
}
#endif
