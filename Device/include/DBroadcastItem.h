
/* This is device header stub */


#ifndef __DBroadcastItem__H__
#define __DBroadcastItem__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>




#include <DDrawer.h>

#include <CanBusAccess.h>
#include <CANopen_frames.h>
#include <CCanAccess.h>

#include <boost/foreach.hpp>

/* forward decl for AddressSpace */
namespace AddressSpace {
class
    ASBroadcastItem
    ;
}
/* forward decl for Parent */




namespace Device
{

/* forward declarations (comes from design.class.hasObjects) */

class DCanBus;
class DDrawer;

class
    DBroadcastItem
{

public:
    /* sample constructor */
    explicit DBroadcastItem ( const Configuration::BroadcastItem & config

                              , DDrawer * parent

                            ) ;
    /* sample dtr */
    ~DBroadcastItem ();

    DDrawer * getParent () const {
        return m_parent;
    }


    /* add/remove for handling device structure */


    /* to safely quit */
    void unlinkAllChildren ();

    /* delegators for
    cachevariables and sourcevariables */

    /* ASYNCHRONOUS !! */
    UaStatus readItemValue (
        OpcUa_Double &value,
        UaDateTime &sourceTime
    );

    /* ASYNCHRONOUS !! */
    UaStatus writeItemValue (
        OpcUa_Double &value
    );

    UaStatus BroadcastItem ();
    void linkAddressSpace (AddressSpace::
                           ASBroadcastItem
                           * as);
    AddressSpace::ASBroadcastItem * getAddressSpaceLink () const {
        return m_addressSpaceLink;
    }


    /* find methods for children */


    /* getters for values which are keys */


    /* mutex operations */


    /* variable-wise locks */

	CanMessage msg;
	CanMessage msgReply;

	void passBROADCAST(const CanMessage rmsg);
	   unsigned int m_index;
	    unsigned int m_subindex;
	    OpcUa_UInt32 m_ind;
	      OpcUa_UInt32 m_subind;



private:


	DDrawer * m_parent;

    OpcUa_Int16 statebyteWrite ;
    OpcUa_Int16 State_Byte  ;
    OpcUa_Int16 repMessageIn ;
    OpcUa_Int16 statusValue;

    AddressSpace::
    ASBroadcastItem
    *m_addressSpaceLink;

    /* if any of our cachevariables has isKey=true then we shall keep its copy here for find functions  (it is const, either way) */


    /* object-wise lock */


    /* variable-wise locks */


};




}

#endif // include guard
