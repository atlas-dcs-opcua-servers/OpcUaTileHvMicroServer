
/* This is device header stub */


#ifndef __DCommandItems__H__
#define __DCommandItems__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>





#include <DChannelNo.h>
#include <ASChannelNo.h>
#include <DDrawer.h>

#include <CanBusAccess.h>
#include <CANopen_frames.h>
#include <CCanAccess.h>

#include <boost/foreach.hpp>



/* forward decl for AddressSpace */
namespace AddressSpace {
class
    ASCommandItems
    ;
}
/* forward decl for Parent */




namespace Device
{

/* forward declarations (comes from design.class.hasObjects) */

class DCanBus;
class DDrawer;
class DChannelNo;

class DCommandItems
{

public:
    /* sample constructor */
    explicit DCommandItems ( const Configuration::CommandItems & config

                             , DDrawer * parent

                           ) ;
    /* sample dtr */
    ~DCommandItems ();

    DDrawer * getParent () const {
        return m_parent;
    }


    /* add/remove for handling device structure */


    /* to safely quit */
    void unlinkAllChildren ();

    /* delegators for
    cachevariables and sourcevariables */

    /* ASYNCHRONOUS !! */
    UaStatus readItemValue (
        OpcUa_Double &value,
        UaDateTime &sourceTime
    );

    UaStatus readGlobalStatus();

    /* ASYNCHRONOUS !! */
    UaStatus writeItemValue (
        OpcUa_Double &value
    );


    void linkAddressSpace (AddressSpace::
                           ASCommandItems
                           * as);

    AddressSpace::ASCommandItems * getAddressSpaceLink () const {
        return m_addressSpaceLink;
    }

    UaStatus sendMessage(const CanMessage & msg);


    /* find methods for children */


    /* getters for values which are keys */


    /* mutex operations */


    /* variable-wise locks */

    void passCOMMANDITEMS(const CanMessage rmsg);

	CanMessage m_bufferReply;
	CanMessage msg;
	CanMessage msgReply;

    OpcUa_Int16 statebyteWrite ;
    OpcUa_Int16 State_Byte  ;
    OpcUa_Int16 SoftAddress ;
    OpcUa_Int16 NewSoftAddress ;
    OpcUa_Int32 HardAddress  ;
    OpcUa_Int16 repMessageIn ;
    OpcUa_UInt32 m_ind;
    OpcUa_UInt32 m_subind;
    OpcUa_Int16 statusValue;





    unsigned int	m_card;
	unsigned int	m_address;
	unsigned int	m_polyn;
	unsigned int	m_deviceDacAdc;
    unsigned int m_index;
    unsigned int m_subindex;


private:


    DDrawer * m_parent;
    OpcUa_UInt32 m_bufferReplySize;


    AddressSpace::
    ASCommandItems
    *m_addressSpaceLink;

    /* if any of our cachevariables has isKey=true then we shall keep its copy here for find functions  (it is const, either way) */


    /* object-wise lock */


    /* variable-wise locks */


};




}

#endif // include guard
