/*
 * DChannel.h
 *
 *  Created on: Mar, 2015
 *      Author: pagoncal
 */



#ifndef __DChannel__H__
#define __DChannel__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>

#include <Base_DChannel.h>


namespace Device
{




class
    DChannel
    : public Base_DChannel
{

public:
    /* sample constructor */
    explicit DChannel (
        const Configuration::Channel& config,
        Parent_DChannel* parent
                      ) ;
    /* sample dtr */
    ~DChannel ();




    /* delegators for
    cachevariables and sourcevariables */

    /* Note: never directly call this function. */


    UaStatus writeWriteOrder ( const OpcUa_Float & v);

    /* Note: never directly call this function. */


    UaStatus writeWriteMaxstep ( const OpcUa_Float & v);

    /* Note: never directly call this function. */


    UaStatus writeSaveOrder ( const OpcUa_Boolean & v);

    /* Note: never directly call this function. */


    UaStatus writeRestoreOrder ( const OpcUa_Boolean & v);

    /* Note: never directly call this function. */


    UaStatus writeCalibrationTrigger ( const OpcUa_Boolean & v);


    /* delegators for methods */

private:
    /* Delete copy constructor and assignment operator */
    DChannel( const DChannel & ) = delete;
    DChannel& operator=(const DChannel &other) = delete;

    // ----------------------------------------------------------------------- *
    // -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
    // -     Don't change this comment, otherwise merge tool may be troubled.  *
    // ----------------------------------------------------------------------- *

public:

    	UaStatus handleReadChannelReply ( const unsigned char reply[], unsigned int msgLen, bool valid);
        UaStatus handleSaveOrder ( const unsigned char reply[], unsigned int msgLen, bool valid);
        UaStatus handleRestoreOrder ( const unsigned char reply[], unsigned int msgLen, bool valid);
        UaStatus handleWriteChannelReply ( const unsigned char reply[], unsigned int msgLen, bool valid);
        //UaStatus handleReadCalibrationsReply ( const unsigned char reply[], unsigned int msgLen, bool valid);
        UaStatus handleReadCalibrationsDACX0Reply ( const unsigned char reply[], unsigned int msgLen, bool valid);
        UaStatus handleReadCalibrationsDACX1Reply ( const unsigned char reply[], unsigned int msgLen, bool valid);
        UaStatus handleReadCalibrationsDACX2Reply ( const unsigned char reply[], unsigned int msgLen, bool valid);

        UaStatus handleReadCalibrationsADCX0Reply ( const unsigned char reply[], unsigned int msgLen, bool valid);
        UaStatus handleReadCalibrationsADCX1Reply ( const unsigned char reply[], unsigned int msgLen, bool valid);
        UaStatus handleReadCalibrationsADCX2Reply ( const unsigned char reply[], unsigned int msgLen, bool valid);
        UaStatus handleWriteItems ( const unsigned char reply[], unsigned int msgLen, bool valid);

        OpcUa_Float returnReference(OpcUa_Int32 channel);




};





}

#endif // __DChannel__H__
