
/* This is device header stub */


#ifndef __DRestoreOrderItem__H__
#define __DRestoreOrderItem__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>



#include <DDrawer.h>

#include <CanBusAccess.h>
#include <CANopen_frames.h>
#include <CCanAccess.h>

#include <boost/foreach.hpp>

/* forward decl for AddressSpace */
namespace AddressSpace {
class
    ASRestoreOrderItem
    ;
}
/* forward decl for Parent */




namespace Device
{

/* forward declarations (comes from design.class.hasObjects) */

class DCanBus;
class DDrawer;

class
    DRestoreOrderItem
{

public:
    /* sample constructor */
    explicit DRestoreOrderItem ( const Configuration::RestoreOrderItem & config

                                 , DChannelNo * parent

                               ) ;
    /* sample dtr */
    ~DRestoreOrderItem ();

    DChannelNo * getParent () const {
        return m_parent;
    }


    /* add/remove for handling device structure */


    /* to safely quit */
    void unlinkAllChildren ();

    /* delegators for
    cachevariables and sourcevariables */

    /* ASYNCHRONOUS !! */
    UaStatus readItemValue (
        OpcUa_Double &value,
        UaDateTime &sourceTime
    );

    /* ASYNCHRONOUS !! */
    UaStatus writeItemValue (
        OpcUa_Double &value
    );


    void linkAddressSpace (AddressSpace::
                           ASRestoreOrderItem
                           * as);
    AddressSpace::ASRestoreOrderItem * getAddressSpaceLink () const {
        return m_addressSpaceLink;
    }

    void passRESTOREORDER(const CanMessage rmsg);
    /* find methods for children */


    /* getters for values which are keys */


    /* mutex operations */


    /* variable-wise locks */


	CanMessage m_bufferReply;
	CanMessage msg;
	CanMessage msgReply;


    OpcUa_Int16 statebyteWrite ;
    OpcUa_Int16 State_Byte  ;
    OpcUa_Int16 SoftAddress ;
    OpcUa_Int16 NewSoftAddress ;
    OpcUa_Int32 HardAddress  ;
    OpcUa_Int16 repMessageIn ;
    OpcUa_UInt32 m_ind;
    OpcUa_UInt32 m_subind;
    OpcUa_Int16 statusValue;


	 unsigned int m_index;
	    unsigned int m_subindex;



private:


	DChannelNo * m_parent;


    OpcUa_UInt32 m_bufferReplySize;


    AddressSpace::
    ASRestoreOrderItem
    *m_addressSpaceLink;

    /* if any of our cachevariables has isKey=true then we shall keep its copy here for find functions  (it is const, either way) */


    /* object-wise lock */


    /* variable-wise locks */


};




}

#endif // include guard
