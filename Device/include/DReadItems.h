
/* This is device header stub */


#ifndef __DReadItems__H__
#define __DReadItems__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>



#include <DDrawer.h>

#include <CanBusAccess.h>
#include <CANopen_frames.h>
#include <CCanAccess.h>

#include <boost/foreach.hpp>


/* forward decl for AddressSpace */
namespace AddressSpace {
class
    ASReadItems
    ;
}
/* forward decl for Parent */

#include <DChannelNo.h>


namespace Device
{

/* forward declarations (comes from design.class.hasObjects) */

class DCanBus;
class DDrawer;
class DChannelNo;
class
    DReadItems
{

public:
    /* sample constructor */
    explicit DReadItems ( const Configuration::ReadItems & config

                          , DChannelNo * parent

                        ) ;
    /* sample dtr */
    ~DReadItems ();

    DChannelNo * getParent () const {
        return m_parent;
    }


    /* add/remove for handling device structure */


    /* to safely quit */
    void unlinkAllChildren ();

    /* delegators for
    cachevariables and sourcevariables */

    /* ASYNCHRONOUS !! */
    UaStatus readItemValue (
        OpcUa_Double &value,
        UaDateTime &sourceTime
    );

    /* ASYNCHRONOUS !! */
    UaStatus writeItemValue (
        OpcUa_Double &value
    );



    void linkAddressSpace (AddressSpace::
                           ASReadItems
                           * as);
    AddressSpace::ASReadItems * getAddressSpaceLink () const {
        return m_addressSpaceLink;
    }


    void passREAD(const CanMessage rmsg);
    UaStatus readManyItems ();
    UaStatus readAppliedValue (OpcUa_Float &value, UaDateTime &sourceTime);


    /* find methods for children */


    /* getters for values which are keys */


    /* mutex operations */


    /* variable-wise locks */


	CanMessage m_bufferReply;

	CanMessage msgReply;
	CanMessage msg;

	unsigned int m_subindex;
	unsigned int m_index;


	DChannelNo * m_parent;


	    OpcUa_Int16 statebyteWrite ;
	    OpcUa_Int16 State_Byte  ;
	    OpcUa_Int16 SoftAddress ;
	    OpcUa_Int16 NewSoftAddress ;
	    OpcUa_Int32 HardAddress  ;
	    OpcUa_Int16 repMessageIn ;
	    OpcUa_UInt32 m_ind;
	    OpcUa_UInt32 m_subind;




private:





    OpcUa_UInt32 m_bufferReplySize;


    OpcUa_Float appliedValue;
    OpcUa_Float orderValue;
    OpcUa_Float maxstepValue;
    OpcUa_Float stateValue;
    OpcUa_Float	statusValue;

    AddressSpace::
    ASReadItems
    *m_addressSpaceLink;

    /* if any of our cachevariables has isKey=true then we shall keep its copy here for find functions  (it is const, either way) */


    /* object-wise lock */


    /* variable-wise locks */


};




}

#endif // include guard
