
/* This is device header stub */


#ifndef __DWriteMaxstep__H__
#define __DWriteMaxstep__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>



/* forward decl for AddressSpace */
namespace AddressSpace {
class
    ASWriteMaxstep
    ;
}
/* forward decl for Parent */

#include <DChannelNo.h>


namespace Device
{

/* forward declarations (comes from design.class.hasObjects) */


class
    DWriteMaxstep
{

public:
    /* sample constructor */
    explicit DWriteMaxstep ( const Configuration::WriteMaxstep & config

                             , DChannelNo * parent

                           ) ;
    /* sample dtr */
    ~DWriteMaxstep ();

    DChannelNo * getParent () const {
        return m_parent;
    }


    /* add/remove for handling device structure */


    /* to safely quit */
    void unlinkAllChildren ();

    /* delegators for
    cachevariables and sourcevariables */

    /* ASYNCHRONOUS !! */
    UaStatus readItemValue (
    		OpcUa_Float &value,
        UaDateTime &sourceTime
    );

    /* ASYNCHRONOUS !! */
    UaStatus writeItemValue (
    		OpcUa_Float &value
    );


    void linkAddressSpace (AddressSpace::
                           ASWriteMaxstep
                           * as);
    AddressSpace::ASWriteMaxstep * getAddressSpaceLink () const {
        return m_addressSpaceLink;
    }


    /* find methods for children */


    /* getters for values which are keys */


    /* mutex operations */


    /* variable-wise locks */


private:


    DChannelNo * m_parent;


    AddressSpace::
    ASWriteMaxstep
    *m_addressSpaceLink;

    /* if any of our cachevariables has isKey=true then we shall keep its copy here for find functions  (it is const, either way) */


    /* object-wise lock */


    /* variable-wise locks */


};




}

#endif // include guard
