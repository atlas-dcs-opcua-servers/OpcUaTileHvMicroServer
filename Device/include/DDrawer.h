/*
 * DDrawer.h
 *
 *  Created on: Mar, 2015
 *      Author: pagoncal
 */



#ifndef __DDrawer__H__
#define __DDrawer__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>

#include <Base_DDrawer.h>
#include <CommunicationController.h>


/* forward decl for AddressSpace */
namespace AddressSpace {
class
    ASDrawer
    ;
}
/* forward decl for Parent */

#include <DCanBus.h>


namespace Device
{




class
    DDrawer
    : public Base_DDrawer
{

public:
    /* sample constructor */
    explicit DDrawer ( const Configuration::Drawer & config

                       , DCanBus * parent

                     ) ;
    /* sample dtr */
    ~DDrawer ();




    /* delegators for
    cachevariables and sourcevariables */

    /* Note: never directly call this function. */


    UaStatus writeVerificationOnOffValue ( const OpcUa_Boolean & v);

    /* Note: never directly call this function. */


    UaStatus writeResetDrawer ( const OpcUa_Boolean & v);

    /* Note: never directly call this function. */


    UaStatus writeOddEvenSwitch0 ( const OpcUa_Boolean & v);

    /* Note: never directly call this function. */


    UaStatus writeOddEvenSwitch1 ( const OpcUa_Boolean & v);

    /* Note: never directly call this function. */


    UaStatus writeOddEvenSwitch2 ( const OpcUa_Boolean & v);

    /* Note: never directly call this function. */


    UaStatus writeOddEvenSwitch3 ( const OpcUa_Boolean & v);

    /* Note: never directly call this function. */


    UaStatus writeOddEvenSwitch4 ( const OpcUa_Boolean & v);

    /* Note: never directly call this function. */


    UaStatus writeInfoTrigger ( const OpcUa_Boolean & v);

    /* Note: never directly call this function. */


    UaStatus writeAddressReadTrigger ( const OpcUa_Boolean & v);

    /* Note: never directly call this function. */


    UaStatus writeAddressWriteNewSoftAddressValueInput ( const OpcUa_Int32 & v);


    /* delegators for methods */

private:
    /* Delete copy constructor and assignment operator */
    DDrawer( const DDrawer & ) = delete;
    DDrawer& operator=(const DDrawer &other) = delete;

    // ----------------------------------------------------------------------- *
    // -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
    // -     Don't change this comment, otherwise merge tool may be troubled.  *
    // ----------------------------------------------------------------------- *

public:

    UaStatus handleReadGlobalStatus (const unsigned char reply[], unsigned int msgLen, bool valid);
        UaStatus handleReadInfo (const unsigned char reply[], unsigned int msgLen, bool valid);
        UaStatus handleControlMode ( const unsigned char reply[], unsigned int msgLen, bool valid);
        UaStatus handleResetReply ( const unsigned char reply[], unsigned int msgLen, bool valid);
        UaStatus handleOddEvenSwitchReply ( const unsigned char reply[], unsigned int msgLen, bool valid);
        UaStatus handleAddressWriteReply ( const unsigned char reply[], unsigned int msgLen, bool valid);
        UaStatus handleAddressReadReply ( const unsigned char reply[], unsigned int msgLen, bool valid);
        UaStatus handleWriteItems ( const unsigned char reply[], unsigned int msgLen, bool valid);
        OpcUa_Int32 convertSerials (OpcUa_Int32 serial);
        void handleMessage (const CanMessage & msg);
        void sendMessage ( const CanMessage & msg);


              	    	       	      void tick ();
              	    	       	      unsigned int id () { return drawerID(); }

              	    	       	     CommunicationController<DDrawer> * getCommunicationController() { return & m_communicationController; }

private:

	CommunicationController<DDrawer> m_communicationController;



};





}

#endif // include guard
