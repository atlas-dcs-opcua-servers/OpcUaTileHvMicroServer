
/* This is device header stub */


#ifndef __DAdvancedHeater__H__
#define __DAdvancedHeater__H__

#include <vector>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>



/* forward decl for AddressSpace */
namespace AddressSpace {
class
    ASAdvancedHeater
    ;
}
/* forward decl for Parent */

#include <DRoot.h>


namespace Device
{

/* forward declarations (comes from design.class.hasObjects) */


class
    DAdvancedHeater
{

public:
    /* sample constructor */
    explicit DAdvancedHeater ( const Configuration::AdvancedHeater & config

                               , DRoot * parent

                             ) ;
    /* sample dtr */
    ~DAdvancedHeater ();

    DRoot * getParent () const {
        return m_parent;
    }


    /* add/remove for handling device structure */


    /* to safely quit */
    void unlinkAllChildren ();

    /* delegators for
    cachevariables and externalvariables */

    /* Note: never directly call this function. */


    UaStatus writeDesiredTemperature ( const OpcUa_Float & v);


    void linkAddressSpace (AddressSpace::
                           ASAdvancedHeater
                           * as);

    /* find methods for children */


    /* getters for values which are keys */


    	void update();

private:


    DRoot * m_parent;


    AddressSpace::
    ASAdvancedHeater
    *m_addressSpaceLink;

    /* if any of our cachevariables has isKey=true then we shall keep its copy here for find functions  (it is const, either way) */

    OpcUa_Float m_currentTemperature;
    OpcUa_Float m_desiredTemperature;


};




}

#endif // include guard
