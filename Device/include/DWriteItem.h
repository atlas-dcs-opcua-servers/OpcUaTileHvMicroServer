
/* This is device header stub */


#ifndef __DWriteItem__H__
#define __DWriteItem__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>
#include <HVcan.h>


/* forward decl for AddressSpace */
namespace AddressSpace {
class
    ASWriteItem
    ;
}
/* forward decl for Parent */




namespace Device
{

/* forward declarations (comes from design.class.hasObjects) */


class
    DWriteItem
{

public:
    /* sample constructor */
    explicit DWriteItem ( const Configuration::WriteItem & config

                          , DChannelNo * parent

                        ) ;
    /* sample dtr */
    ~DWriteItem ();

    DChannelNo * getParent () const {
        return m_parent;
    }


    /* add/remove for handling device structure */


    /* to safely quit */
    void unlinkAllChildren ();

    /* delegators for
    cachevariables and sourcevariables */

    /* ASYNCHRONOUS !! */
    UaStatus readItemValue (
    		OpcUa_Float &value,
        UaDateTime &sourceTime
    );

    /* ASYNCHRONOUS !! */
    UaStatus writeItemValue (
    		OpcUa_Float &value
    );


    void linkAddressSpace (AddressSpace::
                           ASWriteItem
                           * as);

    AddressSpace::ASWriteItem * getAddressSpaceLink () const {
        return m_addressSpaceLink;
    }

    //UaStatus writeOrderValue (OpcUa_Float &value);
    //UaStatus writeMaxStepValue (OpcUa_Float &value);

    /* find methods for children */


    /* getters for values which are keys */


    /* mutex operations */


    /* variable-wise locks */

	CanMessage m_bufferReply;

	CanMessage msgReply;
	CanMessage msg;

	unsigned int m_index;
	DChannelNo * m_parent;

	    unsigned int m_subindex;
	    OpcUa_Int16 statebyteWrite ;
	    OpcUa_Int16 State_Byte  ;
	    OpcUa_Int16 SoftAddress ;
	    OpcUa_Int16 NewSoftAddress ;
	    OpcUa_Int32 HardAddress  ;
	    OpcUa_Int16 repMessageIn ;
	    OpcUa_UInt32 m_ind;
	    OpcUa_UInt32 m_subind;


	    OpcUa_Float orderValue;
	    OpcUa_Float maxstepValue;


	    void passWRITE(const CanMessage rmsg);

private:





    AddressSpace::
    ASWriteItem
    *m_addressSpaceLink;

    /* if any of our cachevariables has isKey=true then we shall keep its copy here for find functions  (it is const, either way) */


    /* object-wise lock */


    /* variable-wise locks */


};




}

#endif // include guard
