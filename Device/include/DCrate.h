/*
 * DCrate.h
 *
 *  Created on: Mar, 2015
 *      Author: pagoncal
 */


#ifndef __DCrate__H__
#define __DCrate__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>

#include <Base_DCrate.h>
#include <CommunicationController.h>




/* forward decl for AddressSpace */
namespace AddressSpace {
class
    ASCrate
    ;
}
/* forward decl for Parent */

#include <DCanBus.h>


namespace Device
{




class
    DCrate
    : public Base_DCrate
{

public:
    /* sample constructor */
    explicit DCrate ( const Configuration::Crate & config

                      , DCanBus * parent

                    ) ;
    /* sample dtr */
    ~DCrate ();




    /* delegators for
    cachevariables and sourcevariables */

    /* Note: never directly call this function. */


    UaStatus writeResetHVMicro ( const OpcUa_Boolean & v);

    /* Note: never directly call this function. */


    UaStatus writeSoftwareVeto ( const OpcUa_Boolean & v);

    /* Note: never directly call this function. */


    UaStatus writeSetClearTrips ( const OpcUa_Boolean & v);

    /* Note: never directly call this function. */


    UaStatus writeInfoTrigger ( const OpcUa_Boolean & v);


private:
    /* Delete copy constructor and assignment operator */
    DCrate( const DCrate & ) = delete;
    DCrate& operator=(const DCrate &other) = delete;

    // ----------------------------------------------------------------------- *
    // -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
    // -     Don't change this comment, otherwise merge tool may be troubled.  *
    // ----------------------------------------------------------------------- *

public:
    
    void tick ();
    void handleMessage (const CanMessage & msg);
            void sendMessage ( const CanMessage & msg);



            unsigned int id () { return crateID();}



            OpcUa_Int32 convertSerials (OpcUa_Int32 serial);
            UaStatus handleReadInfo ( const unsigned char reply[], unsigned int msgLen, bool valid);
            UaStatus handleReadGlobalStatus ( const unsigned char reply[], unsigned int msgLen, bool valid);
            UaStatus handleWriteItems ( const unsigned char reply[], unsigned int msgLen, bool valid);




             	    	       	      

              	    	       	      
	CommunicationController<DCrate> * getCommunicationController() { return & m_communicationController; }
private:


  
	CommunicationController<DCrate> m_communicationController;

};





}

#endif // include guard
