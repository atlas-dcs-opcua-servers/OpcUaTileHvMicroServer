
/* This is device header stub */


#ifndef __DChannelNo__H__
#define __DChannelNo__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>



/* forward decl for AddressSpace */
namespace AddressSpace {
class
    ASChannelNo
    ;
}
/* forward decl for Parent */

#include <DDrawer.h>


namespace Device
{

/* forward declarations (comes from design.class.hasObjects) */

class DReadItems;

class DSaveOrderItem;

class DRestoreOrderItem;

class DWriteItem;


class
    DChannelNo
{

public:
    /* sample constructor */
    explicit DChannelNo ( const Configuration::ChannelNo & config

                          , DDrawer * parent

                        ) ;
    /* sample dtr */
    ~DChannelNo ();

    DDrawer * getParent () const {
        return m_parent;
    }


    /* add/remove for handling device structure */

    void add ( DReadItems *);
    const std::vector<DReadItems * > & readitemss () const {
        return m_ReadItemss;
    }

    void add ( DSaveOrderItem *);
    const std::vector<DSaveOrderItem * > & saveorderitems () const {
        return m_SaveOrderItems;
    }

    void add ( DRestoreOrderItem *);
    const std::vector<DRestoreOrderItem * > & restoreorderitems () const {
        return m_RestoreOrderItems;
    }

    void add ( DWriteItem *);
    const std::vector<DWriteItem * > & writeitems () const {
        return m_WriteItems;
    }


    /* to safely quit */
    void unlinkAllChildren ();

    /* delegators for
    cachevariables and sourcevariables */

    /* Note: never directly call this function. */


   // UaStatus writeWriteOrderValue ( const OpcUa_Float & v);

    /* Note: never directly call this function. */


    //UaStatus writeMaxstepValue ( const OpcUa_Float & v);


    void linkAddressSpace (AddressSpace::
                           ASChannelNo
                           * as);
    AddressSpace::ASChannelNo * getAddressSpaceLink () const {
        return m_addressSpaceLink;
    }




    int getChannelNo(){return m_channelNumber;}
     void handleMessage (const CanMessage & msg);


    /* find methods for children */


    /* getters for values which are keys */


    /* mutex operations */


    /* variable-wise locks */
     OpcUa_UInt32 m_channelNumber;

     void readAllChannels();



     CanMessage msgReply;
     	CanMessage msg;

     	unsigned int m_index;
     	OpcUa_UInt32 m_ind;
     		    OpcUa_UInt32 m_subind;




private:


    DDrawer * m_parent;

    AddressSpace::
    ASChannelNo
    *m_addressSpaceLink;

    std::vector < DReadItems * > m_ReadItemss;

    std::vector < DSaveOrderItem * > m_SaveOrderItems;

    std::vector < DRestoreOrderItem * > m_RestoreOrderItems;

    std::vector < DWriteItem * > m_WriteItems;

    /* if any of our cachevariables has isKey=true then we shall keep its copy here for find functions  (it is const, either way) */


    /* object-wise lock */

    /* variable-wise locks */


};




}

#endif // include guard
