/*
 * PeriodicCommunicationObject.h
 *
 *  Created on: Mar 9, 2015
 *      Author: pnikiel
 */

#ifndef DEVICE_INCLUDE_PERIODICCOMMUNICATIONOBJECT_H_
#define DEVICE_INCLUDE_PERIODICCOMMUNICATIONOBJECT_H_

#include <CommunicationObject.h>
#include <Utils.h>

namespace Device
{

class PeriodicCommunicationObject
{
public:
	PeriodicCommunicationObject( CommunicationObject object, float period ):
		m_object(object),
		m_period(period)
	{
		m_lastRequest.tv_sec = 0;
		m_lastRequest.tv_usec = 0;

	}

	void onFire ()
	{
		gettimeofday( &m_lastRequest, 0 );
	}

	// TODO: put into regualar body
	bool periodPassed ()
	{
		timeval tnow;
		gettimeofday( &tnow, 0 );
		float timePassed = subtractTimeval( m_lastRequest, tnow );
		return timePassed >= m_period;
	}

	CommunicationObject getCommunicationObject () { return m_object; }

private:
	CommunicationObject m_object;
	float m_period;
	timeval m_lastRequest;


};

}

#endif /* DEVICE_INCLUDE_PERIODICCOMMUNICATIONOBJECT_H_ */
