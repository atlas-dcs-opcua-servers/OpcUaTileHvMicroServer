
/* This is device header stub */


#ifndef __DIDReadItem__H__
#define __DIDReadItem__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>


#include <DDrawer.h>

#include <CanBusAccess.h>
#include <CANopen_frames.h>
#include <CCanAccess.h>

#include <boost/foreach.hpp>


/* forward decl for AddressSpace */
namespace AddressSpace {
class
    ASIDReadItem
    ;
}
/* forward decl for Parent */




namespace Device
{

/* forward declarations (comes from design.class.hasObjects) */

class DCanBus;
class DDrawer;

class
    DIDReadItem
{

public:
    /* sample constructor */
    explicit DIDReadItem ( const Configuration::IDReadItem & config

                           , DDrawer * parent

                         ) ;
    /* sample dtr */
    ~DIDReadItem ();

    DDrawer * getParent () const {
        return m_parent;
    }


    /* add/remove for handling device structure */


    /* to safely quit */
    void unlinkAllChildren ();

    /* delegators for
    cachevariables and sourcevariables */

    /* ASYNCHRONOUS !! */
    UaStatus readItemValue (
        OpcUa_Double &value,
        UaDateTime &sourceTime
    );

    /* ASYNCHRONOUS !! */
    UaStatus writeItemValue (
        OpcUa_Double &value
    );

    UaStatus readItemID();

    void linkAddressSpace (AddressSpace::
                           ASIDReadItem
                           * as);
    AddressSpace::ASIDReadItem * getAddressSpaceLink () const {
        return m_addressSpaceLink;
    }

    void passID(const CanMessage rmsg);


    /* find methods for children */


    /* getters for values which are keys */


    /* mutex operations */


    /* variable-wise locks */

	CanMessage m_bufferReply;
	CanMessage msg;
	CanMessage msgReply;

	   unsigned int m_index;
	    unsigned int m_subindex;
	    OpcUa_Int16 statebyteWrite ;
	    OpcUa_Int16 State_Byte  ;
	    OpcUa_Int16 SoftAddress ;
	    OpcUa_Int16 NewSoftAddress ;
	    OpcUa_Int32 HardAddress  ;
	    OpcUa_Int16 repMessageIn ;
	    OpcUa_UInt32 m_ind;
	    OpcUa_UInt32 m_subind;

private:


	DDrawer * m_parent;



    OpcUa_UInt32 m_bufferReplySize;


    AddressSpace::
    ASIDReadItem
    *m_addressSpaceLink;

    /* if any of our cachevariables has isKey=true then we shall keep its copy here for find functions  (it is const, either way) */


    /* object-wise lock */


    /* variable-wise locks */


};




}

#endif // include guard
