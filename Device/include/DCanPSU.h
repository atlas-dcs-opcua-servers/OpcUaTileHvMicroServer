/*
 * DCanPSU.h
 *
 *  Created on: Mar, 2015
 *      Author: pagoncal
 */

#ifndef __DCanPSU__H__
#define __DCanPSU__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>

#include <Base_DCanPSU.h>
/* forward decl for AddressSpace */
namespace AddressSpace {

class
    ASCanPSU
    ;
}
/* forward decl for Parent */

#include <DCrate.h>


namespace Device
{




class
    DCanPSU
    : public Base_DCanPSU
{

public:
    /* sample constructor */
    explicit DCanPSU ( const Configuration::CanPSU & config

                       , DCrate * parent

                     ) ;
    /* sample dtr */
    ~DCanPSU ();




    /* delegators for
    cachevariables and sourcevariables */

    /* Note: never directly call this function. */


    UaStatus writeWriteConsumptionOrder ( const OpcUa_UInt32 & v);

    /* Note: never directly call this function. */


    UaStatus writeWriteConsumptionMaxstep ( const OpcUa_UInt32 & v);

    /* Note: never directly call this function. */


    UaStatus writeWriteReferenceOrder ( const OpcUa_UInt32 & v);

    /* Note: never directly call this function. */


    UaStatus writeWriteReferenceMaxstep ( const OpcUa_UInt32 & v);

    /* Note: never directly call this function. */


    UaStatus writeWriteTemperatureOrder ( const OpcUa_UInt32 & v);

    /* Note: never directly call this function. */


    UaStatus writeWriteTemperatureMaxstep ( const OpcUa_UInt32 & v);

    /* Note: never directly call this function. */


    UaStatus writeWriteVoltageOrder ( const OpcUa_UInt32 & v);

    /* Note: never directly call this function. */


    UaStatus writeWriteVoltageMaxstep ( const OpcUa_UInt32 & v);

    /* Note: never directly call this function. */


    UaStatus writeSwitchChanOnOff ( const OpcUa_Boolean & v);


private:
    /* Delete copy constructor and assignment operator */
    DCanPSU( const DCanPSU & ) = delete;
    DCanPSU& operator=(const DCanPSU &other) = delete;

    // ----------------------------------------------------------------------- *
    // -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
    // -     Don't change this comment, otherwise merge tool may be troubled.  *
    // ----------------------------------------------------------------------- *

public:
    OpcUa_Float returnReference(OpcUa_Int32 channel);

    UaStatus handleWriteGeneric( const unsigned char reply[], unsigned int msgLen, bool valid);
    UaStatus handleChannelStatus ( const unsigned char reply[], unsigned int msgLen, bool valid);
    UaStatus handleReadChannelReply ( const unsigned char reply[], unsigned int msgLen, bool valid);
    UaStatus handleReadChannelVoltageReply( const unsigned char reply[], unsigned int msgLen, bool valid);
    UaStatus handleReadChannelConsumptionReply( const unsigned char reply[], unsigned int msgLen, bool valid);
    UaStatus handleReadChannelReferenceReply( const unsigned char reply[], unsigned int msgLen, bool valid);
    UaStatus handleReadChannelTemperatureReply( const unsigned char reply[], unsigned int msgLen, bool valid);

private:


};





}

#endif // include guard
