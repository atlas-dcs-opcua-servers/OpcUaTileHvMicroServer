
/*
 * DCanBus.h
 *
 *  Created on: Mar, 2015
 *      Author: pagoncal
 */




#ifndef __DCanBus__H__
#define __DCanBus__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>

#include <Base_DCanBus.h>
#include <Configurator.h>
#include <HVcan.h>

#include <CanBusAccess.h>
#include <CCanAccess.h>



namespace Device
{




class
    DCanBus
    : public Base_DCanBus
{

public:
    /* sample constructor */
    explicit DCanBus (
        const Configuration::CanBus& config,
        Parent_DCanBus* parent
                     ) ;
    /* sample dtr */
    ~DCanBus ();




    /* delegators for
    cachevariables and sourcevariables */


private:
    /* Delete copy constructor and assignment operator */
    DCanBus( const DCanBus& other );
    DCanBus& operator=(const DCanBus& other);

    // ----------------------------------------------------------------------- *
    // -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
    // -     Don't change this comment, otherwise merge tool may be troubled.  *
    // ----------------------------------------------------------------------- *

public:
    void handleMessage (const CanMessage & msg);
    unsigned int getNodeId (const CanMessage &msg) { return msg.c_id & NODEID_MASK; }
     unsigned int getObjectId (const CanMessage &msg) { return msg.c_id & OBJECT_MASK; }

     void openCanBus ();
     void sendMessage ( const CanMessage & msg);
     void sendRTR ( unsigned int cobId );
     void readcanBus();


private:
           std::string m_hardwareComponentName;
           std::string m_canPortName;
           std::string m_canPortError;
           std::string m_canPortID;
           std::string m_canPortOptions;
           CCanAccess* m_canBusAccess;
           CanBusAccess* m_canBusAccess2;



};





}

#endif // include guard
