/*
 * CommunicationObject.h
 *
 *  Created on: Mar 9, 2015
 *      Author: pnikiel
 */

#ifndef DEVICE_INCLUDE_COMMUNICATIONOBJECT_H_
#define DEVICE_INCLUDE_COMMUNICATIONOBJECT_H_

#include<statuscode.h>
#include<vector>
#include<boost/function.hpp>

// TODO: change into proper header file
#include<CCanAccess.h>

namespace Device
{


class CommunicationObject
{
public:
	enum { MAX_LEN_ARGUMENTS=7 } ;

	typedef boost::function<UaStatus (const unsigned char*, unsigned int, bool)> HandlerType;

	CommunicationObject(int type,  unsigned char commandIndex, unsigned char nodeID, unsigned char arg, HandlerType handler, float maxWaitingTime );
	virtual ~CommunicationObject() = default;

	//! For 1-arg objects
	CommunicationObject(int type,  unsigned char commandIndex, unsigned char nodeID, const unsigned char * arguments, unsigned int argumentsLen, HandlerType handler, float maxWaitingTime );

	//! Fills up given CAN message buffer such that it can be sent as a request
	//! Note: id is not altered!
	void requestToCanMessage ( CanMessage & msg );

	void handleReply ( const CanMessage & msg, bool valid );

	//!


	float getMaxWaitingTime () const { return m_maxWaitingTime; }

	//! This compares for the request only. maxWaitingTime and handler are not taken into account
	bool operator==(const CommunicationObject& other) { return this->m_commandIndex==other.m_commandIndex && this->m_arguments==other.m_arguments; }

	int m_HardwareType;

private:
	unsigned char m_commandIndex;
	std::vector<unsigned char> m_arguments;
	float m_maxWaitingTime;
	HandlerType m_handler;
	unsigned char nodeID;

};


}



#endif /* DEVICE_INCLUDE_COMMUNICATIONOBJECT_H_ */
