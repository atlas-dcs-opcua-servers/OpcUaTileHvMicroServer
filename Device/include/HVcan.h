
#ifndef HVCAN_H
#define HVCAN_H

#define HV_RESET		      		0x01
#define HV_SAFETY_MODE               		0x02
#define HV_CONTROL_MODE		      		0x03
#define HV_BROADCAST_STATUS	      		0x04
#define HV_READ_GLOBAL_STATUS 	      		0x05
#define HV_VALID_REF_CHECK	      		0x06 
#define HV_CHAN_READ                  		0x07
#define HV_WRITE_ORDER                		0x08
#define HV_SAVE_ORDER                 		0x09
#define HV_RESTORE_ORDER              		0x0A
#define HV_WRITE_MAXSTEP              		0x0B
#define HV_INFO_READ                  		0x0C
#define HV_READ_CALIBRATIO_PARAMETERS		0x22
#define HV_ADDRESS_READ		      		0x1A
#define HV_ADDRESS_WRITE              		0x1B             
#define HV_ODDEVEN_SWITCH			0x13

#define HV_CANBUS_ID_MASK           		0x100
#define OBJECT_MASK               		0x780
#define NODEID_MASK               		0x07F

#define HVPSU_MAX               		1300
#define HVPSU_NORM               		830

#define CANPSU_MAX               		24000

#define CANPSU_Parameter			100
#define HV_Parameter				101





#endif // HVCAN_H

