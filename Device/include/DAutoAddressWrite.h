
/* This is device header stub */


#ifndef __DAutoAddressWrite__H__
#define __DAutoAddressWrite__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>



/* forward decl for AddressSpace */
namespace AddressSpace {
class
    ASAutoAddressWrite
    ;
}
/* forward decl for Parent */

#include <DDrawer.h>


namespace Device
{

/* forward declarations (comes from design.class.hasObjects) */


class
    DAutoAddressWrite
{

public:
    /* sample constructor */
    explicit DAutoAddressWrite ( const Configuration::AutoAddressWrite & config

                                 , DDrawer * parent

                               ) ;
    /* sample dtr */
    ~DAutoAddressWrite ();

    DDrawer * getParent () const {
        return m_parent;
    }
    UaStatus writeAddressSoftValue ( const OpcUa_Int32 & v);

    /* add/remove for handling device structure */


    /* to safely quit */
    void unlinkAllChildren ();

    /* delegators for
    cachevariables and sourcevariables */

    /* ASYNCHRONOUS !! */
    UaStatus readItemValue (
    		OpcUa_Float &value,
        UaDateTime &sourceTime
    );

    /* ASYNCHRONOUS !! */
    UaStatus writeItemValue (
    		OpcUa_Float &value
    );


    void linkAddressSpace (AddressSpace::
                           ASAutoAddressWrite
                           * as);
    AddressSpace::ASAutoAddressWrite * getAddressSpaceLink () const {
        return m_addressSpaceLink;
    }


    /* find methods for children */


    /* getters for values which are keys */


    /* mutex operations */


    /* variable-wise locks */


	CanMessage msg;
	CanMessage msgReply;

	void passADDRESSWRITE(const CanMessage rmsg);

	   unsigned int m_index;
	    unsigned int m_subindex;
	    OpcUa_Int16 statebyteWrite ;
	    OpcUa_Int16 State_Byte  ;
	    OpcUa_Int16 SoftAddress ;
	    OpcUa_Int16 NewSoftAddress ;
	    OpcUa_Int32 HardAddress  ;
	    OpcUa_Int16 repMessageIn ;
	    OpcUa_UInt32 m_ind;
	    OpcUa_UInt32 m_subind;


private:


    DDrawer * m_parent;


    AddressSpace::
    ASAutoAddressWrite
    *m_addressSpaceLink;

    /* if any of our cachevariables has isKey=true then we shall keep its copy here for find functions  (it is const, either way) */


    /* object-wise lock */


    /* variable-wise locks */


};




}

#endif // include guard
