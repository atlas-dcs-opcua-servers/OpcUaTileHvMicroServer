/*
 * SockCanScan.h
 *
 *  Created on: Jul 21, 2011
 *      Author: vfilimon
 */

#ifndef SOCKCANSCAN_H_
#define SOCKCANSCAN_H_

#include <pthread.h>
#include <unistd.h>
#ifdef WIN32
#include "Winsock2.h"
#endif

#include <string>
using namespace std;
#include "CCanAccess.h"

class CSockCanScan : public CCanAccess
{
 public:
  
  CSockCanScan() : sock(0) { }
    
    virtual ~CSockCanScan();
    
    virtual bool sendMessage(short cobID, unsigned char len, unsigned char *message);
    virtual bool sendRemoteRequest(short cobID);
    virtual bool createBUS(const char *name ,const  char *parameters );
    
    string &getNamePort() { return name_of_port; }
    int getHandler() { return sock; }
    string &getChannel() { return channelName; }
    
 private:
    bool run_can;
    int sock;
    
#define Timeout  1000
    pthread_t   m_hCanScanThread;
    int         m_idCanScanThread;

    void sendErrorMessage(const char  *);
    void sendErrorMessage(const struct can_frame *);

    void clearErrorMessage();

    int configureCanboard(const char *,const char *);
    int openCanPort();
    void waitCanDriver();
    static void* CanScanControlThread(void *);
    string name_of_port;
    string channelName;
    string param;
};


#endif /* SOCKCANSCAN_H_ */
