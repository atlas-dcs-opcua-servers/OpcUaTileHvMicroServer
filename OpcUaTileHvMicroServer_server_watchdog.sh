#!/bin/bash

source ~/.bashrc

# colored log messages
#
if tty -s ; then
   declare resetColor="\033[0m"
   declare redFg="\033[31m"
   declare greenFg="\033[32m"
   declare yellowFg="\033[33m"
fi
loginfo() {
   echo -e "${greenFg}Info:${resetColor}" $@
}
logerror() {
   echo -e "${redFg}Error:${resetColor}" $@
}
logwarning() {
   echo -e "${yellowFg}Warning:${resetColor}" $@
}

DEVPATH=/localdisk/winccoa/TilCalHVMicroServer
PRODPATH=/opt/TilCalHVMicroServer/bin
SERVERPATH=$PRODPATH

if [ "$2" = "-dev" ]
then
  loginfo "Starting development version from $DEVPATH ..."
  SERVERPATH=$DEVPATH
else
  loginfo "Starting production version from $PRODPATH ..."
fi
cd $SERVERPATH
pwd

## probably not needed
#export LD_LIBRARY_PATH=$SERVERPATH:$LD_LIBRARY_PATH
#export
#echo HOST $HOST
#echo HOSTNAME $HOSTNAME
##

ps aux | grep -v grep | grep "./TilCalHVMicroServer $1"

# if not found - equals to 1, start it
if [ $? -eq 1 ]
then
    logwarning TilCalHVMicroServer not running! Restarting with command
    logwarning './TilCalHVMicroServer $1 &> /localdisk/TilCalHVMicroServer.log &'
    ./TilCalHVMicroServer $1 &> /localdisk/TilCalHVMicroServer.log &
else
    loginfo TilCalHVMicroServer running.
fi

loginfo Done.
