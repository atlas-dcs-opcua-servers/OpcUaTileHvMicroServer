#ifndef CANBUSACCESS_H
#define CANBUSACCESS_H

// CanBusAccess.h

#include "dlcanbus.h"
#include "CCanAccess.h"
#include <map>
#include <string>

#pragma once

using namespace std;
/**
 * CanBusAccess class ensure a connection to can hardware. 
 * it can create the connection to different hardware at same time
 * the syntax of the name is "name of component:name of the channel"
 */
class CanBusAccess {

	public:
		CanBusAccess() : Component(), ScanManagers() {m_instance = this;};
		CCanAccess * openCanBus(string name,string parameters);
		void closeCanBus(CCanAccess *cca);
		static CanBusAccess * getInstance() { return m_instance; }

	private:
		bool isCanPortOpen(string pn) { return (ScanManagers.find(pn) != ScanManagers.end()); }
		map<string,dlcanbus *> Component;
		map<string,CCanAccess *> ScanManagers;
		static CanBusAccess * m_instance;
};

#endif // CANACCESS_H
