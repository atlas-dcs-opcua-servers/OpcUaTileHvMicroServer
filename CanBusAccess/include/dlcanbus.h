// dlcanbus.h

#pragma once

#include "CCanAccess.h"
#include "stdio.h"

#include <string>
#include <map>

#include <iostream>
#ifdef WIN32
typedef __declspec(dllimport) CCanAccess *create_canObj();
#include "Winsock2.h"
#else
typedef CCanAccess *create_canObj();
#endif

using namespace std;

class dlcanbus {
public:
	dlcanbus();
	~dlcanbus();
	create_canObj	*maker_CanAccessObj;

	bool openInterface(char *ncomponect);
private:
	string	componentName;
#ifdef WIN32
	HMODULE	p_libComponent;
#else
	void	*p_libComponent;
#endif

	static const unsigned int BUF_SIZE = 1024;

};
