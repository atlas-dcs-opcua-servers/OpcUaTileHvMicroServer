/*
 * CCanAccess.h
 *
 *  Created on: Apr 4, 2011
 *      Author: vfilimon
 */

#ifndef CCANACCESS_H_
#define CCANACCESS_H_

#include <time.h>
#include "boost/bind/bind.hpp"
#include "boost/signals2.hpp"

using namespace boost::placeholders;

#ifdef WIN32
#include "Winsock2.h"
#endif

typedef struct CanMsgStruct
{
	long c_id;
	unsigned char c_ff;
	unsigned char c_dlc;
	unsigned char c_data[8];
	timeval	c_time;
public:
	CanMsgStruct() :
		c_id(0),
		c_ff(0),
		c_dlc(0)
		{
			for (int i=0; i<8; i++)
				c_data[i] = 0;

		}
} CanMessage;

class CCanAccess {
public:
	CCanAccess() {};
	virtual bool createBUS(const char * ,const char *) = 0 ;
	virtual bool sendRemoteRequest(short ) = 0;
	virtual bool sendMessage(short , unsigned char, unsigned char *) = 0;

	char *getBusName() { return busName; }
	virtual ~CCanAccess() {};

	boost::signals2::signal<void (const CanMessage &) > canMessageCame;
	boost::signals2::signal<void (const int,const char *,timeval &) > canMessageError;

protected:
	char *busName;
};

#endif /* CCANACCESS_H_ */
