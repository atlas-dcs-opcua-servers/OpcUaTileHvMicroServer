%define version r32709.0.0
%define release 0
%define name TilCalHVMicroServer
%define _topdir %(echo $PWD)
%define _tmpdir %{_topdir}/tmp
%define PREFIX /opt/%{name}

AutoReqProv: yes 
Summary: TilCalHVMicroServer_V2
Name: %{name}
Version: %{version}
Release: %{release}
Source0: checkout.tar.gz
License: zlib/libpng license
Group: Development/Application
BuildRoot: %{_topdir}/BUILDROOT/%{name}-%{version}
BuildArch: x86_64
Prefix: %{PREFIX}
Vendor: CERN 
Requires: OpcUaToolkit




%description
This is our super-amazing-fancy OPC UA server for INSERT NAME OF EQUIPMENT HERE.
Based on Generic OPC UA Server framework by ATLAS Central DCS, CERN.

%prep
echo ">>> setup tag" 
echo %{name}

%setup -n checkout 



%build
echo "--- Build ---"
export PATH=/usr/lib64/qt4/bin:$PATH
export UNIFIED_AUTOMATION_OPCUA_SDK=/opt/OpcUaToolkit-1.3.3/
sh ./automated_build_linux.sh




%install
echo "--- Install (don't confuse with installation; nothing is installed on your system now in fact...) ---"
INSTALLED_DIR=%{buildroot}/%{PREFIX}/bin
/bin/mkdir -p $INSTALLED_DIR 
/bin/cp bin/TilCalHVMicroServer                                         $INSTALLED_DIR
/bin/cp bin/ServerConfig.xml                         $INSTALLED_DIR/
/bin/cp bin/config.xml                          $INSTALLED_DIR 
/bin/cp bin/standard_drawer.xml                          $INSTALLED_DIR 
/bin/cp Configuration/Configuration.xsd                    $INSTALLED_DIR 
/bin/cp TilCalHVMicroServer.conf                           $INSTALLED_DIR
/bin/cp CanInterface/libHVopcuasockcan.so	           $INSTALLED_DIR 





%pre
echo "Pre-install: nothing to do"

%post
cd %{PREFIX}/bin
echo "Post-install:"
echo "Setting CAP_NET_ADMIN on TilCalHVMicroServer's binary..."
/usr/sbin/setcap cap_net_admin=ep TilCalHVMicroServer
echo "Creating symbolic links for libHVTilesockcan.so..."
/bin/ln -f -s %{PREFIX}/bin/libHVopcuasockcan.so %{PREFIX}/bin/libHVTilesockcan.so
echo "Creating ld.so entry..."
/bin/cp -a TilCalHVMicroServer.conf  /etc/ld.so.conf.d
echo "Running ldconfig..."
/sbin/ldconfig
echo "Generating OPC UA TilCalHVMicroServer Certificate..."

%{PREFIX}/bin/TilCalHVMicroServer --create_certificate



%preun


if [ $1 = 0 ]; then
	echo "Pre-uninstall: Complete uninstall: will remove files"
    cd %{PREFIX}/bin
    rm -f libHVopcuasockcan.so* 
fi
# 
# Hint: if your server installs any shared objects, you should run ldconfig here. 
#



%postun
if [ $1 = 0 ]; then
    echo "Post-uninstall: Complete uninstall: will remove files"
    echo "Removing ld.so information..."
	rm -f /etc/ld.so.conf.d/%{name}.conf
fi

# Unconditionally run ldconfig in case any .so (shared object) got uninstalled (due to being in %files section)
# This will not hurt and may save some pain in *ss
#
echo "Running ldconfig..."
/sbin/ldconfig

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root)
%{PREFIX}

%changelog
































