/*
 * Utils.cpp
 *
 *  Created on: Nov 24, 2014
 *      Author: pnikiel
 */


#include <Utils.h>
#include <CustomUtils.h>

double subtractTimeval (const timeval &t1, const timeval &t2)
{

	return t2.tv_sec-t1.tv_sec + double(t2.tv_usec-t1.tv_usec)/1000000.0;

}

UaString bytesToUaString( const unsigned char* data, unsigned int len )
{
	std::string s (reinterpret_cast<const char*>(data), len);
	return UaString ( s.c_str() );
}
