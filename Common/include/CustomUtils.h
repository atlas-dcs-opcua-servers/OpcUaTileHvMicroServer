/*
 * CustomUtils.h
 *
 *  Created on: Jan 25, 2016
 *      Author: pnikiel
 */

#ifndef COMMON_INCLUDE_CUSTOMUTILS_H_
#define COMMON_INCLUDE_CUSTOMUTILS_H_

#include <sys/time.h>
#include <uadatetime.h>

double subtractTimeval (const timeval &t1, const timeval &t2);

UaString bytesToUaString( const unsigned char* data, unsigned int len );



#endif /* COMMON_INCLUDE_CUSTOMUTILS_H_ */
