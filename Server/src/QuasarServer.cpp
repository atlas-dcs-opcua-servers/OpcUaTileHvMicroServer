/* © Copyright CERN, Universidad de Oviedo, 2015.  All rights not expressly granted are reserved.
 * QuasarServer.cpp
 *
 *  Created on: Nov 6, 2015
 * 		Author: Damian Abalo Miron <damian.abalo@cern.ch>
 *      Author: Piotr Nikiel <piotr@nikiel.info>
 *
 *  This file is part of Quasar.
 *
 *  Quasar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public Licence as published by
 *  the Free Software Foundation, either version 3 of the Licence.
 *
 *  Quasar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public Licence for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Quasar.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "QuasarServer.h"
#include <LogIt.h>
#include <string.h>
#include <shutdown.h>
#include <DCanBus.h>
#include <DDrawer.h>
#include <DCrate.h>

QuasarServer::QuasarServer() : BaseQuasarServer()
{

}

QuasarServer::~QuasarServer()
{
 
}

void QuasarServer::mainLoop()
{
    printServerMsg("Press "+std::string(SHUTDOWN_SEQUENCE)+" to shutdown server");

    // Wait for user command to terminate the server thread.

    while(ShutDownFlag() == 0)
    {
	UaThread::sleep (1);

	BOOST_FOREACH( Device::DCanBus * canbus, Device::DRoot::getInstance()->canbuss() )
	{
	    BOOST_FOREACH( Device::DDrawer * drawer, canbus->drawers() )
	    {
		drawer->tick();
	    }
	}


	BOOST_FOREACH( Device::DCanBus * canbus, Device::DRoot::getInstance()->canbuss() )
	{
	    BOOST_FOREACH( Device::DCrate * crate, canbus->crates() )
	    {
		crate->tick();
	    }
	}

    }
    printServerMsg(" Shutting down server");
}

void QuasarServer::initialize()
{
    LOG(Log::INF) << "Initializing TileHvMicro device logic.";
    BOOST_FOREACH( Device::DCanBus* b, Device::DRoot::getInstance()->canbuss() )
    {
	printf("opening the ports\n");
	b->openCanBus ();
    }
}

void QuasarServer::shutdown()
{
    LOG(Log::INF) << "Shutting down Quasar server.";
	
}

void QuasarServer::initializeLogIt()
{
	//Log::initializeLogging();
	BaseQuasarServer::initializeLogIt();
  LOG(Log::INF) << "Logging initialized.";
}
